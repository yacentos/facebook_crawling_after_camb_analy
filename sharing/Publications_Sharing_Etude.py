import networkx as nx
import pylab
import matplotlib.pyplot as plt
from _0_Functions import *
import pickle
import matplotlib.patches as mpatches

G=nx.Graph()
def plot(T,pos,nom,g):
    try:
        import pygraphviz
        from networkx.drawing.nx_agraph import graphviz_layout
    except ImportError:
        try:
            import pydotplus
            from networkx.drawing.nx_pydot import graphviz_layout
        except ImportError:
            raise ImportError("This example needs Graphviz and either "
                              "PyGraphviz or PyDotPlus")
    node_color = []
    node_lise = []
    node_size = []
    i = 0
    j = 0
    directory = "Publications_Sharing_Etude/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    directory = "Publications_Sharing_Etude/"+g+"/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    update = 0

    for vertic in T:
        #if j>40:break
        update = 0

        if vertic[4] == 'n' :
            #print vertic
            if not (G.has_node(vertic[2])):
                #print i, '  haaaaaaaaas not npde', vertic[2]
                node_color.append(vertic[3])
                node_lise.append(int(vertic[2]))
                G.add_node(int(vertic[2]))  # , vertic[0]
                node_size.append(30)
                update = 1
        else:
            i += 1
            #print i, '  -  edge', vertic

            if not (G.has_edge(int(vertic[3]), int(vertic[2]))):
                G.add_edge(int(vertic[3]), int(vertic[2]))
                update = 1
        #print update,"    nodesss =", G.nodes()
        j += 1
        print G.number_of_nodes(), "  ", G.number_of_edges(), "  ", update
        if update != 0 :
            # pos=nx.random_layout(G)
            # pos = nx.spring_layout(G)
            # pos = nx.circular_layout(G)
            # pos = nx.nx_pydot.graphviz_layout(G)
            pos = graphviz_layout(G, prog="neato")  # atlas algorithme
            # pos = nx.fruchterman_reingold_layout(G)
            # plt.clf()


            #if j>40:plt.pause(600.00000000001)
        if (G.number_of_nodes() % 100) == 0 :
            nx.draw_networkx_nodes(G, pos, nodelist=node_lise, node_color=node_color, node_size=node_size)
            nx.draw_networkx_edges(G, pos, width=1.0)
            plt.axis('off')  # supprimer les axes
            plt.pause(0.00000000001)
            plt.show()  # display
            #print pos
            save(plt, nom, g, pos, node_color, node_size, node_lise,"Relation " + str(nom) + "_origine => " + str(nom) + "_destination (" + str(
        G.number_of_nodes()) + "_Nodes -- " + str(G.number_of_edges()) + "_Arrets )")

    print "Nodes= ", G.number_of_nodes()
    print "Edges= ", G.number_of_edges()
    save(plt,"final_"+nom, g, pos, node_color, node_size, node_lise,"Etude PArtage Publication au sein du group "+g)
    #plt.pause(600.00000000001)

def save(plt,nom,g,pos,node_color,node_size,node_lise,title):
    plt.title(title)
    red_patch = mpatches.Patch(color='red', label='Groupe Original')
    green_patch = mpatches.Patch(color='green', label='Groupe Destination')
    blue_patch = mpatches.Patch(color='blue', label='Publication Destianation')
    yellow_patch = mpatches.Patch(color='yellow', label='Publication Original')
    plt.legend(handles=[red_patch, yellow_patch, green_patch, blue_patch])
    plt.savefig("Publications_Sharing_Etude/" + g + "/type_" + nom + "_"+ str(G.number_of_nodes()) + ".png", dpi=700)  # save as png

    with open("Publications_Sharing_Etude/" + g + "/position_" + nom + ".txt", 'wb') as thefile:
        #print pos
        pickle.dump(pos, thefile)

    with open("Publications_Sharing_Etude/" + g + "/node_color" + nom + ".txt", 'wb') as thefile:
        # thefile.write(str(node_color))
        #print node_color
        pickle.dump(node_color, thefile)
    with open("Publications_Sharing_Etude/" + g + "/node_size_" + nom + ".txt", 'wb') as thefile:
        # thefile.write(str(node_size))
        #print node_size
        pickle.dump(node_size, thefile)
    with open("Publications_Sharing_Etude/" + g + "/node_list_" + nom + ".txt", 'wb') as thefile:
        # thefile.write(str(node_lise))
        #print node_lise
        pickle.dump(node_lise, thefile)
    with open("Publications_Sharing_Etude/" + g + "/edges_" + nom + ".txt", 'wb') as thefile:
        # thefile.write(str(node_lise))
        #print G.edges()
        pickle.dump(G.edges(), thefile)

def main():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    pylab.ion()  # Turn on interactive mode.
    pylab.hold(False)  # Clear the plot before adding new data.

    i = 0
    T = []
    sql = "SELECT groups.idgroup,groups.name FROM groups,feed " \
          "where groups.idgroup=feed.idgroup group by groups.idgroup order by idgroup ASC "  #
    print(sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    pos = {}

    for group in groups:

        pos.clear()
        del T[:]
        G.clear()

        sql = 'SELECT created_time_original, id_group_original , id_etude_share ,' \
              ' created_time_destination , id_group_destination ,idfeed_destination , shares.story from shares,feed ' \
              ' where id_etude_share =feed.idfeed and feed.idgroup='+str(group[0])+' order by id_etude_share '#idfeed_original
        print sql
        cursor.execute(sql)
        liste = cursor.fetchall()
        type = 'groups'   #  all    feeds    groupes

        if type == 'groups_feeds':
            for l in liste:
                i += 1
                TT = (float(l[0]),1, l[1], 'red','n') #group original
                T.append(TT)
                TT = (float(l[0]),1, l[2], 'yellow', 'n') #pub original
                T.append(TT)
                TT = (float(l[3]),1, l[4], 'green', 'n') #group destination
                T.append(TT)
                TT = (float(l[3]),1, l[5], 'blue', 'n') #pub destination
                T.append(TT)
                min = float(l[0])
                if min < float(l[3]):
                    min = float(l[3])

                TT = (float(l[0]), 9,l[1] ,l[2],'e')
                T.append(TT)
                TT = (float(l[3]), 9,l[4], l[5],'e')
                T.append(TT)
                TT = (min, 9,l[2], l[5], 'e')
                T.append(TT)
        elif type == 'feeds':
            for l in liste:
                i += 1
                #****************************** Group Original
                TT = (float(l[0]), 1 ,l[2], 'yellow','n')
                T.append(TT)
                TT = (float(l[3]), 1, l[5], 'blue', 'n')
                T.append(TT)
                TT = (float(l[3]), 9 , l[2], l[5],'e')
                T.append(TT)
        elif type == 'groups':
            for l in liste:
                i += 1
                TT = (float(l[0]), 1,l[1], 'red','n')
                T.append(TT)
                TT = (float(l[3]), 1,l[4], 'green','n')
                T.append(TT)
                TT = (float(l[3]), 9,l[1], l[4], 'e')
                T.append(TT)

        T.sort()
        print T.__len__(),"     avant i = " , i
        plot(T,pos,type,str(group[0]))
    connection.close()

if __name__ == '__main__':
    main()