import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import xlsxwriter

from _0_Functions import *


def main():
    """ Avec ce code , je peux afficher :
        1 : les publications destinations qui deviennent original et ont a leur tour des publication destinations
        2 : les groupes destinations qui deviennent original et ont a leur tour des groupes destinations"""
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    ################## difference et difference  ######################
    T = []
    sql = "SELECT groups.idgroup,groups.name FROM groups,feed where groups.idgroup=feed.idgroup group by groups.idgroup order by idgroup DESC "  #
    print(sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    for group in groups:

        workbook = xlsxwriter.Workbook('Publications_Propagation/Publications_Propagation_'+str(group[0])+'.xlsx')
        worksheet = workbook.add_worksheet()

        # Add a bold format to use to highlight cells.
        bold = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})

        worksheet.write_string('A1', 'Id feed', bold)
        worksheet.write_string('B1', 'Date1', bold)
        worksheet.write_string('C1', 'Id Destination', bold)
        worksheet.write_string('D1', 'Date2', bold)
        worksheet.write_string('E1', 'Difference', bold)
        worksheet.write_string('F1', 'Diferrence_2', bold)
        worksheet.write_string('G1', 'Id feed etudie', bold)


        sql = 'SELECT created_time_original, id_group_original , idfeed_original ,' \
              ' created_time_destination , id_group_destination ,idfeed_destination , story ,id_etude_share from shares ' \
              ' where id_group_original ='+str(group[0])+' order by id_etude_share , created_time_original,created_time_destination'
        print sql
        cursor.execute(sql)
        liste = cursor.fetchall()
        i = 0
        precedent =  0
        ancien = 0
        interval2 = []
        evolution1 = []
        evolution2 =[]
        moyenne_date22 = []

        for l in liste:
            #print ' Loooooooooooooong L2  ==> ', long(l[2])
            if long(ancien) <> long(l[2]):
                #print " difference   "
                ancien = l[2]
                first = float(l[0])
                precedent = float(l[3])

            difference2 = float(l[3]) - precedent
            difference = float(l[3]) - float(l[0])
            print ancien, ' ==> ', long(l[2]), " ==> ", l[0], " ==> ", l[5], " ==> ", l[3], " ==> ", difference, " ==> ", difference2

            sql = ' SELECT count(*) from shares where id_group_destination ='+str(l[1])
            cursor.execute(sql)
            nbr = cursor.fetchone()
            print str(l[2]), "  ====>  ",nbr[0]
            print datetime.datetime.fromtimestamp(int(l[0])).strftime('%d-%m-%Y')

            t = l[2], l[0],l[5], l[3], difference,difference2,l[7]
            T.append(t)

            if ancien == 223708587811000:
                moyenne_date22.append(str((datetime.datetime.fromtimestamp(int(l[3])).strftime('%d-%m-%Y')) + "//" +
                                         (datetime.datetime.fromtimestamp(int(precedent)).strftime('%d-%m-%Y'))))
                moyenne_date = (float(precedent) + float(l[3])) / 2
                interval2.append(moyenne_date)

                evolution1.append(difference)
                evolution2.append(difference2)

            precedent = float(l[3])

        #T.sort()
        plt.clf()
        # print interval
        # print comments
        # print feeds
        # print moyenne_date2[0],"    ", moyenne_date2[moyenne_date2.__len__()-1],"    ", min_date, "    ",max_date
        directory = "Publications_Propagation/"
        if not os.path.exists(directory):
            os.makedirs(directory)

        print len(interval2),"  ",interval2
        print len(evolution2), "  ", evolution2
        print len(moyenne_date22), "  ", moyenne_date22
        plt.figure(1)
        plt.title(" Difference Partage Source Destination ")
        plt.xticks(interval2, moyenne_date22, rotation=90)
        red_patch = mpatches.Patch(color='red', label='Difference destination-Source ')
        #green_patch = mpatches.Patch(color='green', label='Difference Destination2-Destination1')
        plt.legend(handles=[red_patch])#, green_patch])
        plt.plot(interval2, evolution1, 'r')#, interval2, evolution2, 'g')
        plt.ylabel('Nombre')
        plt.xlabel('Date')
        plt.savefig("Publications_Propagation/destination-Source.png")
        #plt.show()

        plt.figure(2)
        plt.title(" Difference Partage Destination2-Destination1")
        plt.xticks(interval2, moyenne_date22, rotation=90)
        #red_patch = mpatches.Patch(color='red', label='Difference destination-Source ')
        green_patch = mpatches.Patch(color='green', label='Difference Destination2-Destination1')
        plt.legend(handles=[green_patch])
        plt.plot(interval2, evolution2, 'g')
        plt.ylabel('Nombre')
        plt.xlabel('Date')
        plt.savefig("Publications_Propagation/Destination2-Destination1.png")
        #plt.show()


        row = 1
        col = 0
        for t in T :
            print t
            worksheet.write_string(row, col , str(t[0]))
            worksheet.write_string(row, col + 1, str(t[1]))
            worksheet.write_string(row, col + 2, str(t[2]))
            worksheet.write_string(row, col + 3, str(t[3]))
            worksheet.write_string(row, col + 4, str(t[4]))
            worksheet.write_string(row, col + 5, str(t[5]))
            worksheet.write_string(row, col + 6, str(t[6]))
            row +=1
        workbook.close()
    connection.close()


if __name__ == '__main__':
    main()