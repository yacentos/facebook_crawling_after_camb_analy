import xlsxwriter
import matplotlib.pyplot as plt

from _0_Functions import *

def Nombre_publication_en_24_heure():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('Nombre_publication_en_24_heure.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.write_string('A1', 'Id Communaute', bold)
    #worksheet.merge()
    worksheet.write_string('B1', 'Nom', bold)
    worksheet.write_string('C1', '0h', bold)
    worksheet.write_string('D1', '1h', bold)
    worksheet.write_string('E1', '2h', bold)
    worksheet.write_string('F1', '3h', bold)
    worksheet.write_string('G1', '4h', bold)
    worksheet.write_string('H1', '5h', bold)
    worksheet.write_string('I1', '6h', bold)
    worksheet.write_string('J1', '7h', bold)
    worksheet.write_string('K1', '8h', bold)
    worksheet.write_string('L1', '9h', bold)
    worksheet.write_string('M1', '10h', bold)
    worksheet.write_string('N1', '11h', bold)
    worksheet.write_string('O1', '12h', bold)
    worksheet.write_string('P1', '13h', bold)
    worksheet.write_string('Q1', '14h', bold)
    worksheet.write_string('R1', '15h', bold)
    worksheet.write_string('S1', '16h', bold)
    worksheet.write_string('T1', '17h', bold)
    worksheet.write_string('U1', '18h', bold)
    worksheet.write_string('V1', '19h', bold)
    worksheet.write_string('W1', '20h', bold)
    worksheet.write_string('X1', '21h', bold)
    worksheet.write_string('Y1', '22h', bold)
    worksheet.write_string('Z1', '23h', bold)
    worksheet.write_string('AA1', 'Total', bold)

    sql = "SELECT idgroup,name FROM groups order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0
    for group in groups:
            count += 1
            del stat_detai[:]
            stat_detai.append(group[0])
            stat_detai.append(group[1])
            worksheet.write_number(row, col   , group[0])
            worksheet.write_string(row, col+1 , group[1])

            print( " (",count,") ",int(group[0]) , " len ",len(groups))

            # datetime between 2h and 8h
            sql2 = "select count(idgroup)  ,  FLOOR((createdtime%86400)/3600) as differene  " \
                   "from feed where  idgroup=" + str(
                group[0]) + "  group by differene; "
            print " sql 2 ",sql2

            cursor.execute(sql2)
            dates = cursor.fetchall()
            first_shift = 0
            second_shift = 0
            third_shift = 0

            for d in dates :

                worksheet.write_number(row, col + 2 + int (d[1]), d[0])
                #stat_detai.append(first_shift)
            row += 1
            statistics[group[0]]=stat_detai
    #print( workbook
    workbook.close()
    #ecriture_fichier("Sharing_horaire_Statistics", statistics)
    connection.close()

def Somme_Partage_publication_en_24_heure():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('Somme_Partage_publication_en_24_heure.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.write_string('A1', 'Id Communaute', bold)
    #worksheet.merge()
    worksheet.write_string('B1', 'Nom', bold)
    worksheet.write_string('C1', '0h', bold)
    worksheet.write_string('D1', '1h', bold)
    worksheet.write_string('E1', '2h', bold)
    worksheet.write_string('F1', '3h', bold)
    worksheet.write_string('G1', '4h', bold)
    worksheet.write_string('H1', '5h', bold)
    worksheet.write_string('I1', '6h', bold)
    worksheet.write_string('J1', '7h', bold)
    worksheet.write_string('K1', '8h', bold)
    worksheet.write_string('L1', '9h', bold)
    worksheet.write_string('M1', '10h', bold)
    worksheet.write_string('N1', '11h', bold)
    worksheet.write_string('O1', '12h', bold)
    worksheet.write_string('P1', '13h', bold)
    worksheet.write_string('Q1', '14h', bold)
    worksheet.write_string('R1', '15h', bold)
    worksheet.write_string('S1', '16h', bold)
    worksheet.write_string('T1', '17h', bold)
    worksheet.write_string('U1', '18h', bold)
    worksheet.write_string('V1', '19h', bold)
    worksheet.write_string('W1', '20h', bold)
    worksheet.write_string('X1', '21h', bold)
    worksheet.write_string('Y1', '22h', bold)
    worksheet.write_string('Z1', '23h', bold)
    worksheet.write_string('AA1', 'Total', bold)

    sql = "SELECT idgroup,name FROM groups order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0
    for group in groups:
            count += 1
            del stat_detai[:]
            stat_detai.append(group[0])
            stat_detai.append(group[1])
            worksheet.write_number(row, col   , group[0])
            worksheet.write_string(row, col+1 , group[1])

            print( " (",count,") ",int(group[0]) , " len ",len(groups))

            # datetime between 2h and 8h
            sql2 = "select sum(shares)  ,  FLOOR((createdtime%86400)/3600) as differene  " \
                   "from feed where  idgroup=" + str(group[0]) + "  group by differene; "
            print " sql 2 ",sql2

            cursor.execute(sql2)
            dates = cursor.fetchall()
            first_shift = 0
            second_shift = 0
            third_shift = 0

            for d in dates :

                worksheet.write_number(row, col + 2 + int (d[1]), d[0])
                #stat_detai.append(first_shift)
            row += 1
            statistics[group[0]]=stat_detai
    #print( workbook
    workbook.close()
    #ecriture_fichier("Sharing_horaire_Statistics", statistics)
    connection.close()

def horaire_plage():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('horaire_plage_Statistics.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.merge_range('A1:A2', 'Id Communaute', bold)
    #worksheet.merge()
    worksheet.merge_range('B1:B2', 'Nom', bold)
    worksheet.merge_range('C1:C2', '2h-10h', bold)
    worksheet.merge_range('D1:D2', '10h-18h', bold)
    worksheet.merge_range('E1:E2', '18h-2h', bold)
    worksheet.merge_range('F1:F2', 'Total', bold)

    sql = "SELECT idgroup,name FROM groups order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0
    for group in groups:
            count += 1
            del stat_detai[:]
            stat_detai.append(group[0])
            stat_detai.append(group[1])
            worksheet.write_number(row, col   , group[0])
            worksheet.write_string(row, col+1 , group[1])

            print( " (",count,") ",int(group[0]) , " len ",len(groups))

            # datetime between 2h and 8h
            sql2 = "select count(id_etude_share)  ,  FLOOR((created_time_destination%86400)/3600) as differene  " \
                   "from shares,feed where  feed.idgroup="+ str(group[0]) +" and id_etude_share=feed.idfeed group by differene; "
            sql2 = "select count(id_group_original)  ,  FLOOR((created_time_destination%86400)/3600) as differene  " \
                   "from shares where  id_group_original=" + str(
                group[0]) + "  group by differene; "
            print " sql 2 ",sql2

            cursor.execute(sql2)
            dates = cursor.fetchall()
            first_shift = 0
            second_shift = 0
            third_shift = 0

            for d in dates :
                if d[1]>=0 and d[1]< 8 :
                    first_shift += d[0]
                elif d[1]>=8 and d[1]< 16 :
                    second_shift += d[0]
                else:
                    third_shift += d[0]

            print(sql2, "  ==>  ", first_shift)
            worksheet.write_number(row, col + 2, first_shift)
            stat_detai.append(first_shift)

            # datetime between 10h and 18h
            print( sql2, "  ==>  ", second_shift)
            worksheet.write_number(row, col + 3, second_shift)
            stat_detai.append(second_shift)

            # datetime between 18h and 2h
            print( sql2, "  ==>  ", third_shift)
            worksheet.write_number(row, col + 4, third_shift)
            stat_detai.append(third_shift)

            # datetime between 18h and 2h
            print(sql2, "  ==>  ", first_shift+second_shift+third_shift)
            worksheet.write_number(row, col + 5, first_shift+second_shift+third_shift)
            stat_detai.append(first_shift+second_shift+third_shift)

            if first_shift==0 and second_shift==0 and third_shift==0 :
                sql2 = "SELECT count(*) FROM feed where idGroup=" + str(group[0])
                cursor.execute(sql2)
                feeds = cursor.fetchone()
                if feeds[0]==0:
                    sql2 = "delete FROM groups where idGroup=" + str(group[0])
                    cursor.execute(sql2)
                    connection.commit()

                    sql2 = "delete FROM nexts where idnexts=" + str(group[0])
                    cursor.execute(sql2)
                    connection.commit()
            row += 1
            statistics[group[0]]=stat_detai
    #print( workbook
    workbook.close()
    ecriture_fichier("horaire_plage_Statistics", statistics)
    connection.close()

def statistic():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('Sharing_Statistics.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.merge_range('A1:A2', 'Id Communaute', bold)

    #worksheet.merge()
    worksheet.merge_range('B1:B2', 'Nom', bold)
    worksheet.merge_range('C1:C2', 'Nombre Total De Publication', bold)
    worksheet.merge_range('D1:D2', 'Nombre De Partage Public de Publication', bold)
    worksheet.merge_range('E1:E2', 'Pourcentage', bold)
    worksheet.merge_range('F1:F2', 'Total Partage (Public+Prive)', bold)
    worksheet.merge_range('G1:G2', 'Date Premier Partage Public', bold)
    worksheet.merge_range('H1:H2', 'Date Dernier Partage Public', bold)

    sql = "SELECT groups.idgroup,groups.name FROM groups,feed where groups.idgroup=feed.idgroup group by groups.idgroup order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    axis_x = []
    axis_y = []
    count = 0
    directory = "Statistics_partage/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    axis_x.append(" Nombre Publication ")
    axis_x.append(" Nombre Partage ")
    axis_x.append(" Total Partage ")
    for group in groups:
            del axis_y[:]
            count += 1
            del stat_detai[:]
            stat_detai.append(group[0])
            stat_detai.append(group[1])
            worksheet.write_number(row, col   , group[0])
            worksheet.write_string(row, col+1 , group[1])

            print(" (", count, ") ", int(group[0]), " len ", len(groups))

            # Nombre De Publication
            sql2 = "SELECT count(*) FROM feed where idGroup=" + str(group[0])
            cursor.execute(sql2)
            feeds = cursor.fetchone()
            print(sql2, "  ==>  ", feeds[0])
            worksheet.write_number(row, col + 2, feeds[0])
            stat_detai.append(feeds[0])
            axis_y.append(feeds[0])

            # Nombre de partage
            sql2 = "SELECT count(*) FROM shares,feed where shares.id_etude_share=feed.idfeed and feed.idGroup=" + str(group[0])
            cursor.execute(sql2)
            shares = cursor.fetchone()
            print(sql2, "  ==>  ", shares[0])
            worksheet.write_number(row, col + 3, shares[0])
            stat_detai.append(shares[0])
            axis_y.append(shares[0])

            # pourcentage
            div = shares[0] / feeds[0]
            print(sql2, "  ==>  ", div)
            worksheet.write_number(row, col + 4, div)
            stat_detai.append(div)

            #Total De Partage
            sql2 = "select sum(shares) from feed where  feed.idGroup=" + str(group[0])
            cursor.execute(sql2)
            sum_share = cursor.fetchone()
            print(sql2, "  ==>  ", sum_share[0])
            worksheet.write_number(row, col + 5, sum_share[0])
            stat_detai.append(sum_share[0])
            axis_y.append(sum_share[0])

            # DatePremiere et Derniere Partage Publication
            sql2 = "select min(feed.createdtime),max(feed.createdtime) from groups,feed where feed.idgroup=" + str(group[0])
            cursor.execute(sql2)
            dates_min_max = cursor.fetchone()
            print(sql2, "  ==>  ", dates_min_max[0])
            worksheet.write_string(row, col + 6, datetime.datetime.fromtimestamp(int(dates_min_max[0])).strftime('%d-%m-%Y'))
            stat_detai.append(dates_min_max[0])

            print(sql2, "  dates_min_max ==>  ", dates_min_max[1])
            worksheet.write_string(row, col + 7, datetime.datetime.fromtimestamp(int(dates_min_max[1])).strftime('%d-%m-%Y'))
            stat_detai.append(dates_min_max[1])
            row += 1

            plt.figure(figsize=(8.0, 5.0))
            plt.xticks([1, 3, 5], axis_x, rotation='vertical', fontsize=8)
            plt.bar([1, 3, 5], axis_y)
            for a,b in zip([1, 3, 5],axis_y):
                plt.text(a,b,str(b))
            plt.title("Statistic Partage Group " + group[1])
            plt.xlabel("Value")
            plt.ylabel("Nombre")
            plt.savefig("Statistics_partage/" + str(group[0]) + ".png", dpi=700)
            plt.close()
            print ("*******************************************************************************************")
            statistics[group[0]]=stat_detai
    #print( workbook
    workbook.close()
    ecriture_fichier("Sharing_Statistics", statistics)
    connection.close()

def five_most_shared():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    workbook = xlsxwriter.Workbook('five_most_shared.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})

    worksheet.write_string('A1', 'Id Communaute', bold)
    worksheet.write_string('B1', 'Nom', bold)
    worksheet.write_string('C1', 'Id Publication', bold)
    worksheet.write_string('D1', 'Nombre Partage', bold)
    worksheet.write_string('E1', 'Description', bold)

    sql = "SELECT groups.idgroup,groups.name FROM groups,feed where groups.idgroup=feed.idgroup group by groups.idgroup order by idgroup DESC "  #
    print(sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0
    for group in groups:
        print len(groups), "   ", sql
        sql = " select idfeed,shares,name from feed where feed.idgroup="+str(group[0])+" order by floor(shares) DESC LIMIT 10 "

        cursor.execute(sql)
        feeds = cursor.fetchall()
        for feed in feeds:
            print feed[0],"   ",len(feeds), "   ", sql
            count += 1
            worksheet.write_number(row, col, group[0])
            worksheet.write_string(row, col + 1, group[1])
            worksheet.write_number(row, col + 2, feed[0])
            worksheet.write_string(row, col + 3, feed[1])
            worksheet.write_string(row, col + 4, feed[2])
            row += 1
        print " "
    workbook.close()
    connection.close()


if __name__ == "__main__":
    horaire_plage()
    statistic()
    Nombre_publication_en_24_heure()
    Somme_Partage_publication_en_24_heure()
    five_most_shared()