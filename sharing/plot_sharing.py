import xlsxwriter
from _0_Functions import *
import matplotlib.pyplot as plt

def Nombre_publication_en_24_heure():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    axis_x = []
    axis_y = []

    sql = "SELECT idgroup,name FROM groups order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    count = 0
    indice = 0

    for group in groups:
            count += 1
            del axis_x[:]
            del axis_y[:]

            print( " (",count,") ",int(group[0]) , " len ",len(groups))

            # datetime between 2h and 8h
            sql2 = "select count(idgroup)  ,  FLOOR((createdtime%86400)/3600) as differene  " \
                   "from feed where  idgroup=" + str(
                group[0]) + "  group by differene order by differene; "
            print " sql 2 ",sql2

            cursor.execute(sql2)
            dates = cursor.fetchall()
            count = 0
            for d in dates :
                while count != int (d[1]) :
                    axis_x.append(count)
                    axis_y.append(0)
                    print count, "   ", count, "   0 "
                    count += 1

                print count , "   ",int (d[1]),"   ",d[0]
                axis_x.append( int (d[1]))
                axis_y.append(d[0])
                count += 1
                #stat_detai.append(first_shift)
            print " axis_x " ,axis_x
            print " axis_y ", axis_y

            plt.figure(figsize=(8.0, 5.0))
            plt.xticks(axis_x, axis_x, rotation='vertical', fontsize=8)
            plt.plot(axis_x, axis_y)
            plt.title("Nombre_publication_en_24_heure Group "+group[1])
            plt.xlabel("Value")
            plt.ylabel("Frequence")
            # plt.show()
            plt.savefig("Nombre_publication_en_24_heure/"+str(group[0])+".png", dpi=700)
            indice += 1
            plt.close()

    #ecriture_fichier("Sharing_horaire_Statistics", statistics)
    connection.close()

def Nombre_publication_en_heure():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}

    axis_x = []
    axis_y = []
    gr = []
    heure_24 = []
    minute_59 = []



        #heure_24[j] = minute_59
#    print heure_24[0][59]
    sql = "SELECT groups.idgroup,groups.name FROM feed , groups where groups.idgroup=feed.idgroup " \
          " group by feed.idgroup order by feed.idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    count = 0
    indice = 0

    for group in groups:
            for j in range(24):
                heure_24.append([0] * 60)
                print j, "  ", heure_24[j]
            gr.append(group[0])
            print " remplissage   ",heure_24



            count += 1
            print( " (",count,") ",int(group[0]) , " len ",len(groups))
            print heure_24
            # datetime between 2h and 8h   216000
            sql2 = "select count(idfeed)  ,  FLOOR ((createdtime%86400)/3600) as dh,FLOOR (((createdtime%86400)%3600)/60) as dm " \
                   "from feed where  idgroup=" + str(group[0]) + " group by dh , dm"
            print " sql 2 ",sql2


            cursor.execute(sql2)
            dates = cursor.fetchall()

            for d in dates :
                print int(d[1]), "___", int(d[2]), "___",heure_24[int(d[1])]#[int(d[2])]
                heure_24[int(d[1])][int(d[2])] = d[0]#heure_24[int(d[1])][int(d[2])] + 1
                print int(d[1]),"___",int(d[2]),"___",heure_24[int(d[1])][int(d[2])]
                print " "
                #time.sleep(3)
            statistics[group[0]] = heure_24
            print " statistics ", statistics
            #time.sleep(10)
            #plot_figures(group[0],heure_24)


            #for j in range(0, 24, 1):
                #print group[0],"   ",j ,"   ", heure_24[j]
            #time.sleep(10)

    ecriture_fichier("Nombre_publication_en_heure_doc",statistics)
    #
    connection.close()

def plot_figures():
    axis_x = []
    axis_y = []
    statistics = lecture_fichier("Nombre_publication_en_heure_doc")
    elem = 0
    indice = 0
    for elem in statistics:

        heure = statistics[elem]
        nom = elem
        print nom
        print heure
        time.sleep(2)

        for j in range(0, 24, 1):
                del axis_x[:]
                del axis_y[:]
                for i in range(0, 60, 1):
                    axis_x.append(i)
                    axis_y.append(heure[j][i])
                    print j, " _ ", i , "  == ",heure[j][i]
                    print str(nom) +" axis_x " ,axis_x
                    print str(nom) +" axis_y ", axis_y

                plt.figure(figsize=(8.0, 5.0))
                plt.xticks(axis_x, axis_x, rotation='vertical', fontsize=8)
                plt.plot(axis_x, axis_y)
                plt.title("Nombre_publication_entre_"+str(j)+"h_"+str(j+1)+"h_Group "+str(nom))
                plt.xlabel("Value")
                plt.ylabel("Frequence")
                    # plt.show()
                directory = "Nombre_publication_en_heure/" + str(nom) + "/"
                if not os.path.exists(directory):
                        os.makedirs(directory)
                plt.savefig("Nombre_publication_en_heure/"+str(nom)+"/"+str(j)+"h_"+str(j+1)+"h.png", dpi=700)
                indice += 1
                plt.close()

def horaire_plage():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    axis_x = []
    axis_y = []


    sql = "SELECT idgroup,name FROM groups order by idgroup DESC "#
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0
    axis_x.append("0h-8h")
    axis_x.append("8h-16h")
    axis_x.append("16h-0h")
    for group in groups:
            count += 1
            del axis_y[:]


            print( " (",count,") ",int(group[0]) , " len ",len(groups))

            # datetime between 2h and 8h
            sql2 = "select count(id_etude_share)  ,  FLOOR((created_time_destination%86400)/3600) as differene  " \
                   "from shares,feed where  feed.idgroup="+ str(group[0]) +" and id_etude_share=feed.idfeed group by differene; "
            sql2 = "select count(id_group_original)  ,  FLOOR((created_time_destination%86400)/3600) as differene  " \
                   "from shares where  id_group_original=" + str(
                group[0]) + "  group by differene; "
            print " sql 2 ",sql2

            cursor.execute(sql2)
            dates = cursor.fetchall()
            first_shift = 0
            second_shift = 0
            third_shift = 0

            for d in dates :
                if d[1]>=0 and d[1]< 8 :
                    first_shift += d[0]
                elif d[1]>=8 and d[1]< 16 :
                    second_shift += d[0]
                else:
                    third_shift += d[0]
            axis_y.append(first_shift)
            axis_y.append(second_shift)
            axis_y.append(third_shift)
            print " axis_x ", axis_x
            print " axis_y ", axis_y

            plt.figure(figsize=(8.0, 5.0))
            plt.xticks([0,5,10], axis_x, rotation='vertical', fontsize=8)
            plt.plot([0,5,10], axis_y)
            plt.title("Nombre_publication_plage_horaire Group " + group[1])
            plt.xlabel("Value")
            plt.ylabel("Frequence")
            # plt.show()
            directory = "horaire_plage/"
            if not os.path.exists(directory):
                os.makedirs(directory)
            plt.savefig("horaire_plage/" + str(group[0]) + ".png", dpi=700)
            plt.close()

if __name__ == "__main__":
    horaire_plage()
    #Nombre_publication_en_24_heure()
    #Nombre_publication_en_heure()
    #plot_figures()