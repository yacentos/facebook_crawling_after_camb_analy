import xlsxwriter
from _0_Functions import *

def statistic():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('Sharing_reactions_Statistics.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.merge_range('A1:A2', 'Id Communaute', bold)
    worksheet.merge_range('B1:B2', 'Nom', bold)
    worksheet.merge_range('C1:C2', 'id_feed', bold)
    worksheet.merge_range('D1:D2', 'shares', bold)
    worksheet.merge_range('E1:E2', 'date_creation', bold)
    worksheet.merge_range('F1:K1', 'Reactions', bold)
    worksheet.write('K2', 'LIKE', bold)
    worksheet.write('F2', 'LOVE', bold)
    worksheet.write('G2', 'HAHA', bold)
    worksheet.write('H2', 'WOW', bold)
    worksheet.write('I2', 'TRISTE', bold)
    worksheet.write('J2', 'Grr', bold)

    sql = " select max(floor(feed.shares)),feed.idgroup,groups.name  from feed,groups " \
          " where groups.idgroup=feed.idgroup group by  feed.idgroup order by  feed.idgroup DESC"
    print sql
    cursor3 = connection.cursor()

    count = 0
    row = 2
    col = 0

    cursor3.execute(sql)
    shares = cursor3.fetchall()
    for sh in shares:
        sql = " SELECT idfeed , createdtime FROM feed where feed.shares='" + str(int(sh[0])) + "' " \
            "and feed.idgroup='" + str(int(sh[1])) + "' "

        cursor2.execute(sql)
        groups = cursor2.fetchall()
        print len(groups),"     sql ",sql

        group = groups.__getitem__(0)
        if True : #for group in groups:
                count += 1
                del stat_detai[:]
                stat_detai.append(sh[1])
                stat_detai.append(sh[2])
                stat_detai.append(group[0])
                stat_detai.append(group[1])
                stat_detai.append(sh[0])
                worksheet.write_number(row, col   , sh[1])
                worksheet.write_string(row, col + 1 , sh[2])
                worksheet.write_number(row, col + 2, group[0])
                worksheet.write_string(row, col + 3, datetime.datetime.fromtimestamp(int(group[1])).strftime('%d-%m-%Y'))
                worksheet.write_number(row, col + 4, sh[0])

                print(" (", count, ") ", int(group[0]), " len ", len(shares))

                # Nombre De Publication
                sql2 = "SELECT count(idLiked) , type_like FROM likes where likes.idLiked=" + str(group[0]) + " " \
                        "group by type_like "
                print sql2
                cursor.execute(sql2)
                feeds_like = cursor.fetchall()
                print(sql2, "  ==>  ", feeds_like)
                worksheet.write_number(row, col + 10, 0)
                worksheet.write_number(row, col + 5, 0)
                worksheet.write_number(row, col + 6, 0)
                worksheet.write_number(row, col + 7, 0)
                worksheet.write_number(row, col + 8, 0)
                worksheet.write_number(row, col + 9, 0)

                stat_detai.append(0)
                stat_detai.append(0)
                stat_detai.append(0)
                stat_detai.append(0)
                stat_detai.append(0)
                stat_detai.append(0)

                for feed_like in feeds_like:
                    print(feed_like[0], "       ", feed_like[1])
                    if feed_like[1] == 'LIKE':
                        worksheet.write_number(row, col + 10, feed_like[0])
                        stat_detai.append(feed_like[0])

                    elif feed_like[1] == 'LOVE':
                        worksheet.write_number(row, col + 5, feed_like[0])
                        stat_detai.append(feed_like[0])

                    elif feed_like[1] == 'HAHA':
                        worksheet.write_number(row, col + 6, feed_like[0])
                        stat_detai.append(feed_like[0])

                    elif feed_like[1] == 'WOW':
                        worksheet.write_number(row, col + 7, feed_like[0])
                        stat_detai.append(feed_like[0])

                    elif feed_like[1] == 'TRISTE':
                        worksheet.write_number(row, col + 8, feed_like[0])
                        stat_detai.append(feed_like[0])

                    else:
                        worksheet.write_number(row, col + 9, feed_like[0])
                        stat_detai.append(feed_like[0])

                row += 1
                statistics[group[0]]=stat_detai
        #print( workbook
    workbook.close()
    ecriture_fichier("Sharing_reactions_Statistics", statistics)
    connection.close()

if __name__ == "__main__":
    statistic()