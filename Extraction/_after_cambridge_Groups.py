# import _suivi_publication
from _after_cambridge_functions import *
from _0_Functions import *

def mise_a_jour_users(sql, f, usr, pwd):
    connection = connect_db()
    cursor = connection.cursor()
    count = 1

    cursor.execute(sql)
    groups = cursor.fetchall()
    graph = ''
    graph = retourner_graph(f, usr, pwd, lock)
    for group in groups:

        id_group = str(group[0])
        try:
            post_id = str(id_group) + '?fields=link,name'
            post = graph.get_object(id=post_id)
            type_sender = remplacer_quotes(str(post['link']))
            nom_sender = remplacer_quotes(str(post['name']))
            if "user" in type_sender:
                type_sender = "User"
            else:
                type_sender = "Page"

            # ajout_acteur(id_group, nom_sender, cursor, connection, count, "Acteur")
            sql = "update `" + database_name + "`.`groups`  set `type`='" + type_sender + "' , `name`='" + nom_sender + "'" \
                                                                                                                        " where `idGroup` =" + id_group
            print sql, "  :  ", post
            cursor.execute(sql)
            connection.commit()

        except Exception as e:
            print " errooor ", e
            graph = retourner_graph(f, usr, pwd, lock)

    cursor.close()
    connection.close()
    # _suivi_publication.main()
    # os._exit(0)


def mise_a_jour_senders(first_sql, usr, pwd):
    connection = connect_db()
    cursor = connection.cursor()
    count = 1
    print " sql : ", first_sql, "  => ", heure_actuelle()
    cursor.execute(first_sql)
    groups = cursor.fetchall()
    print " sql : ", first_sql, "  => ", heure_actuelle()
    nombre_groupes = cursor.rowcount

    group_type_nom = ''
    type_sender = ''

    driver = Acces_Webdriver()

    if nombre_groupes > 0:

        Authentification(driver, usr, pwd)

        while nombre_groupes > 0:
                    for group in groups:
                        id_group = group[0]
                        type_sender    = ""
                        group_type_nom = ""
                        group_url = 'https://www.facebook.com/' + str(id_group)
                        print ' the url is  ', group_url
                        driver.get(group_url)
                        time.sleep(10)
                        group_type_error = driver.find_elements_by_css_selector(
                            "i[class='uiHeaderImage img sp_uFKie7PBuaY sx_1997c6']")
                        if len(group_type_error) > 0:
                            type_sender = "Disparu"
                            group_type_nom = "Disparu"
                        else:

                            # It s a USER
                            # group_type_user = driver.find_elements_by_css_selector(
                            #    "button[class='_42ft _4jy0 FriendRequestAdd addButton _4jy4 _517h _9c6']")

                            group_type_user = driver.find_elements_by_css_selector("a[class='_2nlw _2nlv']")


                            if len(group_type_user) > 0:
                                group_type_nom = group_type_user[0]
                                type_sender = "User"
                                group_type_nom = group_type_nom.get_attribute("innerHTML")
                                group_type_nom = str(group_type_nom).split("<span")
                                if len(group_type_nom) > 0 :
                                    group_type_nom = str(group_type_nom[0]) #.split('">')
                                    #group_type_nom = str(group_type_nom[1])
                                print " user  ",group_type_nom

                            else:
                                # It s a GROUP
                                group_type_groupe = driver.find_elements_by_css_selector(
                                    "a[class='_42ft _4jy0 _21ku _4jy4 _4jy1 selected _51sy mrm']")
                                if len(group_type_groupe) > 0:
                                    type_sender = "Group"
                                    group_type_nom = driver.find_element_by_css_selector("h1[class='seo_h1_tag']")
                                    group_type_nom = group_type_nom.get_attribute("outerHTML")
                                    print " groouuup  ",group_type_nom
                                    group_type_nom = str(group_type_nom).split("<span>")
                                    group_type_nom = str(group_type_nom[0]).split('">')
                                    group_type_nom = str(group_type_nom[1])

                                else:
                                    # It s a PAGE
                                    group_type_page = driver.find_elements_by_css_selector(
                                        "button[class='likeButton _4jy0 _4jy4 _517h _51sy _42ft']")
                                    if len(group_type_page) > 0:
                                        type_sender = "Page"
                                        group_type_nom = driver.find_element_by_css_selector("a[class='_64-f']")
                                        group_type_nom = group_type_nom.get_attribute("innerHTML")
                                        print " Page  ", group_type_nom

                                # ajout_acteur(id_group, nom_sender, cursor, connection, count, "Acteur")
                        sql = "update `" + database_name + "`.`groups`  set `type`='" + type_sender + \
                             "' , `name`='" + group_type_nom + "' where idGroup = " + str(id_group)

                        print " Sql  :  ", sql
                        cursor.execute(sql)
                        connection.commit()

                    cursor.execute(first_sql)
                    nombre_groupes = cursor.rowcount
                    print nombre_groupes ," first_sql  :  ", first_sql

    #cursor.close()
    connection.close()
    driver.close()

if __name__ == "__main__":

    lock = threading.Lock()
    usr_c = usr_pwd[1]

    fp = open("D:\Token_Facebook\groups_update.txt")
    line = fp.readline()
    line = str(line)
    print line,"  ",line.__contains__("Api_Graph")

    if line.__contains__("Api_Graph") :
        sql2 = " select idGroup from groups where type = '' order by idGroup Desc "
        print sql2
        mise_a_jour_users(sql2, usr_c[2], usr_c[0], usr_c[1])
    else:
        sql2 = " select idGroup from groups where type = 'need_search' order by idGroup Desc "
        print sql2
        mise_a_jour_senders(sql2, usr_c[0], usr_c[1])

    fp.close()

