# -*- coding: utf-8 -*-
import pickle
import time
import urllib
from BeautifulSoup import BeautifulStoneSoup
import cgi
from HTMLParser import HTMLParser
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
#chromedriver = "D:\chromedriver.exe"
import json
import requests
from _0_Functions import  *
import urllib, urllib2, cookielib, getpass
chromedriver = "E:\chromedriver.exe"

def extract_info(feed_item , key1 , key2 , delimeter):
    key_value = -1
    feed_key_exist = feed_item.split(key1)
    #print " nbr de " , key1 , " est : " , len(feed_key_exist)
    if len(feed_key_exist) > 1:
        key_exist = feed_key_exist[1].split(delimeter)
        key_value = key_exist[0]
        print key1, " : ", key_value, " ## "
    else:
        feed_key_exist = feed_item.split(key2)
        if len(feed_key_exist) > 1:
            key_exist = feed_key_exist[1].split(delimeter)
            key_value = key_exist[0]
        print key2, " : ", key_value, " ## "
    return key_value

def extraction_profile_info(profile):
    id = extract_info(profile, '"id":', '//***###-1', ',')
    gender = extract_info(profile, '"gender":', '//***###-1', ',')
    i18nGender = extract_info(profile, '"i18nGender":', '//***###-1', ',')
    name = extract_info(profile, '"name":', '//***###-1', ',')
    type = extract_info(profile, '"type":', '//***###-1', ',')
    uri = extract_info(profile, '"uri":', '//***###-1', ',')
    firstName = extract_info(profile, '"firstName":', '//***###-1', ',')
    vanity = extract_info(profile, '"vanity":', '//***###-1', ',')
    thumbSrc = extract_info(profile, '"thumbSrc":', '//***###-1', ',')
    persona = extract_info(profile, '"persona":', '//***###-1', ',')

def extraction_comment_body(body):
    text = extract_info(str(body), '"text":', '//***###-1', ',')
    url = extract_info(body, '"url":', '//***###-1', ',')
    id = extract_info(body, '"id":', '//***###-1', ',')
    type = extract_info(body, '"type":', '//***###-1', ',')
    likecount = extract_info(body, '"likecount":', '//***###-1', ',')
    parentcommentid = extract_info(body, '"parentcommentid":', '//***###-1', ',')
    parentcommentauthorid = extract_info(body, '"parentcommentauthorid":', '//***###-1', ',')
    fbid = extract_info(body, '"fbid":', '//***###-1', ',')
    author = extract_info(body, '"author":', '//***###-1', ',')
    reactioncount = extract_info(body, '"reactioncount":', '//***###-1', ',')

    candeletecommentwithfeedback = extract_info(body, '"candeletecommentwithfeedback":', '//***###-1', ',')
    candeletewithrule = extract_info(body, '"candeletewithrule":', '//***###-1', ',')
    viewercanlike = extract_info(body, '"viewercanlike":', '//***###-1', ',')
    isfeatured = extract_info(body, '"isfeatured":', '//***###-1', ',')
    canremove = extract_info(body, '"canremove":', '//***###-1', ',')
    canreport = extract_info(body, '"canreport":', '//***###-1', ',')
    canedit = extract_info(body, '"canedit":', '//***###-1', ',')
    canmutemembers = extract_info(body, '"canmutemembers":', '//***###-1', ',')
    isauthorbot = extract_info(body, '"isauthorbot":', '//***###-1', ',')
    isauthorweakreference = extract_info(body, '"isauthorweakreference":', '//***###-1', ',')
    isauthororiginalposter = extract_info(body, '"isauthororiginalposter":', '//***###-1', ',')
    isauthornoncoworker = extract_info(body, '"isauthornoncoworker":', '//***###-1', ',')
    istranslatable = extract_info(body, '"istranslatable":', '//***###-1', ',')
    candeleteandblockcommenter = extract_info(body, '"candeleteandblockcommenter":', '//***###-1', ',')
    canviewerremovecommenter = extract_info(body, '"canviewerremovecommenter":', '//***###-1', ',')
    canseeremovecontentoptions = extract_info(body, '"canseeremovecontentoptions":', '//***###-1', ',')
    candeletecommentwithfeedback = extract_info(body, '"candeletecommentwithfeedback":', '//***###-1', ',')
    candeletewithrule = extract_info(body, '"candeletewithrule":', '//***###-1', ',')
    viewercanlike = extract_info(body, '"viewercanlike":', '//***###-1', ',')

    cancomment = extract_info(body, '"cancomment":', '//***###-1', ',')
    spamreplycount = extract_info(body, '"spamreplycount":', '//***###-1', ',')

    canembed = extract_info(body, '"canembed":', '//***###-1', ',')

    canEditConstituentTitle = extract_info(body, '"canEditConstituentTitle":', '//***###-1', ',')
    hasConstituentBadge = extract_info(body, '"hasConstituentBadge":', '//***###-1', ',')
    hasConstituentBadge = extract_info(body, '"hasConstituentBadge":', '//***###-1', ',')

    hasFriendsOnlyBadge = extract_info(body, '"hasFriendsOnlyBadge":', '//***###-1', ',')
    needsApproval = extract_info(body, '"needsApproval":', '//***###-1', ',')
    isApproved = extract_info(body, '"isApproved":', '//***###-1', ',')
    canAppeal = extract_info(body, '"canAppeal":', '//***###-1', ',')
    canSecure = extract_info(body, '"canSecure":', '//***###-1', ',')
    daysToExpire = extract_info(body, '"daysToExpire":', '//***###-1', ',')
    ispinnedbyauthor = extract_info(body, '"ispinnedbyauthor":', '//***###-1', ',')
    canviewerpin = extract_info(body, '"canviewerpin":', '//***###-1', ',')
    canviewerunpin = extract_info(body, '"canviewerunpin":', '//***###-1', ',')
    cansubscribe = extract_info(body, '"cansubscribe":', '//***###-1', ',')
    issubscribed = extract_info(body, '"issubscribed":', '//***###-1', ',')
    canhidetextdelights = extract_info(body, '"canhidetextdelights":', '//***###-1', ',')
    voteEnabled = extract_info(body, '"voteEnabled":', '//***###-1', ',')
    commentVoteUIVersion = extract_info(body, '"commentVoteUIVersion":', '//***###-1', ',')
    displayNegativeVoteCount = extract_info(body, '"displayNegativeVoteCount":', '//***###-1', ',')
    voteLabelVisibleDuration = extract_info(body, '"voteLabelVisibleDuration":', '//***###-1', ',')
    voteArrowNUXV2 = extract_info(body, '"voteArrowNUXV2":', '//***###-1', ',')
    voteFirstArrowNUXV2 = extract_info(body, '"voteFirstArrowNUXV2":', '//***###-1', ',')
    parentcommentid = extract_info(body, '"parentcommentid":', '//***###-1', ',')
    parentcommentauthorid = extract_info(body, '"parentcommentauthorid":', '//***###-1', ',')
    __typename = extract_info(body, '"__typename":', '//***###-1', ',')
    source = extract_info(body, '"source":', '//***###-1', ',')
    highlightcomment = extract_info(body, '"highlightcomment":', '//***###-1', ',')
    scrolltopoffset = extract_info(body, '"scrolltopoffset":', '//***###-1', ',')
    text_delights_are_hidden = extract_info(body, '"text_delights_are_hidden":', '//***###-1', ',')
    privacy_value = extract_info(body, '"privacy_value":', '//***###-1', ',')
    canviewerreact = extract_info(body, '"canviewerreact":', '//***###-1', ',')
    reactioncountmap = extract_info(body, '"reactioncountmap":{', '', '}},')
    reactioncountreduced = extract_info(body, '"reactioncountreduced":', '//***###-1', ',')
    viewerreaction = extract_info(body, '"viewerreaction":', '//***###-1', ',')
    time = extract_info(body, '"time":', '//***###-1', ',')

def comment_extraction():

    print "script: __file__ is", repr(__file__)
    feed_id = []
    page_id = 1222222222222
    pub_id = 1696797043745865
    offset = 0
    key = 0
    comment_count = 100

    h = HTMLParser()
    while key >= 0 :
        file = ''
        fichier = str(page_id)+"_"+str(pub_id)+"_comments_" + str(key) + ".txt"
        with open(fichier, 'rb') as f:
            file = f.readlines()

        file = file[0]
        print file
        key -= 1
        file = traiter_code_html(file)
        #Extract feed

        #src = src.replace("\\", "\\")
        src = ''
        src = traiter_code_html(src)
        print src

        # Les ids des commentaires
        comments_ids = str(file).split('{"comments":[{')
        comments_ids = str(comments_ids[1]).split('],')
        comments_ids = str(comments_ids[0]).split(',')

        # Les reponses des commentaires
        comments_replies = extract_info(str(file) , '"replies":' , ' ','"featuredcommentlists"' )
        #print comments_replies

        """
        # Les corps des commentaires
        bodies = str(file).split('"body":{')
        for bod in bodies:
            if '"text":' in bod:
                #print 'text   ', body
                extraction_comment_body(bod)

        # Les utilisateurs des commentaires
        comments_profiles = extract_info(str(file), '"profiles":', ' ', '"actions"')
        profile_details = str(comments_profiles).split('is_friend')
        for prof in profile_details:
            extraction_profile_info(prof)

        """
        #print " comments_profiles " ,comments_profiles
        id_nbr = 0
        for id in comments_ids:
            id_nbr += 1
            comments_details = str(comments_replies).split(id)
            comments_details = str(comments_details[1]).split(',"ftentidentifier"')
            comments_details = str(comments_details[0]).split('"count":')
            comments_replies_count = str(comments_details[1])
            print id_nbr, "   comments id : ", id, '   ', comments_replies_count



        exit(0)

        item = src.split("_5i_s _8o _8r lfloat _ohe")
        item = item[1]
        user_id = extract_info(item, "user.php?id=", "page.php?id=", "&")
        user_href = extract_info(item, 'href="', "", '"')
        user_nom = extract_info(item, 'aria-label="', "", '"')
        user_nom = h.unescape(user_nom)

        exit(0)
        nbr_feed = 1
        for feed in comments:
            if nbr_feed > 1 :
                feed.replace('&quot;' , '"')
                #print " feed is " ,feed
                #Feed ID
                key1 = "#################"
                post_fbid = extract_info(feed, 'post_fbid:', '"post_fbid":', "}")
                #print post_fbid
                if post_fbid not in feed_id :
                    feed_id.append(post_fbid)
                    #Owner ID
                    ownerid = extract_info(feed, key1 , '"ownerid":', "}")
                    "ownerid"
                    #Owner Name
                    ownerName = extract_info(feed, key1 , '"ownerName":' , ",")
                    "ownerName"
                    #Actor IDs
                    actorIDs = extract_info(feed, key1 , '"actorIDs":' , "],")
                    "actorIDs"
                    #Actor Short Name
                    actorshortname = extract_info(feed, key1 , '"actorshortname":' , ",")
                    "actorshortname"
                    #Actor Name
                    actorname = extract_info(feed, key1 , '"actorname":' , ",")
                    "actorname"
                    #Feed Permalink
                    permalink = extract_info(feed, key1 , '"permalink":' , ",")
                    "permalink"
                    #Recation count reduced
                    reactioncountreduced = extract_info(feed, key1 , '"reactioncountreduced":' , ",")
                    "reactioncountreduced"
                    # Recation count Map
                    reactioncountmap = extract_info(feed, key1 , '"reactioncountmap":', "},")
                    "reactioncountmap"
                    #Supported Reaction
                    supportedreactions = extract_info(feed, key1 , '"supportedreactions":', "],")
                    "supportedreactions"
                    #FeedBack ID
                    feedbackID = extract_info(feed, key1 , '"feedbackID":' , ",")

                    "feedbackID"
                    #Reaction count
                    reactioncount = extract_info(feed, key1 , '"reactioncount":' , ",")
                    "reactioncount"
                    #Like count reduced
                    likecountreduced = extract_info(feed, key1 , '"likecountreduced":' , ",")
                    "likecountreduced"
                    #Like count
                    likecount = extract_info(feed, key1 , '"likecount":' , ",")
                    "likecount"
                    #Viewer ID
                    viewerid = extract_info(feed, key1 , '"viewerid":' , ",")
                    "viewerid"
                    #Target Fb ID
                    targetfbid = extract_info(feed, key1 , '"targetfbid":' , ",")
                    "targetfbid"
                    #Is Public
                    ispublic = extract_info(feed, key1 , '"ispublic":' , ",")
                    "ispublic"
                    #Is owner Page
                    isownerpage = extract_info(feed, key1 , '"isownerpage":' , ",")
                    "isownerpage"
                    #Is ranked
                    isranked = extract_info(feed, key1 , '"isranked":' , ",")
                    "isranked"
                    #Is Factor Fb Entity
                    isActorFBEntity = extract_info(feed, key1 , '"isActorFBEntity":' , ",")
                    "isActorFBEntity"
                    #Is Share
                    isshare = extract_info(feed, key1 , '"isshare":' , ",")
                    "isshare"
                    #Share Fb ID
                    sharefbid = extract_info(feed, key1 , '"sharefbid":' , ",")
                    "sharefbid"
                    #Share Count
                    sharecount = extract_info(feed, key1 , '"sharecount":' , ",")
                    "sharecount"
                    #Seen Count
                    seencount = extract_info(feed, key1 , '"seencount":' , ",")
                    "seencount"
                    #Seen by all : true or false
                    seenbyall = extract_info(feed, key1 , '"seenbyall":' , ",")
                    "seenbyall"
                    #Comnt Id Permalink
                    permalinkcommentid = extract_info(feed, key1 , '"permalinkcommentid":' , ",")
                    "permalinkcommentid"
                    #Default Number Comments To Expand
                    defaultNumCommentsToExpand = extract_info(feed, key1 , '"defaultNumCommentsToExpand":' , ",")
                    "defaultNumCommentsToExpand"
                    #Token
                    token = extract_info(feed, key1 , '"token":' , ",")
                    "token"
                    #Viewer
                    viewer = extract_info(feed, key1 , '"viewer":' , ",")
                    "viewer"
                    #Context ID
                    context_id = extract_info(feed, key1 , '"context_id":' , ",")
                    "context_id"
                    #Last Seen Time
                    lastseentime = extract_info(feed, key1 , '"lastseentime":' , ",")
                    "lastseentime"
                    #Comments target fb id
                    commentstargetfbid = extract_info(feed, key1 , '"commentstargetfbid":' , ",")
                    "commentstargetfbid"
                    #Comment Count
                    commentcount = extract_info(feed, key1 , '"commentcount":' , ",")
                    "commentcount"
                    #Comment Total Count"
                    commentTotalCount = extract_info(feed, key1 , '"commentTotalCount":' , ",")
                    "commentTotalCount"
                    #Comment Count Reduced
                    commentcountreduced = extract_info(feed, key1 , '"commentcountreduced":' , ",")
                    "commentcountreduced"
                    #Are Comments Disabled
                    arecommentsdisabled = extract_info(feed, key1 , '"arecommentsdisabled":' , ",")
                    "arecommentsdisabled"
                    #Actor ID
                    actorid = extract_info(feed, key1 , '"actorid":' , ",")
                    "actorid"
                    #Actor For Post
                    actorforpost = extract_info(feed, key1 , '"actorforpost":' , ",")
                    "actorforpost"
                    #Server Time
                    servertime = extract_info(feed, key1 , '"servertime":' , ",")
                    "servertime"
                    #Featured Comment Lists
                    featuredcommentlists = extract_info(feed, key1 , '"featuredcommentlists":' , ",")
                    "featuredcommentlists"
                    #Share Dialog prompt
                    shareNowMenuURI = extract_info(feed, key1 , '"shareNowMenuURI":' , ",")
                    "shareNowMenuURI"
                    #Share URI
                    shareURI = extract_info(feed, key1 , '"shareURI":' , ",")
                    "shareURI"
                    #Is Live Streaming
                    islivestreaming = extract_info(feed, key1 , '"islivestreaming":' , ",")
                    "islivestreaming"
                    #is Sponsored
                    isSponsored = extract_info(feed, key1 , '"isSponsored":' , ",")
                    "isSponsored"
                    #feed Location Type
                    feedLocationType = extract_info(feed, key1 , '"feedLocationType":' , ",")
                    "feedLocationType"
                    #mall_how_many_post_comments
                    mall_how_many_post_comments = extract_info(feed, key1 , '"mall_how_many_post_comments":' , ",")
                    "mall_how_many_post_comments"
                    #interacted Story Type
                    interacted_story_type = extract_info(feed, key1 , '"interacted_story_type":' , ",")
                    "interacted_story_type"

                    "parentcommentid"




                #print " -------------------------------------------------------------------- "
                #print " feed  ",feed
                #exit(0)
            nbr_feed += 1
        print " -------------------------------------------------------------------- "
    print " nbr feeds ", len(feed_id)

def comment_reaction_extraction():

    #exit(0)
    nbr_file = 0
    caps = DesiredCapabilities.CHROME
    caps['loggingPrefs'] = {'performance': 'ALL'}
    header = {"user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver,chrome_options=chrome_options,desired_capabilities=caps)
    #Authentification(driver,usr_pwd[0][0],usr_pwd[0][1])
    driver.get("http://www.facebook.com")
    time.sleep(2)
    elem = driver.find_element_by_id("email")
    elem.send_keys("sanasara265@gmail.com")

    elem = driver.find_element_by_id("pass")
    elem.send_keys("123456789@1")

    elem.send_keys(Keys.RETURN)
    time.sleep(5)

    pub_id = '1665205483571688'
    reaction_map = {}
    reaction_map['1'] = '58'
    reaction_map['4'] = '10'
    for key in reaction_map:
        print key, "  ", reaction_map[key]

    after_url = ''
    for entry in driver.get_log('performance'):
            timeline = str(entry)
            if '?modules=ChatSidebarSheet.react' in timeline:
                print " timeline : " , timeline
                after_url = timeline.split('?modules=ChatSidebarSheet.react')
                after_url = after_url[1].split('"}')
                after_url = after_url[0]
                print " after_url  ",after_url
                break

    for key in reaction_map:
                    print key, "  ", reaction_map[key]
                    url = "https://www.facebook.com/ufi/reaction/profile/browser/fetch/?limit=" + reaction_map[key]  + \
                      "&reaction_type=" + key + \
                      "&total_count=" + reaction_map[key] + \
                      "&ft_ent_identifier=" + str(pub_id) + str(after_url)

                    print " url is " , url
                    driver.get(url)
                    time.sleep(3)

def export_comments():
    caps = DesiredCapabilities.CHROME
    caps['loggingPrefs'] = {'performance': 'ALL'}
    header = {
        "user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options, desired_capabilities=caps)
    # Authentification(driver,usr_pwd[0][0],usr_pwd[0][1])
    usr = usr_pwd[0]
    after_url = usr[5]
    print after_url

    Authentification(driver, usr[0], usr[1])

    page_id = 1222222222222
    pub_id = 1696797043745865
    offset = 0
    key = 0
    comment_count = 100

    while offset <= comment_count :
        url_to_comments = 'https://www.facebook.com/ajax/ufi/comment_fetch.php?ft_ent_identifier=' + str(
            pub_id) + '&offset=' + str(offset) + '&length=50'+after_url
        print offset, "  ", key , "    ",url_to_comments
        driver.get(url_to_comments)
        src = driver.page_source
        ecriture_fichier2(str(page_id)+"_"+str(pub_id)+"_comments_" + str(key) + ".txt", src)
        key += 1
        offset += 50
        time.sleep(5)

    driver.get("https://www.facebook.com")
    deconnexion_user_without_driver(driver)
    driver.close()
    driver.quit()
    exit(0)

def traiter_code_html(src):
    src = src.replace("amp;", "")
    src = src.replace("&quot;", '"')
    src = src.replace("&#125;", "}")
    src = src.replace("&#123;", "{")
    src = src.replace("\u005c", "\\")
    src = src.replace("\\", '')
    src = src.replace("\/", "/")
    src = src.replace("&gt;", ">")
    src = src.replace("u003C", "<")
    src = src.replace("u0025", "%")
    src = src.replace("\%", "%")
    return src

def HTMLEntitiesToUnicode(text):
    """Converts HTML entities to unicode.  For example '&amp;' becomes '&'."""
    text = unicode(BeautifulStoneSoup(text, convertEntities=BeautifulStoneSoup.ALL_ENTITIES))
    return text

def unicodeToHTMLEntities(text):
    """Converts unicode to HTML entities.  For example '&' becomes '&amp;'."""
    text = cgi.escape(text).encode('ascii', 'xmlcharrefreplace')
    return text

if __name__ == '__main__':

    after_url_detection()
    #export_comments()
    comment_extraction()
    #comment_reaction_extraction()

