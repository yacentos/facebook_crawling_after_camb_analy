from _0_Functions import *


def comment_tags(first_sql, file, usr, pwd,  nom, lock, fin=0):
    connection = connect_db()
    cursor = connection.cursor()
    post = ''
    next_feeds = ''
    comments = ''
    comments_next = ''
    comment2 = ''
    comments2 = ''
    comments_next2 = ''
    message_tags = ''
    post_id = ''
    feeds = ''
    message_tags2 = ''
    name_message = ''

    graph = ''
    count = 0
    json_data = 0
    cursor.execute(first_sql)
    groups = cursor.fetchall()

    for group in groups:
        id_group = group[0]
        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, file, lock)
            if json_data == 100:
                print json_data, "  au suivant "
            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = remplacer_limit(json_data['paging']['next'])
        else:
            post_id = str(id_group) + '?fields=feed{comments{message_tags,id,comments{message_tags,id,from},from}}'
            print " post id ", post_id
            graph = retourner_graph(file, usr, pwd, lock)
            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " post id error ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100
            if 'feed' in post and 'data' in post['feed']:
                feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                next_feeds = remplacer_limit(post['feed']['paging']['next'])

        while feeds <> '':
            for feed in feeds:
                if 'comments' in feed:
                    if 'data' in feed['comments']:
                        comments = feed['comments']['data']
                    if 'paging' in feed['comments'] and 'next' in feed['comments']['paging']:
                        comments_next = remplacer_limit(feed['comments']['paging']['next'])
                    while comments <> '':
                        for comment in comments:
                            # print " commm  111", comment
                            # time.sleep(3)
                            if 'message_tags' in comment:
                                message_tags = comment['message_tags']
                                nom_from = 'need_search'
                                id_from = 0
                                if "from" in comment:
                                    if 'name' in comment['from']:
                                        nom_from = comment['from']['name']
                                    if "id" in comment['from']:
                                        id_from = comment['from']['id']
                                for message in message_tags:
                                    # print " messsage  ", message
                                    # time.sleep(3)

                                    if 'name' in message:
                                        name_message = message['name']
                                    else:
                                        name_message = 'Link'
                                    idcomm = comment['id'].split('_', 1)
                                    if len(idcomm) > 1:
                                        idcom = idcomm[1]
                                    else:
                                        idcom = idcomm[0]
                                    count += 1
                                    ajout_tag('message', message['id'], idcom, 'c', cursor, name_message, nom_from,
                                                   id_from, connection,
                                                   count, id_group, nom + "tag_in_comment")
                                    # time.sleep(1500)
                            if 'comments' in comment:
                                if 'data' in comment['comments']:
                                    comments2 = comment['comments']['data']
                                if 'paging' in comment['comments'] and 'next' in comment['comments']['paging']:
                                    comments_next2 = remplacer_limit(comment['comments']['paging']['next'])
                                # print " commm 2  ", comments2
                                # time.sleep(5)
                                while comments2 <> '':
                                    for comment2 in comments2:
                                        if 'message_tags' in comment2:
                                            message_tags2 = comment2['message_tags']
                                            nom_from2 = 'need_search'
                                            id_from2 = 0
                                            if "from" in comment2:
                                                if 'name' in comment2['from']:
                                                    nom_from2 = comment2['from']['name']
                                                if "id" in comment2['from']:
                                                    id_from2 = comment2['from']['id']
                                            for message2 in message_tags2:
                                                if 'name' in message2:
                                                    name_message = message2['name']
                                                idcomm = comment2['id'].split('_', 1)
                                                if len(idcomm) > 1:
                                                    idcom = idcomm[1]
                                                else:
                                                    idcom = idcomm[0]

                                                count += 1
                                                ajout_tag('message', message2['id'], idcom, 'c', cursor,
                                                               name_message, nom_from2, id_from2, connection,
                                                               count, id_group, nom + "tag_in_sous_comment")

                                                # connection.commit()
                                    comments2 = ''
                                    if comments_next2 <> '':
                                        json_data = url_Suivant(comments_next2, usr, pwd, file, lock)
                                        comments_next2 = ''
                                        if json_data == 100:
                                            print json_data, "  au suivant "
                                        else:
                                            if 'data' in json_data:
                                                comments2 = json_data['data']
                                            if 'paging' in json_data and 'next' in json_data['paging']:
                                                comments_next2 = remplacer_limit(json_data['paging']['next'])
                                                #update_next(id_group, 'comments_tags_next_feeds', comments_next2, cursor, connection, count, nom)
                        comments = ''
                        if comments_next <> '' and json_data <> 100 :

                            json_data = url_Suivant(comments_next, usr, pwd, file, lock)
                            comments_next = ''
                            if json_data == 100:
                                print json_data, "  au suivant "
                            else:
                                if 'data' in json_data:
                                    comments = json_data['data']
                                if 'paging' in json_data and 'next' in json_data['paging']:
                                    comments_next = remplacer_limit(json_data['paging']['next'])
                                    #update_next(id_group, 'comments_tags_next_feeds', comments_next, cursor, connection, count, nom)
            feeds = ''
            if next_feeds <> '' and json_data <> 100 :
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if json_data == 100:
                    print json_data, "  au suivant "
                else:
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = remplacer_limit(json_data['paging']['next'])
                        update_next(id_group, 'comments_tags_next_feeds', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']

        if json_data <> 100:
            sql = " update nexts set comments_tags_next_feeds = '1'   where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break
    connection.close()


def lancer_comment_tags():
    nbr = 0

    usr_p = [("ali.boulmi@gmail.com", "123456789@1", "../Token/ali.boulmi"),
           ("inouflmsti@gmail.com", "123456789@1 ", "../Token/inouflmsti")]

    sql2 = " select idgroup,comments_tags_next_feeds from groups , nexts where idNexts=idgroup " \
           "and( comments_tags_next_feeds <> '1') "

    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()
    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=comment_tags,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread0 " + str(nbr), lock))
            thread_1.start()
            thread_1.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=comment_tags,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=comment_tags,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            nbr = 0
    else:
        os._exit(0)


if __name__ == "__main__":
    lancer_comment_tags()

    while SystemExit:
        lancer_comment_tags()
