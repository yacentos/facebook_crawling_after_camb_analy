from _0_Functions import *


def partage(first_sql, file, usr, pwd,  nom, lock, fin=0):
    connection = connect_db()
    cursor = connection.cursor()

    graph = ''
    json_data = 0
    print first_sql
    cursor.execute(first_sql)
    groups = cursor.fetchall()

    for group in groups:
        count = 0
        id_group = group[0]
        print id_group
        next_feeds = ''
        feeds = ''
        story_tags = ''
        story_tags_next = ''
        cle = "sharedposts"

        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, file, lock)
            if json_data == 100:
                print json_data, "  au suivant "
            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
        else:
            post_id = str(
                id_group) + '?fields=feed{sharedposts{id,parent_id,created_time,name,from,story},created_time,shares}'
            graph = retourner_graph(file, usr, pwd, lock)
            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " errooor  : ", ex
                sql3 = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(id_group)
                cursor.execute(sql3)
                connection.commit()
                json_data = 100
                feeds = ''
                next_feeds = ""
            if 'feed' in post and 'data' in post['feed']:
                    feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                    next_feeds = post['feed']['paging']['next']

        while feeds <> '':
            for feed in feeds:
                if cle in feed:
                    fe_id = feed['id']
                    fe_id = fe_id.split('_', 1)
                    id_group_original = id_group
                    idfeed_original = fe_id[1]
                    created_time_original = uniix_time(feed['created_time'])
                    shares_count = -1
                    if 'shares' in feed:
                        shares_count = feed['shares']['count']
                    story_tags = feed[cle]['data']
                    if 'next' in feed[cle]:
                        story_tags_next = feed[cle]['next']

                    while story_tags <> '':
                        for tag in story_tags:
                            created_time_destination = uniix_time(tag['created_time'])
                            disapear_time = ''
                            dest_id = tag['id']
                            dest_id = dest_id.split('_', 1)
                            id_group_destination = dest_id[0]
                            idfeed_destination = dest_id[1]
                            parent_id = ''
                            story = ''
                            if 'parent_id' in tag:
                                parent_id = tag['parent_id']
                            if 'story' in tag:
                                story = remplacer_quotes(tag['story'])
                            count += 1
                            ajout_share(id_group_original, idfeed_original, created_time_original,
                                        created_time_destination, disapear_time, id_group_destination,
                                        idfeed_destination, parent_id, story, shares_count, cursor, connection, count,
                                        idfeed_original, nom)
                            connection.commit()
                        story_tags = ''

                        if story_tags_next <> '':
                            json_data = url_Suivant(story_tags_next, usr, pwd, file, lock)
                            story_tags_next = ''
                            if json_data == 100:
                                print json_data, "  au suivant "
                            else:
                                if 'data' in json_data:
                                    story_tags = json_data['data']
                                    print " wiiiiith  ", story_tags
                                if 'paging' in json_data and 'next' in json_data['paging']:
                                    story_tags_next = json_data['paging']['next']
                                    print story_tags_next
                                    update_next(id_group, 'story_tags_next_feeds', story_tags_next, cursor, connection,
                                                count, nom)
                                    # ajout_next(id_group, 'with_tags_next', with_tags_next, cursor)
            feeds = ''
            if next_feeds <> '':
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if json_data == 100:
                    print json_data, "  au suivant "
                else:
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = json_data['paging']['next']
                        # print next_feeds
                        update_next(id_group, 'story_tags_next_feeds', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']
                        # print feeds
        if json_data <> 100:
            sql = " update nexts set story_tags_next_feeds = 1   where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break

    connection.close()


def lancer_partage():
    nbr = 0

    usr_p = [("sanasara265@gmail.com", "123456789abcd", "../Token/sanasara265","1-1-1988"),
            ("bazzihozzi@gmail.com", "123456789@1", "../Token/bazzihozzi","1-1-1988")]

    sql2 = " select idgroup,story_tags_next_feeds from groups  , nexts where idNexts=idgroup  " \
           " and (  story_tags_next_feeds <> '1') "
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()

    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=partage,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread0 " + str(nbr), lock))
            thread_1.start()
            thread_1.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=partage,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=partage,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    else:
        os._exit(0)

if __name__ == "__main__":
    lancer_partage()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_partage()
