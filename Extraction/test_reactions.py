# -*- coding: utf-8 -*-
import pickle
import time
import urllib
from BeautifulSoup import BeautifulStoneSoup
import cgi
from HTMLParser import HTMLParser
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
#chromedriver = "D:\chromedriver.exe"
import json
import requests
from _0_Functions import  *
import urllib, urllib2, cookielib, getpass
chromedriver = "E:\chromedriver.exe"

def ecriture_fichier2(nom_fic, val):
    # print " fichier ",nom_fic
    with open(nom_fic, 'wb') as f:
        pickle.dump(val, f)
        f.close()

def nbr_feeds(source):
    nbr = 0
    timeline = source.split('feedcontext')
    nbr = len(timeline)
    if len(timeline) > 1:
        return len(timeline)-1
    else:
        return 0

def face_json_reactions():

    #exit(0)
    nbr_file = 0
    driver = Configurer_driver()

    usr = usr_pwd[4]
    email = usr[0]
    pwd = usr[1]
    after_url = usr[5]

    driver = Authentification(driver,email,pwd)
    driver.get("http://www.facebook.com")
    time.sleep(2)

    entity_id = '11111111111111'
    pub_id = '1665205483571688'
    reaction_map = {}
    reaction_map['1'] = '58'
    reaction_map['4'] = '10'
    for key in reaction_map:
        print key, "  ", reaction_map[key]
    """
    after_url = ''
    for entry in driver.get_log('performance'):
            timeline = str(entry)
            if '?modules=ChatSidebarSheet.react' in timeline:
                print " timeline : " , timeline
                after_url = timeline.split('?modules=ChatSidebarSheet.react')
                after_url = after_url[1].split('"}')
                after_url = after_url[0]
                print " after_url  ",after_url
                break
    """

    for key in reaction_map:
                    print key, "  ", reaction_map[key]
                    url = "https://www.facebook.com/ufi/reaction/profile/browser/fetch/?limit=" + reaction_map[key]  + \
                      "&reaction_type=" + key + \
                      "&total_count=" + reaction_map[key] + \
                      "&ft_ent_identifier=" + str(pub_id) + str(after_url)
                    driver.get(url)
                    time.sleep(3)
                    ecriture_fichier2(str(entity_id)+"_"+str(pub_id)+"_reaction_" +key+".txt", driver.page_source)

def extract_info(feed_item , key1 , key2 , delimeter):
    key_value = -1
    feed_key_exist = feed_item.split(key1)
    if len(feed_key_exist) > 1:
        key_exist = feed_key_exist[1].split(delimeter)
        key_value = key_exist[0]
        print key1, " : ", key_value, " ## " ,
    else:
        feed_key_exist = feed_item.split(key2)
        if len(feed_key_exist) > 1:
            key_exist = feed_key_exist[1].split(delimeter)
            key_value = key_exist[0]
        print key2, " : ", key_value, " ## " ,
    return key_value

def extraction():

    print "script: __file__ is", repr(__file__)
    feed_id = []

    nbr_file = 7
    entity_id = '11111111111111'
    pub_id = '1665205483571688'

    reaction_map = {}
    reaction_map['1'] = '58'
    reaction_map['4'] = '10'

    h = HTMLParser()
    for key in reaction_map:
        print key, "  ", reaction_map[key]
        fichier = str(entity_id)+"_"+str(pub_id)+"_reaction_" +key+".txt"

        with open(fichier, 'rb') as f:
            file = f.readlines()
        nbr_file -= 1
        #Extract feed
        engagements = str(file).split("engagement")
        print fichier , "   feeds   ",len(engagements)
        src = engagements[1]
        #src = src.replace("\\", "\\")

        src = traiter_code_html(src)
        print src
        exit(0)

        item = src.split("_5i_s _8o _8r lfloat _ohe")
        item = item[1]
        user_id = extract_info(item, "user.php?id=", "page.php?id=", "&")
        user_href = extract_info(item, 'href="', "", '"')
        user_nom = extract_info(item, 'aria-label="', "", '"')
        user_nom = h.unescape(user_nom)

        exit(0)
        nbr_feed = 1
        for feed in engagements:
            if nbr_feed > 1 :
                feed.replace('&quot;' , '"')
                #print " feed is " ,feed
                #Feed ID
                key1 = "####1##0##2##3##5##8##9#"
                post_fbid = extract_info(feed, 'post_fbid:', '"post_fbid":', "}")
                #print post_fbid
                if post_fbid not in feed_id :
                    feed_id.append(post_fbid)
                    #Owner ID
                    ownerid = extract_info(feed, key1 , '"ownerid":', "}")
                    "ownerid"
                    #Owner Name
                    ownerName = extract_info(feed, key1 , '"ownerName":' , ",")
                    "ownerName"
                    #Actor IDs
                    actorIDs = extract_info(feed, key1 , '"actorIDs":' , "],")
                    "actorIDs"
                    #Actor Short Name
                    actorshortname = extract_info(feed, key1 , '"actorshortname":' , ",")
                    "actorshortname"
                    #Actor Name
                    actorname = extract_info(feed, key1 , '"actorname":' , ",")
                    "actorname"
                    #Feed Permalink
                    permalink = extract_info(feed, key1 , '"permalink":' , ",")
                    "permalink"
                    #Recation count reduced
                    reactioncountreduced = extract_info(feed, key1 , '"reactioncountreduced":' , ",")
                    "reactioncountreduced"
                    # Recation count Map
                    reactioncountmap = extract_info(feed, key1 , '"reactioncountmap":', "},")
                    "reactioncountmap"
                    #Supported Reaction
                    supportedreactions = extract_info(feed, key1 , '"supportedreactions":', "],")
                    "supportedreactions"
                    #FeedBack ID
                    feedbackID = extract_info(feed, key1 , '"feedbackID":' , ",")

                    "feedbackID"
                    #Reaction count
                    reactioncount = extract_info(feed, key1 , '"reactioncount":' , ",")
                    "reactioncount"
                    #Like count reduced
                    likecountreduced = extract_info(feed, key1 , '"likecountreduced":' , ",")
                    "likecountreduced"
                    #Like count
                    likecount = extract_info(feed, key1 , '"likecount":' , ",")
                    "likecount"
                    #Viewer ID
                    viewerid = extract_info(feed, key1 , '"viewerid":' , ",")
                    "viewerid"
                    #Target Fb ID
                    targetfbid = extract_info(feed, key1 , '"targetfbid":' , ",")
                    "targetfbid"
                    #Is Public
                    ispublic = extract_info(feed, key1 , '"ispublic":' , ",")
                    "ispublic"
                    #Is owner Page
                    isownerpage = extract_info(feed, key1 , '"isownerpage":' , ",")
                    "isownerpage"
                    #Is ranked
                    isranked = extract_info(feed, key1 , '"isranked":' , ",")
                    "isranked"
                    #Is Factor Fb Entity
                    isActorFBEntity = extract_info(feed, key1 , '"isActorFBEntity":' , ",")
                    "isActorFBEntity"
                    #Is Share
                    isshare = extract_info(feed, key1 , '"isshare":' , ",")
                    "isshare"
                    #Share Fb ID
                    sharefbid = extract_info(feed, key1 , '"sharefbid":' , ",")
                    "sharefbid"
                    #Share Count
                    sharecount = extract_info(feed, key1 , '"sharecount":' , ",")
                    "sharecount"
                    #Seen Count
                    seencount = extract_info(feed, key1 , '"seencount":' , ",")
                    "seencount"
                    #Seen by all : true or false
                    seenbyall = extract_info(feed, key1 , '"seenbyall":' , ",")
                    "seenbyall"
                    #Comnt Id Permalink
                    permalinkcommentid = extract_info(feed, key1 , '"permalinkcommentid":' , ",")
                    "permalinkcommentid"
                    #Default Number Comments To Expand
                    defaultNumCommentsToExpand = extract_info(feed, key1 , '"defaultNumCommentsToExpand":' , ",")
                    "defaultNumCommentsToExpand"
                    #Token
                    token = extract_info(feed, key1 , '"token":' , ",")
                    "token"
                    #Viewer
                    viewer = extract_info(feed, key1 , '"viewer":' , ",")
                    "viewer"
                    #Context ID
                    context_id = extract_info(feed, key1 , '"context_id":' , ",")
                    "context_id"
                    #Last Seen Time
                    lastseentime = extract_info(feed, key1 , '"lastseentime":' , ",")
                    "lastseentime"
                    #Comments target fb id
                    commentstargetfbid = extract_info(feed, key1 , '"commentstargetfbid":' , ",")
                    "commentstargetfbid"
                    #Comment Count
                    commentcount = extract_info(feed, key1 , '"commentcount":' , ",")
                    "commentcount"
                    #Comment Total Count"
                    commentTotalCount = extract_info(feed, key1 , '"commentTotalCount":' , ",")
                    "commentTotalCount"
                    #Comment Count Reduced
                    commentcountreduced = extract_info(feed, key1 , '"commentcountreduced":' , ",")
                    "commentcountreduced"
                    #Are Comments Disabled
                    arecommentsdisabled = extract_info(feed, key1 , '"arecommentsdisabled":' , ",")
                    "arecommentsdisabled"
                    #Actor ID
                    actorid = extract_info(feed, key1 , '"actorid":' , ",")
                    "actorid"
                    #Actor For Post
                    actorforpost = extract_info(feed, key1 , '"actorforpost":' , ",")
                    "actorforpost"
                    #Server Time
                    servertime = extract_info(feed, key1 , '"servertime":' , ",")
                    "servertime"
                    #Featured Comment Lists
                    featuredcommentlists = extract_info(feed, key1 , '"featuredcommentlists":' , ",")
                    "featuredcommentlists"
                    #Share Dialog prompt
                    shareNowMenuURI = extract_info(feed, key1 , '"shareNowMenuURI":' , ",")
                    "shareNowMenuURI"
                    #Share URI
                    shareURI = extract_info(feed, key1 , '"shareURI":' , ",")
                    "shareURI"
                    #Is Live Streaming
                    islivestreaming = extract_info(feed, key1 , '"islivestreaming":' , ",")
                    "islivestreaming"
                    #is Sponsored
                    isSponsored = extract_info(feed, key1 , '"isSponsored":' , ",")
                    "isSponsored"
                    #feed Location Type
                    feedLocationType = extract_info(feed, key1 , '"feedLocationType":' , ",")
                    "feedLocationType"
                    #mall_how_many_post_comments
                    mall_how_many_post_comments = extract_info(feed, key1 , '"mall_how_many_post_comments":' , ",")
                    "mall_how_many_post_comments"
                    #interacted Story Type
                    interacted_story_type = extract_info(feed, key1 , '"interacted_story_type":' , ",")
                    "interacted_story_type"

                    "parentcommentid"




                #print " -------------------------------------------------------------------- "
                #print " feed  ",feed
                #exit(0)
            nbr_feed += 1
        print " -------------------------------------------------------------------- "
    print " nbr feeds ", len(feed_id)

def traiter_code_html(src):
    src = src.replace("amp;", "")
    src = src.replace("&quot;", '"')
    src = src.replace("&#125;", "}")
    src = src.replace("&#123;", "{")
    src = src.replace("\u005c", "\\")
    src = src.replace("\\", '')
    src = src.replace("\/", "/")
    src = src.replace("&gt;", ">")
    src = src.replace("u003C", "<")
    src = src.replace("u0025", "%")
    src = src.replace("\%", "%")
    return src

def HTMLEntitiesToUnicode(text):
    """Converts HTML entities to unicode.  For example '&amp;' becomes '&'."""
    text = unicode(BeautifulStoneSoup(text, convertEntities=BeautifulStoneSoup.ALL_ENTITIES))
    return text

def unicodeToHTMLEntities(text):
    """Converts unicode to HTML entities.  For example '&' becomes '&amp;'."""
    text = cgi.escape(text).encode('ascii', 'xmlcharrefreplace')
    return text

if __name__ == '__main__':

    face_json_reactions()
    extraction()
    h = HTMLParser()
    print(h.unescape("fff"))
    exit(0)










