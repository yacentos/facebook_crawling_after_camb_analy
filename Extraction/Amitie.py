from _0_Functions import *

def publication():

    connection = connect_db()
    cursor = connection.cursor()
    next_friends = ''
    friends = ''
    name = ""
    story = ""
    message = ""
    shares = 0
    count = 0
    id_user = '0'
    graph = ''
    rupture = 1
    # type Amitie = 1 ==> Pas encore traite
    # type Amitie = 2 ==> Utilisateur Muet
    # type Amitie = 3 ==> Utilisateur Bavard
    # type Amitie = 4 ==> Utilisateur .....
    sql = " select idActeurFace,next_friends from acteur where next_friends is null or  next_friends <> '1' order by idActeurFace"# DESC"  # like ' ')" #where type_amitie = 1 #where idActeurFace=1705107
    print (" Sql ",sql)
    cursor.execute(sql)
    users = cursor.fetchall()
    print " nombre d'utilisateur = ", len(users) #nombre d'utilisateur =  3671709
    for user in users:
        id_user = user[0]
        print id_user, " #### ", user[0], " lll ", user[1]
        if len(user[1]) > 10:
            json_data = url_Suivant(user[1])
            if 'data' in json_data:
                friends = json_data['data']
            if 'paging' in json_data and 'next' in json_data['paging']:
                next_friends = remplacer_limit(json_data['paging']['next'])
        else:
            post_id = str(id_user) + '?fields=friends{name,work,locale,birthday,id,timezone,posts{message,story,created_time,id,updated_time}}'
            print post_id
            graph = retourner_graph(f, usr, pwd, lock)
            print graph
            try:
                post = graph.get_object(id=post_id)
                if 'friends' in post and 'data' in post['friends']:
                    friends = post['friends']['data']
                if 'friends' in post and 'paging' in post['friends'] and 'next' in post['friends']['paging']:
                    next_friends = remplacer_limit(post['friend']['paging']['next'])
            except Exception as e:
                dt = datetime.datetime.now()
                disap_time = calendar.timegm(dt.utctimetuple())
                sql = "update acteur set disapear_time='" + str(disap_time) + "' where idActeurFace=" + str(id_user)
                print sql
                cursor.execute(sql)
                print " ID introuvable :  ", e
        print next_friends,"  ===>  ",friends
        while friends != '':
                for friend in friends:
                    count += 1

                    if 'name' in friend:
                        name_friend     = remplacer_quotes(friend['name'])
                    if 'locale' in friend:
                        local_friend     = remplacer_quotes(friend['locale'])
                    if 'id' in friend:
                        id_friend     = remplacer_quotes(friend['id'])

                    ajout_acteur(id_friend, name_friend, cursor , connection,count )
                    ajout_ami(id_user , id_friend)

                    if 'posts' in friend  and 'data' in post['friends']['posts'] :
                        posts = post['friends']['posts']
                    if 'paging' in posts and 'next' in posts['paging']:
                        next_posts = remplacer_limit(posts['paging']['next'])

                    while posts != '':
                        for p in posts:
                            created_time = uniix_time(p['created_time'])
                            updated_time = uniix_time(p['updated_time'])
                            if 'shares' in p:
                                shares   = p['shares']['count']
                            if 'message' in p:
                                message   = remplacer_quotes( p['message'] )
                            if 'story' in p:
                                story = remplacer_quotes( p['story'] )
                            idpost = friend['id'].split('_', 1)

                            ajout_next(idpost[1],cursor, connection, count)
                            ajout_post(idpost[1], message,story, created_time, updated_time, shares, idpost[0],cursor, connection, count)

                        posts = ''
                        if next_posts <> '':
                            print next_posts
                            json_data = url_Suivant(next_posts)
                            next_posts = ''
                            if 'paging' in json_data and 'next' in json_data['paging']:
                                next_posts = remplacer_limit(json_data['paging']['next'])
                                update_next(idpost[1], 'next_posts', next_posts, cursor, connection, count)
                            if 'data' in json_data:
                                posts = json_data['data']
                #connection.commit()
                friends = ''
                if next_friends <> '':
                    print next_friends
                    json_data = url_Suivant(next_friends)
                    next_friends = ''
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_friends = remplacer_limit(json_data['paging']['next'])
                        update_acteur_next(id_user, 'next_friends', next_friends, cursor,connection,count)
                    if 'data' in json_data:
                        friends = json_data['data']
        sql = " update acteur set next_friends = '1'   where idActeurFace=" + str(id_user)
        print sql
        cursor.execute(sql)
    connection.commit()
    connection.close()

if __name__ == "__main__":
    publication()