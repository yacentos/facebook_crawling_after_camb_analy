from _0_Functions import *
import _0_Groups


def partage_profondeur(sql, file, usr, pwd,  nom, lock):
    connection = connect_db()
    cursor = connection.cursor()
    graph = ''
    print sql
    cursor.execute(sql)
    groups = cursor.fetchall()

    for group in groups:

        count = 0
        id_cherche = str(group[0]) + "_" + str(group[1])
        id_group = group[0]
        print id_group
        next_feeds = ''
        feeds = ''
        story_tags = ''
        story_tags_next = ''
        cle = "sharedposts"

        if True:
            post_id = str(id_cherche) + '?fields=sharedposts{id,parent_id,created_time,name,from,story}'
            print "poooooost id ", post_id
            try:
                graph = retourner_graph2(post_id, lock)
                post = graph.get_object(id=post_id)
                if cle in post and 'data' in post[cle]:
                    feeds = post[cle]['data']
                if cle in post and 'paging' in post[cle] and 'next' in post[cle]['paging']:
                    next_feeds = post[cle]['paging']['next']

            except Exception as e:
                dt = datetime.datetime.now()
                disap_time = calendar.timegm(dt.utctimetuple())
                sql = "update groups set Disapear_time='" + str(disap_time) + "' where idGroup=" + str(id_group)
                # print sql
                # cursor.execute(sql)
                print " ID introuvable :  ", e
        print " feeds", feeds
        while feeds <> '':

            id_group_original = group[0]
            idfeed_original = group[1]
            created_time_original = group[3]
            for feed in feeds:
                created_time_destination = uniix_time(feed['created_time'])
                disapear_time = ''
                dest_id = feed['id']
                dest_id = dest_id.split('_', 1)
                id_group_destination = dest_id[0]
                idfeed_destination = dest_id[1]
                parent_id = ''
                story = ''
                if 'parent_id' in feed:
                    parent_id = feed['parent_id']
                if 'story' in feed:
                    story = remplacer_quotes(feed['story'])
                count += 1
                # print "ffed  ",feed
                # print "story  ", story_tags
                ajout_share_prof(id_group_original, idfeed_original, created_time_original,
                                 created_time_destination, disapear_time, id_group_destination,
                                 idfeed_destination, parent_id, story, cursor, connection, count, group[2])
                connection.commit()
                # ajout_next(id_group, 'with_tags_next', with_tags_next, cursor)
            feeds = ''
            if next_feeds <> '':
                json_data = url_Suivant2(next_feeds)
                next_feeds = ''
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
                    # print next_feeds
                    # update_next(id_group, 'story_tags_next_feeds', next_feeds, cursor, connection,count)
                if 'data' in json_data:
                    feeds = json_data['data']
                    # print feeds
        sql = " update shares set traite = 3   where id_group_destination=" + str(group[0]) + \
              " and idfeed_destination=" + str(group[1])
        print " update sql ", sql
        cursor.execute(sql)
        connection.commit()
    connection.close()


def lancer_partage_profondeur():
    nbr = 0

    usr_p = [("inouflmsti@gmail.com", "@msti@2017", "inouflmsti"),
             ("crawling.user2@gmail.com", "123456789@1", "crawling.user2")]

    sql1 = " select id_group_destination,idfeed_destination,id_etude_share,created_time_original " \
          " from shares where parent_id = '' and traite <> 3 order by idfeed_destination ASC "

    sql2 = " select id_group_destination,idfeed_destination,id_etude_share,created_time_original " \
          " from shares where parent_id = '' and traite <> 3 order by idfeed_destination DESC "

    lock = threading.Lock()
    try:
        usr_c = usr_p[0]

        print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
        thread_1 = threading.Thread(target=partage_profondeur,
                                    args=(sql2, usr_c[2], usr_c[0], usr_c[1],  "thread1 " + str(nbr), lock))
        thread_1.start()
        nbr += 1
        usr_c = usr_p[1]
        print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
        thread_2 = threading.Thread(target=partage_profondeur,
                                    args=(sql1, usr_c[2], usr_c[0], usr_c[1],  "thread2 " + str(nbr), lock))
        thread_2.start()

        thread_1.join()
        thread_2.join()

    except SystemExit:
        print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
        # time.sleep(10)
        nbr = 0


if __name__ == "__main__":
    lancer_partage_profondeur()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_partage_profondeur()


