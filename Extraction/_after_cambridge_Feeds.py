from _0_Functions import *


def publication(first_sql, f, usr, pwd,  nom, lock, fin=0):

    connection = connect_db()
    cursor = connection.cursor()

    next_feeds = ''
    feeds = ''
    name = ""
    story = ""
    message = ""
    shares = 0
    count = 0
    post = ''
    json_data = 0
    id_group = '0'
    graph = ''

    cursor.execute(first_sql)
    groups = cursor.fetchall()
    print first_sql, " Nombre ", len(groups)

    for group in groups:

        feeds = ''
        id_group = group[0]
        print id_group, " (", len(group[1]), " ) ", group[1]

        if len(group[1]) > 10:

            json_data = url_Suivant(group[1], usr, pwd, f,  lock)
            if json_data == 100:
                print " Au suivant ", json_data
            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = remplacer_limit(json_data['paging']['next'])
        else:

            post_id = str(id_group) + '?fields=feed{id,message,story,updated_time,created_time,type,shares,from,name,description}'
            print " Recherche des pubs : ", post_id
            graph = retourner_graph(f, usr, pwd, lock)

            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " Post error : ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "' where idNexts=" + str(id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100

            if 'feed' in post and 'data' in post['feed']:
                    feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                    next_feeds = remplacer_limit(post['feed']['paging']['next'])

        while feeds != '':
            for feed in feeds:

                    updated_time = uniix_time(feed['updated_time'])
                    created_time = uniix_time(feed['created_time'])

                    count += 1

                    from_id = 0
                    if 'from' in feed and 'id' in feed['from'] :
                        from_id = feed['from']['id']

                    from_name = ''
                    if 'from' in feed and 'name' in feed['from']:
                        from_name = remplacer_quotes(feed['from']['name'])

                    ajout_acteur(from_id, from_name, cursor, connection, count, nom)

                    idFeed1 = feed['id'].split('_', 1)

                    if 'name' in feed:
                        name = remplacer_quotes(feed['name'])
                    if 'shares' in feed:
                        shares = feed['shares']['count']
                    if 'description' in feed:
                        description = remplacer_quotes(feed['description'])
                    if 'message' in feed:
                        message = remplacer_quotes(feed['message'])
                    if 'story' in feed:
                        story = remplacer_quotes(feed['story'])

                    ajout_feed(idFeed1[1], name, created_time, updated_time, story, message, from_id, shares, id_group,
                               cursor, connection, count, nom)

            feeds = ''
            if next_feeds <> '':
                print "Next a rechehrcher :",next_feeds
                json_data = url_Suivant(next_feeds, usr, pwd, f,  lock)
                next_feeds = ''

                if json_data == 100:
                    print json_data, " Au suivant "
                else:
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = remplacer_limit(json_data['paging']['next'])
                        update_next(id_group, 'next_feeds', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']

        if json_data <> 100:

            sql = " update nexts set next_feeds = '1'   where idNexts=" + str(id_group)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin
            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break

    connection.close()

"""
def lancer_publication():

    nbr = 0

    usr_p = [usr_pwd[2],usr_pwd[1]]

    sql2 = " select id_sender , threat_feeds from hashtag_video group by id_sender having threat_feeds = 0 "
    print " Sql 2  = ",sql2

    connection = connect_db()
    cursor = connection.cursor()
    print " sql : ", sql2, "  => ", heure_actuelle()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    print " sql : ", sql2, "  => ", heure_actuelle()
    connection.close()

    lock = threading.Lock()


    print " Nombre nbr groups : ", len(nbr_groups)


    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=publication,
                                        args=(
                                            sql2, usr_c[2], usr_c[0], usr_c[1],  "thread0 " + str(nbr),
                                            lock))
            thread_1.start()
            nbr += 1
            thread_1.join()
            os._exit(0)
        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            nbr = 0
            exit()

    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   Nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   "
            thread_1 = threading.Thread(target=publication,
                                        args=(
                                            sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1],  "thread1 " + str(nbr),
                                            lock))
            thread_1.start()
            nbr += 1

            usr_c = usr_p[1]
            print "   Nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   "
            thread_2 = threading.Thread(target=publication,
                                        args=(
                                            sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1],  "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            nbr = 0
    else:
        exit()
"""

if __name__ == "__main__":

    #lancer_publication()

    #while SystemExit:
        #lancer_publication()

    sql2 = " select id_sender , threat_feeds from hashtag_video group by id_sender having threat_feeds = 0 "

    usr_pwd.sort(reverse=False)
    lock = threading.Lock()
    nbr = 0
    while (True):

        usr_c = usr_pwd[nbr]
        thread_1 = threading.Thread(target=publication,
                                    args=(sql2 + " order by id_sender ASC", usr_c[2], usr_c[0], usr_c[1],
                                          "thread1 " + str(nbr), lock))

        nbr += 1
        if nbr == len(usr_pwd):
            nbr = 0
        usr_c = usr_pwd[nbr]
        thread_2 = threading.Thread(target=publication,
                                    args=(sql2 + " order by id_sender DESC", usr_c[2], usr_c[0], usr_c[1],
                                          "thread2 " + str(nbr), lock))

        thread_1.start()
        thread_2.start()

        thread_1.join()
        thread_2.join()
        time.sleep(25)

        nbr += 1
        if nbr == len(usr_pwd):
            nbr = 0
