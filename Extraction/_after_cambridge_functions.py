# -*- coding: utf-8 -*-
import mysql.connector
import pickle
import requests
import threading
import datetime as dt
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import time
import sys
import calendar
reload(sys)

sys.setdefaultencoding('utf-8')

chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
mot_cle = ''
parametre_group = ''

#mot_cle = 'and hs.hashtag_name like "%سنطرال%" or hs.hashtag_name like "%خليه%" or hs.hashtag_name like "%افريقيا%" or hs.hashtag_name like "%مازوط%") '
#mot_cle = 'and hs.hashtag_name like "%النكبة%" or hs.hashtag_name like "%مليونية_العودة‬%" or hs.hashtag_name like "%يوم_العبور%" or hs.hashtag_name like "%جمعة_النذير%") '

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"


fp = open("E:\Token_Facebook\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 3:
        is_paramtrable = 1
        parametre_group = str(line).replace("\n", '')
        #print " parametre ",parametre_group
        if not parametre_group.__contains__('#'):
            fp_p = open("D:\Token_Facebook\mot_cles.txt")
            linee = fp_p.readline()
            #print " linee ", linee
            mot_cles = str(linee).split("#")
            mot_cle = mot_cles[0]
            #mot_cles = str(mot_cles).split("||")

            #mot_cherche = mot_cles[0]
            #mot_cles = str(mot_cles[1]).split(",")
            #print "  mot_cles  ", mot_cles
            #for mot in mot_cles:
                #mot_cle = mot_cle + ' and '+ mot_cherche +' like "%' + str(mot) + '%"'
            fp_p.close()
        parametre_group = parametre_group +  str(mot_cle)
    elif i == 4 :
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        #print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3])
        usr_pwd.append(usr_to_add)
fp.close()



#print parametre_group, "|  :  ", database_name, "  :  ", database_password,"  :   ", usr_pwd

nombre_arret = 100000000000000000450
Date_D_arret = 1483228800  # 01/01/2017


def connect_db_db(db):
    print "  Connect to Mysql db  =>  ", db
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=db,
                                   charset='utf8mb4', use_unicode=True)


def connect_db():
    print "  Connect to Mysql db  =>  ", database_name
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=database_name,
                                   charset='utf8mb4', use_unicode=True)


def scroll_down(driver):
    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")
    fin_resultat = driver.find_elements_by_css_selector("div[class='phm _64f']")
    print " Fin Des Resultats ", len(fin_resultat)

    while len(fin_resultat) == 0:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

        fin_resultat = driver.find_elements_by_css_selector("div[class='phm _64f']")
        print " Fin Des Resultats ", len(fin_resultat)


def Afficher_la_suite(driver):
    there_is_more = 1
    while there_is_more == 1:
        more_comments = driver.find_elements_by_css_selector("a[class='see_more_link']")
        there_is_more = 0
        for elem in more_comments:
            print len(more_comments), " - CLick on : ", elem
            driver.execute_script("arguments[0].click();", elem)
            # time.sleep(2)

def ecriture_fichier2(nom_fic, val):
    # print " fichier ",nom_fic
    with open(nom_fic, 'wb') as f:
        pickle.dump(val, f)
        f.close()


def Acces_Webdriver():
    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    print chromedriver.__len__() , "   ", chromedriver
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver

def generation_token(path_file, driver):
    token = ''
    try:
        url = 'https://developers.facebook.com/tools/explorer/?method=GET&path=me&version=v2.7#'
        driver.get(url)
        time.sleep(7)
        elem_id_user_2 = driver.find_element_by_css_selector("input[class='_58al']")
        token = elem_id_user_2.get_attribute("value")
        print " Token Genere est : ", token
        ecriture_fichier2(path_file, token)

    except Exception as e:
        print " ID introuvable :  ", e


def remove_sessions(driver):

    url = 'https://www.facebook.com/settings?tab=security'
    driver.get(url)
    time.sleep(5)
    now = dt.datetime.utcnow()
    time_to_sleep = 3601 - now.minute * 60 - now.second
    print ' date execution : ', now.day, "-", now.month, "-", now.year, "__", now.hour, ":", now.minute, ":", now.second, " time_to_sleep ", time_to_sleep
    try:

        more_comments = driver.find_elements_by_css_selector("span[class='_2gee']")
        if len(more_comments) > 0:
            more_comments = more_comments[0]
            print " - CLick on : ", more_comments
            driver.execute_script("arguments[0].click();", more_comments)
            time.sleep(5)

            more_comments = driver.find_element_by_css_selector("span[class='_4h8f']")
            print " - CLick on : ", more_comments
            driver.execute_script("arguments[0].click();", more_comments)
            time.sleep(3)

            more_comments = driver.find_element_by_css_selector(
                "a[class=' layerCancel _4jy0 _4jy3 _4jy1 _51sy selected _42ft']")
            print " - CLick on : ", more_comments
            driver.execute_script("arguments[0].click();", more_comments)
            time.sleep(3)

            # generation_token(chemin, driver)
            # time.sleep(3)
            driver.get(url)
            time.sleep(3)

        deconnexion_user_without_driver(driver)

    except Exception as e:
        print " ID introuvable :  ", e
    return 1


def deconnexion_user_without_driver(driver):
    try:
        more_comments = driver.find_element_by_css_selector("a[class='_5lxs _3qct _p']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(10)

        more_comments = driver.find_element_by_css_selector("form[class='_w0d _w0d']")
        print " - CLick on : ", more_comments
        more_comments.submit()
        # driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

    except Exception as e:
        print " ID introuvable :  ", e


def deconnexion_user(usr, pwd):
    driver = Acces_Webdriver()

    Authentification(driver, usr, pwd)

    try:

        more_comments = driver.find_element_by_css_selector("a[class='_5lxs _3qct _p']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(5)

        more_comments = driver.find_element_by_css_selector("form[class='_w0d _w0d']")
        print " - CLick on : ", more_comments
        more_comments.submit()
        # driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

    except Exception as e:
        print " ID introuvable :  ", e
        token = 100
    driver.close()
    driver.quit()


def More_element(share, driver):
    nbr_click = 1
    there_is_more = 1
    while there_is_more == 1:
        more_comments = share.find_elements_by_css_selector("a[class='UFIPagerLink']")
        if len(more_comments) == 0:
            there_is_more = 0
        else:
            for elem in more_comments:
                print " - CLick on : ", elem
                driver.execute_script("arguments[0].click();", elem)
                time.sleep(5)

    there_is_more = 1
    while there_is_more == 1:
        more_comments = share.find_elements_by_css_selector("a[class='pam uiBoxLightblue uiMorePagerPrimary']")
        if len(more_comments) == 0:
            there_is_more = 0
        else:
            for elem in more_comments:
                print nbr_click," - CLick on : ", elem
                driver.execute_script("arguments[0].click();", elem)
                time.sleep(5)
                nbr_click += 1


def Authentification(driver, usr, pwd):
    driver.get("http://www.facebook.com")
    elem = driver.find_element_by_id("email")
    elem.clear()
    elem.send_keys(usr)
    time.sleep(1.5)

    elem = driver.find_element_by_id("pass")
    elem.clear()
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(5)


def ajout_hashtag(hash_name, hash_url, con, cur, count, nom):
    sql = " SELECT * FROM `hashtags` WHERE hashtag_url = '" + hash_url + "'"
    cur.execute(sql)
    hashtag = cur.fetchone()
    id_inserted = -1
    print cur.rowcount, "  :  ", hash_name, "  :  ", sql
    if cur.rowcount > 0:
        id_inserted = hashtag[0]
    else:
        try:
            sql = " INSERT INTO `" + database_name + "`.`hashtags` (`hashtag_name`,`hashtag_url`) VALUES " \
                                                     "('" + str(hash_name) + "','" + hash_url + "');"
            print "(", count, "_", nom, ") Sql Hashtag : ", sql
            cur.execute(sql)
            con.commit()
            id_inserted = cur.lastrowid
        except Exception as e:
            print "(", count, "_", nom, ") Insert Hashtag Exception  :  ", e
    return id_inserted

def detail_reactions(reactions,nbr_likes,c,type_r, comment_id, pub_id, con, cur, count,nom,type_com_pub):
    # POUR CHAQUE REACTION
    for reaction in reactions:
        nbr_likes += 1
        # my_reaction = reaction.find_element_by_css_selector("div[class='_5j0e fsl fwb fcb']")
        my_reaction_item = reaction.get_attribute('outerHTML')
        print " 1 reaaaction ", my_reaction_item
        if '<div class="_5j0e fsl fwb fcb _5wj-"' in str(my_reaction_item):
            my_reaction_item = str(my_reaction_item).split('<div class="_5j0e fsl fwb fcb _5wj-"')
        else:
            my_reaction_item = str(my_reaction_item).split('<div class="_5j0e fsl fwb fcb"')

        my_reaction_item = my_reaction_item[1]
        #print " 2 reaaaction ", my_reaction_item
        # EXTRACTION DU NOM DE LA PERSONNE  AYANT REAGI
        nom_reacter = str(my_reaction_item).split('<a')
        nom_reacter = str(nom_reacter[1]).split('</a>')
        #print " reaaaction ", nom_reacter
        nom_reacter = str(nom_reacter[0]).split('">')
        #print " reaaaction ", nom_reacter
        nom_reacter = nom_reacter[1]

        # EXTRACTION DE L'ID DE LA PERSONNE  AYANT REAGI
        #print " user reaaaction ", my_reaction_item
        id_reacter = str(my_reaction_item).split('.php?id=')
        id_reacter = str(id_reacter[1]).split('&')
        id_reacter = id_reacter[0]

        print " id reacter :  ", id_reacter, " nom  :  ", nom_reacter
        ajout_sharing_reaction(id_reacter, nom_reacter, type_com_pub, type_r, comment_id, pub_id, con, cur, count,nom)
        c += 1
    return nbr_likes,c

def ajout_reaction(type_r , react , comment_id , pub_id , con ,cur, nbr_likes,type_com_pub  ):
    count = 1
    # EXTRACTION DE TOUTES LES REACTIONS
    reactions1 = react.find_elements_by_css_selector("div[class='_5j0e fsl fwb fcb']")
    print " Nombre de ",type_r,"  francais : ", len(reactions1)
    if len(reactions1) > 0 :
        nbr_likes,count = detail_reactions(reactions1, nbr_likes, count,type_r, comment_id, pub_id, con, cur, count," Reactions ",type_com_pub)

    reactions2 = react.find_elements_by_css_selector("div[class='_5j0e fsl fwb fcb _5wj-']")
    print " Nombre de ", type_r, "  arabe : ", len(reactions2)
    if len(reactions2) > 0:
        nbr_likes, count = detail_reactions(reactions2, nbr_likes, count,type_r, comment_id, pub_id, con, cur, count," Reactions ",type_com_pub)

    print " Final Nombre de ", type_r, "  : ", len(reactions1)+len(reactions2)

    return nbr_likes

def ajout_hashtag_video(id_hashtag, pub_id, pub_sender_id, con, cur, nom):
    date_lancement = int(time.time())
    try:
        sql = "INSERT INTO `" + database_name + "`.`hashtag_video` (`id_hashtag`,`id_pub`,`id_sender`) VALUES (" + \
              str(id_hashtag) + "," + str(pub_id) + "," + str(pub_sender_id) + ");"

        print "(", nom, ") Sql video : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", nom, ") Insert vidoe Exception  :  ", e


def ajout_sharing_pub(share_pub_new_id, share_time_text, nombre_reaction_classe, nombre_partage_classe,
                      share_sender_id, id_pub, con, cur, count, nom):
    date_lancement = int(time.time())
    try:
        sql = "INSERT INTO `" + database_name + "`.`shared_pub` (`id_pub`,`nombre_reaction`," \
                "`nombre_partage`,`created_time`,`pub_sender_id`,`date_script`,`id_parent_pub`) VALUES (" + \
              str(share_pub_new_id) + ",'" + str(nombre_reaction_classe) + "','" + str(nombre_partage_classe)\
              + "','" + str(share_time_text) + "'," + str(share_sender_id) + ",'" + str(date_lancement) + "'," + \
              str(id_pub) + ");"

        print "(", count, "_", nom, ") Sql Publication : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Publication Exception  :  ", e


def ajout_hashtag_sharing_pub(share_pub_new_id, share_time_text, nombre_reaction_classe, nombre_partage_classe,
                              share_sender_id, id_pub, con, cur, count, nom):
    date_lancement = int(time.time())
    try:
        sql = "INSERT INTO `" + database_name + "`.`shared_hashtaged_pub` (`idshared_pub`,`nombre_reaction`," \
                                                "`nombre_partage`,`created_time`,`pub_sender_id`,`date_script`,`id_parent_pub`) VALUES (" + \
              str(share_pub_new_id) + ",'" + str(nombre_reaction_classe) + "','" + str(nombre_partage_classe) + "','" \
              + str(share_time_text) + "'," + str(share_sender_id) + ",'" + str(date_lancement) + "'," + str(
            id_pub) + ");"

        print "(", count, "_", nom, ") Sql hashtag Publication : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert hashtag Publication Exception  :  ", e


def ajout_hashtag_pub(pub_id, nombre_comment_classe, nombre_reaction_classe, nombre_partage_classe,
                      nombre_vue_classe, pub_sender_id, con, cur, count, nom):
    date_lancement = int(time.time())
    try:
        sql = "INSERT INTO `" + database_name + "`.`hashtag_pub` (`id_pub`,`nombre_comment`,`nombre_reaction`," \
                                                "`nombre_partage`,`nombre_vue`,`pub_sender_id`,`date_script`) VALUES (" + str(
            pub_id) + ",'" \
              + str(nombre_comment_classe) + "','" + str(nombre_reaction_classe) + "','" + str(
            nombre_partage_classe) + "','" \
              + str(nombre_vue_classe) + "'," + str(pub_sender_id) + ",'" + str(date_lancement) + "');"

        print "(", count, "_", nom, ") Sql Publication : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Publication Exception  :  ", e


def ajout_uknown_user(pub_sender_name, con, cur, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`acteur` (`name`) VALUES " \
                                                "('" + remplacer_quotes(str(pub_sender_name)) + "');"
        print "(", nom, ") Sql Uknown User : ", sql
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print "(", nom, ") Insert Unkown User Exception  :  ", e


def ajout_sharing_user(pub_sender_id, pub_sender_name, con, cur, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`acteur` (`idActeurFace`,`name`) VALUES " \
                                                "(" + str(pub_sender_id) + ",'" + remplacer_quotes(
            str(pub_sender_name)) + "');"
        print "(", count, "_", nom, ") Sql User : ", sql
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print "(", count, "_", nom, ") Insert User Exception  :  ", e


def ajout_sharing_reaction(id_reacter, nom_reacter, type_com_pub, type_r, pub_id, sender_id, con, cur, count, nom):
    ajout_sharing_user(id_reacter, nom_reacter, con, cur, count, nom)
    date_lancement = int(time.time())

    try:
        sql = "INSERT INTO `" + database_name + "`.`likes_sharing` (`idActeur`,`idLiked`,`idSender`,`type_com_pub`," \
                                                "`type_like`,`date_script`) VALUES (" + str(id_reacter) + "," + str(
            pub_id) + "," + str(sender_id) + \
              ",'" + str(type_com_pub) + "','" + str(type_r) + "','" + str(date_lancement) + "');"
        print "(", count, "_", nom, ") Sql Reaction : ", sql
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print "(", count, "_", nom, ") Insert Reaction Exception  :  ", e


def update_disapear_time(table, field_update, critere, con, cur, count, nom):
    try:
        sql = " update "+str(table)+" set "+str(field_update)+" where " + str(critere)
        print "(", count, "_", nom, ") Sql update "+str(table)+"  : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update "+str(table)+"  Exception  :  ", e

def update_acteur_comment(acteur_id, acteur_name, id_comment, id_pub, con, cur, count, nom):
    ajout_sharing_user(acteur_id, acteur_name, con, cur, count, nom)
    try:
        sql = " update comments set idActeur = " + str(acteur_id) + " where idComments=" + \
              str(id_comment) + " and idfeed=" + str(id_pub)
        print "(", count, "_", nom, ") Sql update Comment : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update Comment Exception  :  ", e


def update_hashtag(hashtag_searched_id, hashtag_url, explore, con, cur, nom):
    try:
        sql = " update hashtags set hashtag_hall_url = '" + str(hashtag_url) + "' , explore = " + \
              str(explore) + "  where idhashtag=" + str(hashtag_searched_id)
        print "(", nom, ")  Sql update Hashtag : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", nom, ") Update Hashtag Exception  :  ", e


def update_threat(table, threated, nbr_threat, identifiant_table, identifiant_val, con, cur, nom):
    try:
        if nbr_threat == 0:
            nbr_threat = -1

        sql = " update " + str(table) + " set " + str(threated) + " =  " + str(nbr_threat) + \
              " where " + str(identifiant_table) + "=" + str(identifiant_val)
        print "(", nom, ")  Sql update Threat : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", nom, ") Update Threat Exception  :  ", e


def update_hashtag_video(hashtag_searched_id, explore, con, cur, nom):
    try:
        sql = " update hashtags set  explore_video = " + \
              str(explore) + "  where idhashtag=" + str(hashtag_searched_id)
        print "(", nom, ")  Sql update Hashtag video : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", nom, ") Update Hashtag video Exception  :  ", e


def ajout_hashtag_pubs(id_hashtag, pub_id, con, cur, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`hashtag_pub_etrangere` (`idHashtag`,`id_pub`) VALUES " \
                                                "(" + str(id_hashtag) + "," + str(pub_id) + ");"
        print "(", count, "_", nom, ") Sql hashtag etrangere : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert hashtag etrangere Exception  :  ", e

def ajout_replying(id_com1, id_com2,idfeed,idgroup, con, cur, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`comments_replying` (`idcoments1`,`idcoments2`,`idfeed`,`idgroup`) VALUES " \
                                                "(" + str(id_com1) + "," + str(id_com2) + "," + str(idfeed) + "," + str(idgroup) + ");"
        print "(", count, "_", nom, ") Sql comments_replying : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert comments_replying Exception  :  ", e


def remplacer_quotes(chaine):
    chaine = chaine.replace("'", "")
    chaine = chaine.replace('"', '')
    return chaine


def heure_actuelle():
    now = dt.datetime.utcnow()
    print ' Date recherche : ', now.day, "-", now.month, "-", now.year, "__", now.hour, ":", now.minute, ":", now.second
