from _0_Functions import *
import _0_Groups


def partage_hauteur(sql, file, usr, pwd,  nom, lock):
    connection = connect_db()
    cursor = connection.cursor()
    graph = ''
    print sql
    cursor.execute(sql)
    groups = cursor.fetchall()
    print " count ", len(groups)
    for group in groups:
        count = 0
        id_group1 = group[0]
        id_group1 = id_group1.split('_', 1)
        id_group = id_group1[0]
        id_feed_or = id_group1[1]

        print id_group
        next_feeds = ''
        feeds = ''
        story_tags = ''
        story_tags_next = ''
        cle = "sharedposts"

        if True:

            post_id = str(group[0]) + '?fields=sharedposts{id,parent_id,created_time,name,from,story},created_time'
            print "poooooost id ", post_id

            try:
                graph = retourner_graph2(post_id, lock)
                post = graph.get_object(id=post_id)
                if cle in post and 'data' in post[cle]:
                    feeds = post[cle]['data']
                if cle in post and 'paging' in post[cle] and 'next' in post[cle]['paging']:
                    next_feeds = post[cle]['paging']['next']

            except Exception as e:
                dt = datetime.datetime.now()
                disap_time = calendar.timegm(dt.utctimetuple())
                sql = "update groups set Disapear_time='" + str(disap_time) + "' where idGroup=" + str(id_group)
                print sql
                # cursor.execute(sql)
                print " ID introuvable :  ", e

        while feeds <> '':
            print " feeds", feeds
            id_group_original = id_group
            idfeed_original = id_feed_or
            created_time_original = uniix_time(post['created_time'])
            for feed in feeds:
                created_time_destination = uniix_time(feed['created_time'])
                disapear_time = ''
                dest_id = feed['id']
                dest_id = dest_id.split('_', 1)
                id_group_destination = dest_id[0]
                idfeed_destination = dest_id[1]
                parent_id = ''
                story = ''
                if 'parent_id' in feed:
                    parent_id = feed['parent_id']
                if 'story' in feed:
                    story = remplacer_quotes(feed['story'])
                count += 1
                ajout_share(id_group_original, idfeed_original, created_time_original,
                            created_time_destination, disapear_time, id_group_destination,
                            idfeed_destination, parent_id, story, cursor, connection, count, group[1])
                connection.commit()
                # ajout_next(id_group, 'with_tags_next', with_tags_next, cursor)
            feeds = ''
            if next_feeds <> '':
                json_data = url_Suivant2(next_feeds)
                next_feeds = ''
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
                    # print next_feeds
                    # update_next(id_group, 'story_tags_next_feeds', next_feeds, cursor, connection,count)
                if 'data' in json_data:
                    feeds = json_data['data']
                    # print feeds
        # sql = " update shares set traite = 1   where id_group_destination=" + str(id_group_destination) +\
        #      " and idfeed_destination=" + str(idfeed_destination)
        # print " update sql " , sql
        # cursor.execute(sql)
        # connection.commit()
        sql = " update shares set traite = 2   where id_group_original=" + str(id_group) + \
              " and id_etude_share=" + str(id_feed_or)
        print " update sql ", sql
        cursor.execute(sql)
        connection.commit()
    connection.close()


def lancer_partage_hauteur():
    nbr = 0

    usr_p = [("inouflmsti@gmail.com", "@msti@2017", "inouflmsti"),
             ("crawling.user2@gmail.com", "123456789@1", "crawling.user2")]

    sql1 = " select parent_id,id_etude_share from shares where parent_id <> '' and traite <> '2' order by parent_id DESC "

    sql2 = " select parent_id,id_etude_share from shares where parent_id <> '' and traite <> '2' order by parent_id ASC "

    lock = threading.Lock()
    try:
        usr_c = usr_p[0]

        print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
        thread_1 = threading.Thread(target=partage_hauteur,
                                    args=(sql2, usr_c[2], usr_c[0], usr_c[1],  "thread1 " + str(nbr), lock))
        thread_1.start()
        nbr += 1
        usr_c = usr_p[1]
        print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
        thread_2 = threading.Thread(target=partage_hauteur,
                                    args=(sql1, usr_c[2], usr_c[0], usr_c[1],  "thread2 " + str(nbr), lock))
        thread_2.start()

        thread_1.join()
        thread_2.join()

    except SystemExit:
        print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
        # time.sleep(10)
        nbr = 0


if __name__ == "__main__":
    lancer_partage_hauteur()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_partage_hauteur()
