use crawling_2018 ;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: crawling_2018
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acteur`
--

DROP TABLE IF EXISTS `acteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acteur` (
  `iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idActeurFace` bigint(255) NOT NULL,
  `name` varchar(66) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'user',
  `disapear_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_amitie` int(11) DEFAULT '1',
  `next_friends` longtext COLLATE utf8mb4_unicode_ci,
  `error` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`idActeurFace`),
  KEY `key_iterator1` (`iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=92063839 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cata`
--

DROP TABLE IF EXISTS `cata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cata` (
  `id_pub` bigint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catb`
--

DROP TABLE IF EXISTS `catb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catb` (
  `id_pub` bigint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `com_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idComments` bigint(255) NOT NULL,
  `message` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `created_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `like_count` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idfeed` bigint(255) NOT NULL,
  `idActeur` bigint(255) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idComments`),
  KEY `idFeed_idx` (`idfeed`),
  KEY `idActeur_idx` (`idActeur`),
  KEY `iterator_index3` (`com_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=21867306 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `comments_not_group`
--

DROP TABLE IF EXISTS `comments_not_group`;
/*!50001 DROP VIEW IF EXISTS `comments_not_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `comments_not_group` AS SELECT 
 1 AS `idcomments`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `comments_sharing`
--

DROP TABLE IF EXISTS `comments_sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_sharing` (
  `com_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idComments` bigint(255) NOT NULL,
  `message` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `created_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `like_count` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idfeed` bigint(255) NOT NULL,
  `idActeur` bigint(255) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idComments`),
  KEY `idFeed_idx` (`idfeed`),
  KEY `idActeur_idx` (`idActeur`),
  KEY `iterator_index3` (`com_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=21867306 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed`
--

DROP TABLE IF EXISTS `feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed` (
  `feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idFeed` bigint(255) NOT NULL,
  `name` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdActeurs` bigint(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `feed_share` int(11) DEFAULT '0',
  PRIMARY KEY (`idFeed`),
  KEY `IdActeurs_idx` (`IdActeurs`),
  KEY `idGroup_idx` (`idGroup`),
  KEY `iterator_index2` (`feed_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=12828192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_event`
--

DROP TABLE IF EXISTS `feed_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_event` (
  `feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idFeed` bigint(255) NOT NULL,
  `name` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdActeurs` bigint(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `feed_share` int(11) DEFAULT '0',
  PRIMARY KEY (`idFeed`),
  KEY `IdActeurs_idx` (`IdActeurs`),
  KEY `idGroup_idx` (`idGroup`),
  KEY `iterator_index2` (`feed_iterator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_link`
--

DROP TABLE IF EXISTS `feed_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_link` (
  `feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idFeed` bigint(255) NOT NULL,
  `name` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdActeurs` bigint(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `feed_share` int(11) DEFAULT '0',
  PRIMARY KEY (`idFeed`),
  KEY `IdActeurs_idx` (`IdActeurs`),
  KEY `idGroup_idx` (`idGroup`),
  KEY `iterator_index2` (`feed_iterator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `feed_not_group`
--

DROP TABLE IF EXISTS `feed_not_group`;
/*!50001 DROP VIEW IF EXISTS `feed_not_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `feed_not_group` AS SELECT 
 1 AS `idfeed`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `feed_note`
--

DROP TABLE IF EXISTS `feed_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_note` (
  `feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idFeed` bigint(255) NOT NULL,
  `name` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdActeurs` bigint(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `feed_share` int(11) DEFAULT '0',
  PRIMARY KEY (`idFeed`),
  KEY `IdActeurs_idx` (`IdActeurs`),
  KEY `idGroup_idx` (`idGroup`),
  KEY `iterator_index2` (`feed_iterator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_videos`
--

DROP TABLE IF EXISTS `feed_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_videos` (
  `feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `idFeed` bigint(255) NOT NULL,
  `name` tinytext CHARACTER SET utf8mb4 NOT NULL,
  `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `IdActeurs` bigint(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `feed_share` int(11) DEFAULT '0',
  PRIMARY KEY (`idFeed`),
  KEY `IdActeurs_idx` (`IdActeurs`),
  KEY `idGroup_idx` (`idGroup`),
  KEY `iterator_index2` (`feed_iterator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `feeds_share`
--

DROP TABLE IF EXISTS `feeds_share`;
/*!50001 DROP VIEW IF EXISTS `feeds_share`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `feeds_share` AS SELECT 
 1 AS `feed_iterator`,
 1 AS `idFeed`,
 1 AS `name`,
 1 AS `createdtime`,
 1 AS `updatedtime`,
 1 AS `Disapear_Time`,
 1 AS `message`,
 1 AS `story`,
 1 AS `shares`,
 1 AS `IdActeurs`,
 1 AS `idGroup`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `idfriends1` bigint(255) NOT NULL,
  `idfriends2` bigint(255) NOT NULL,
  PRIMARY KEY (`idfriends1`,`idfriends2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `idGroup` bigint(255) NOT NULL,
  `group_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT ' ',
  `type` varchar(45) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT ' ',
  `email` varchar(45) DEFAULT ' ',
  `updated_time` varchar(255) DEFAULT '0',
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `adherents` varchar(255) DEFAULT '0',
  `owner_name` varchar(45) DEFAULT NULL,
  `owner_id` bigint(255) DEFAULT NULL,
  `hashtag` int(255) DEFAULT '0',
  `hashtag_concerne` varchar(45) DEFAULT ' ',
  PRIMARY KEY (`idGroup`),
  KEY `iterator_index5` (`group_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=1828810620698126 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `groups_share`
--

DROP TABLE IF EXISTS `groups_share`;
/*!50001 DROP VIEW IF EXISTS `groups_share`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `groups_share` AS SELECT 
 1 AS `idGroup`,
 1 AS `group_iterator`,
 1 AS `name`,
 1 AS `type`,
 1 AS `privacy`,
 1 AS `email`,
 1 AS `updated_time`,
 1 AS `Disapear_Time`,
 1 AS `adherents`,
 1 AS `owner_name`,
 1 AS `owner_id`,
 1 AS `hashtag`,
 1 AS `hashtag_concerne`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `hashtag_pub`
--

DROP TABLE IF EXISTS `hashtag_pub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtag_pub` (
  `id_pub` bigint(255) NOT NULL,
  `nombre_comment` varchar(45) DEFAULT '0',
  `nombre_partage` varchar(45) DEFAULT '0',
  `nombre_reaction` varchar(45) DEFAULT '0',
  `nombre_vue` varchar(45) DEFAULT '0',
  `pub_sender_id` bigint(255) NOT NULL,
  `date_script` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pub`,`date_script`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hashtag_pub_etrangere`
--

DROP TABLE IF EXISTS `hashtag_pub_etrangere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtag_pub_etrangere` (
  `idHashtag`   bigint(255) NOT NULL,
  `id_pub` bigint(255) NOT NULL,
  PRIMARY KEY (`idHashtag`,`id_pub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `hashtag_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtag_video` (
  `id_Hashtag`   bigint(255) NOT NULL,
  `id_pub` bigint(255) NOT NULL,
  `id_sender` bigint(255) NOT NULL,
  PRIMARY KEY (`id_Hashtag`,`id_pub`,`id_sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `idHashtag`   bigint(255) NOT NULL AUTO_INCREMENT,
  `hashtag_name` varchar(255) DEFAULT '',
  `hashtag_url` varchar(455) NOT NULL,
  `hashtag_hall_url` varchar(655) DEFAULT '',
  `explore` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`idHashtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `idActeur` bigint(255) NOT NULL,
  `idLiked` bigint(255) NOT NULL,
  `type` varchar(25) NOT NULL,
  `type_like` varchar(45) NOT NULL,
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`idActeur`,`idLiked`,`type`,`type_like`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `likes_coments_not_group`
--

DROP TABLE IF EXISTS `likes_coments_not_group`;
/*!50001 DROP VIEW IF EXISTS `likes_coments_not_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `likes_coments_not_group` AS SELECT 
 1 AS `idliked`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `likes_feed_not_group`
--

DROP TABLE IF EXISTS `likes_feed_not_group`;
/*!50001 DROP VIEW IF EXISTS `likes_feed_not_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `likes_feed_not_group` AS SELECT 
 1 AS `idliked`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `likes_sharing`
--

DROP TABLE IF EXISTS `likes_sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes_sharing` (
  `idActeur` bigint(255) NOT NULL,
  `idLiked` bigint(255) NOT NULL,
  `type` varchar(25) NOT NULL,
  `type_like` varchar(45) NOT NULL,
  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`idActeur`,`idLiked`,`type`,`type_like`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `idGroup` bigint(255) NOT NULL,
  `idActeurs` bigint(255) NOT NULL,
  `administrateur` varchar(45) DEFAULT 'false',
  `disapear_time` varchar(45) DEFAULT '0',
  PRIMARY KEY (`idGroup`,`idActeurs`),
  KEY `idActeurs_idx` (`idActeurs`),
  KEY `idActeurs_idx1` (`idActeurs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nexts`
--

DROP TABLE IF EXISTS `nexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nexts` (
  `idNexts` bigint(255) NOT NULL,
  `feed_member_next` longtext,
  `feed_comments_next` longtext,
  `next_feeds` longtext,
  `feed_likes_next` longtext,
  `with_tags_next_feeds` longtext,
  `message_tags_next_feeds` longtext,
  `feed_reactions_next` longtext,
  `comments_tags_next_feeds` longtext,
  `story_tags_next_feeds` longtext,
  `error` longtext,
  `feed_likes_next1` longtext,
  `feed_likes_next2` longtext,
  `feed_comments_next1` longtext,
  `feed_comments_next2` longtext,
  `feed_reactions_next1` longtext,
  PRIMARY KEY (`idNexts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `numbercomments4pub`
--

DROP TABLE IF EXISTS `numbercomments4pub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numbercomments4pub` (
  `ID_PUB` bigint(255) DEFAULT NULL,
  `Number_Comment` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `idposts` bigint(255) NOT NULL,
  `message` longtext,
  `story` longtext,
  `created_time` varchar(255) DEFAULT NULL,
  `update_time` varchar(255) DEFAULT NULL,
  `disapear_time` varchar(255) DEFAULT NULL,
  `shares` varchar(45) DEFAULT NULL,
  `idActeurFace` bigint(255) NOT NULL,
  PRIMARY KEY (`idposts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pub_comments`
--

DROP TABLE IF EXISTS `pub_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pub_comments` (
  `id_pub` bigint(255) DEFAULT NULL,
  `TimePub` bigint(255) DEFAULT NULL,
  `comments_time` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pubs_per_h_m`
--

DROP TABLE IF EXISTS `pubs_per_h_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pubs_per_h_m` (
  `id_pub` bigint(255) DEFAULT NULL,
  `houre` bigint(21) DEFAULT NULL,
  `minutes` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `redandants`
--

DROP TABLE IF EXISTS `redandants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redandants` (
  `nb` int(11) NOT NULL,
  `id` bigint(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shared_hashtaged_pub`
--

DROP TABLE IF EXISTS `shared_hashtaged_pub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_hashtaged_pub` (
  `idshared_pub` bigint(255) NOT NULL,
  `nombre_partage` varchar(45) DEFAULT '0',
  `nombre_reaction` varchar(45) DEFAULT '0',
  `created_time` varchar(45) DEFAULT '0',
  `pub_sender_id` bigint(255) NOT NULL,
  `date_script` varchar(45) NOT NULL,
  `id_parent_pub` bigint(255) NOT NULL,
  PRIMARY KEY (`idshared_pub`,`date_script`,`id_parent_pub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shared_pub`
--

DROP TABLE IF EXISTS `shared_pub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_pub` (
  `id_pub` bigint(255) NOT NULL,
  `nombre_comment` varchar(45) DEFAULT '0',
  `nombre_partage` varchar(45) DEFAULT '0',
  `nombre_reaction` varchar(45) DEFAULT '0',
  `created_time` varchar(45) DEFAULT '0',
  `pub_sender_id` bigint(255) NOT NULL,
  `date_script` varchar(45) NOT NULL,
  `id_parent_pub` bigint(255) NOT NULL,
  PRIMARY KEY (`id_pub`,`date_script`,`id_parent_pub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shares`
--

DROP TABLE IF EXISTS `shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shares` (
  `id_group_original` bigint(255) NOT NULL,
  `idfeed_original` bigint(255) NOT NULL,
  `created_time_original` varchar(45) DEFAULT NULL,
  `created_time_destination` varchar(45) DEFAULT NULL,
  `disapear_time` varchar(45) DEFAULT NULL,
  `id_group_destination` bigint(255) NOT NULL,
  `idfeed_destination` bigint(255) NOT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `story` longtext,
  `id_etude_share` bigint(255) DEFAULT NULL,
  `traite` int(11) DEFAULT '0',
  `share_count` bigint(255) DEFAULT '-1',
  PRIMARY KEY (`id_group_original`,`idfeed_original`,`id_group_destination`,`idfeed_destination`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `idActeurFace` bigint(255) NOT NULL,
  `idtag` bigint(255) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  `type_tag` varchar(45) DEFAULT NULL,
  `Disapear_Time` varchar(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `tag_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idActeurFace`,`idtag`,`idGroup`),
  KEY `iterator_index4` (`tag_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=1989523 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags_acteur`
--

DROP TABLE IF EXISTS `tags_acteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_acteur` (
  `idActeurFace` bigint(255) NOT NULL,
  `idtag` bigint(255) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  `type_tag` varchar(45) DEFAULT NULL,
  `Disapear_Time` varchar(255) NOT NULL,
  `idGroup` bigint(255) NOT NULL,
  `tag_iterator` bigint(255) NOT NULL AUTO_INCREMENT,
  `id_tag_where` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`idActeurFace`,`idtag`,`idGroup`),
  KEY `iterator_index4` (`tag_iterator`)
) ENGINE=InnoDB AUTO_INCREMENT=413502 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `comments_not_group`
--

/*!50001 DROP VIEW IF EXISTS `comments_not_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `comments_not_group` AS select `comments`.`idComments` AS `idcomments` from (`comments` join `feed_not_group`) where (`comments`.`idfeed` = `feed_not_group`.`idfeed`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `feed_not_group`
--

/*!50001 DROP VIEW IF EXISTS `feed_not_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `feed_not_group` AS select `feed`.`idFeed` AS `idfeed` from `feed` where (not(`feed`.`idGroup` in (select `groups`.`idGroup` from `groups`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `feeds_share`
--

/*!50001 DROP VIEW IF EXISTS `feeds_share`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `feeds_share` AS select `feed`.`feed_iterator` AS `feed_iterator`,`feed`.`idFeed` AS `idFeed`,`feed`.`name` AS `name`,`feed`.`createdtime` AS `createdtime`,`feed`.`updatedtime` AS `updatedtime`,`feed`.`Disapear_Time` AS `Disapear_Time`,`feed`.`message` AS `message`,`feed`.`story` AS `story`,`feed`.`shares` AS `shares`,`feed`.`IdActeurs` AS `IdActeurs`,`feed`.`idGroup` AS `idGroup` from (`groups` join `feed`) where ((`feed`.`idGroup` = `groups`.`idGroup`) and ((`groups`.`idGroup` = '377364058953191') or (`groups`.`idGroup` = '4910417718') or (`groups`.`idGroup` = '157713321077194') or (`groups`.`idGroup` = '1217400568290089') or (`groups`.`idGroup` = '413304748792047') or (`groups`.`idGroup` = '767319176720995'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `groups_share`
--

/*!50001 DROP VIEW IF EXISTS `groups_share`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `groups_share` AS select `groups`.`idGroup` AS `idGroup`,`groups`.`group_iterator` AS `group_iterator`,`groups`.`name` AS `name`,`groups`.`type` AS `type`,`groups`.`privacy` AS `privacy`,`groups`.`email` AS `email`,`groups`.`updated_time` AS `updated_time`,`groups`.`Disapear_Time` AS `Disapear_Time`,`groups`.`adherents` AS `adherents`,`groups`.`owner_name` AS `owner_name`,`groups`.`owner_id` AS `owner_id`,`groups`.`hashtag` AS `hashtag`,`groups`.`hashtag_concerne` AS `hashtag_concerne` from `groups` where ((`groups`.`idGroup` = '377364058953191') or (`groups`.`idGroup` = '4910417718') or (`groups`.`idGroup` = '157713321077194') or (`groups`.`idGroup` = '1217400568290089') or (`groups`.`idGroup` = '413304748792047') or (`groups`.`idGroup` = '767319176720995')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `likes_coments_not_group`
--

/*!50001 DROP VIEW IF EXISTS `likes_coments_not_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `likes_coments_not_group` AS select `likes`.`idLiked` AS `idliked` from (`likes` join `comments_not_group`) where (`likes`.`idLiked` = `comments_not_group`.`idcomments`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `likes_feed_not_group`
--

/*!50001 DROP VIEW IF EXISTS `likes_feed_not_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `likes_feed_not_group` AS select `likes`.`idLiked` AS `idliked` from (`likes` join `feed_not_group`) where (`likes`.`idLiked` = `feed_not_group`.`idfeed`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-12  0:48:14
