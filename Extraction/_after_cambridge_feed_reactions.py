# coding=utf-8
from _after_cambridge_functions import *

def parcours_reacts(usr, pwd):

    first_sql = 'select f.idfeed,f.idGroup from feed f , groups g where f.idGroup=g.idGroup and f.threat_reaction = 0 and f.Disapear_time = 1 '

    if is_paramtrable == 1 :
        first_sql = first_sql + parametre_group
    first_sql = first_sql + ' limit 500 '
    connection = connect_db()
    cursor = connection.cursor()
    print " sql : ", first_sql, "  => ", heure_actuelle()
    cursor.execute(first_sql)
    nbr_pubs = cursor.fetchall()
    print " sql : ", first_sql, "  => ", heure_actuelle()
    nombre_row = cursor.rowcount
    print " Nombrre row : ", nombre_row
    count = 0

    if nombre_row > 0 :
        try:
            driver = Acces_Webdriver()
            Authentification(driver, usr, pwd)

            try:
                for pub in nbr_pubs:

                    count += 1
                    is_connected = driver.find_elements_by_id("email")
                    if len(is_connected) > 0:
                        print " UTilisateur deconnecte par facebook "
                        nombre_row = -1
                        break

                    pub_id = int(pub[0])
                    sender_id = int(pub[1])

                    # ACCES DIRECT AUX REACTIONS DE LA PUBLICATION
                    url = 'https://www.facebook.com/ufi/reaction/profile/browser/?ft_ent_identifier=' + str(pub_id)
                    print "url is " , url
                    driver.get(url)
                    time.sleep(5)

                    is_there_element_in = driver.find_elements_by_css_selector('div[class="mtm fsm fwn fcg"]')
                    if len(is_there_element_in) > 0:
                        now = calendar.timegm(time.gmtime())
                        critere = " idfeed = " + str(pub_id)
                        updated = " Disapear_time=" + str(now) + " "
                        update_disapear_time("feed", updated, critere, connection, cursor, count, " feed_disapear ")

                    else:
                        # CLICK SUR AFFICHER PULISIEURS
                        More_element(driver, driver)

                        # EXTRACTION DES REACTIONS DE LA PUBLICATION
                        reacts_types = driver.find_elements_by_css_selector("div[class='_3p56']")
                        i = 1
                        nbr_feed_like = 0

                        # POUR CHAQUE TYPE DE REACTION : HAHA , SAD , ...
                        for elem in reacts_types:
                            # TYPE DE REACTION
                            type_reaction = elem.get_attribute('innerHTML')

                            j_aime = driver.find_element_by_xpath(
                                "/html/body/div[1]/div[3]/div[1]/div/div/div[1]/ul/li[" + str(i) + "]")
                            nbr_feed_like = ajout_reaction(type_reaction, j_aime, pub_id, sender_id, connection, cursor,
                                                           nbr_feed_like,"feed")
                            i += 1

                        update_threat("feed", "threat_reaction", nbr_feed_like, "idfeed", pub_id, connection, cursor,
                                      " Reaction ")
                        time.sleep(3)
                    nombre_row -= 1

                #cursor.execute(first_sql)
                #nbr_pubs = cursor.fetchall()
                #nombre_row = cursor.rowcount
            except Exception as e:
                print " ID introuvable :  ", e

            driver.close()
            driver.quit()

        except Exception as e:
            print " Error : ", e
            exit()


if __name__ == "__main__":
    usr_pwd.sort(reverse=True)
    while (True):
        for usr in usr_pwd:
            thread_1 = threading.Thread(target=parcours_reacts, args=(usr[0], usr[1]))
            thread_1.start()
            thread_1.join()
            time.sleep(25)
