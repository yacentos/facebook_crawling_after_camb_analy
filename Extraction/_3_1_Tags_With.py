from _0_Functions import *


def feedwith_tags(first_sql, file, usr, pwd,  nom, lock, fin=0):
    # my_token='EAACEdEose0cBAIX1VTX9FHhphq8KcrmhkCnxXEotV3sFSAILn9AlX8uZBEZBnZAJGwtj7XupyxOpF9xQOVl9T34uCufYEYDBBu4I71qhmg56dCeBP4Bt21TFQcJZBmf68F6DVJZA8JlKLV5BNZCCTzVD0jeZCaxnjnB0C6ZAeoezmwZDZD'
    graph = ''

    connection = connect_db()
    cursor = connection.cursor()

    next_feeds = ''
    feeds = ''
    with_tags = ''
    with_tags_next = ''
    fee_next = ''
    json_data = 0
    count = 0

    rupture = 0

    cursor.execute(first_sql)
    groups = cursor.fetchall()
    print first_sql, " leeeeeeeeeeeeeeeeeeen ", len(groups)

    for group in groups:
        id_group = group[0]
        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, file, lock)
            if json_data == 100:
                print json_data, "  au suivant "
            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
        else:
            post_id = str(id_group) + '?fields=feed{with_tags{id,name}}'
            graph = retourner_graph(file, usr, pwd, lock)
            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " post id error ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100
            if 'feed' in post and 'data' in post['feed']:
                    feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                    next_feeds = post['feed']['paging']['next']

                # print ( feeds, '           ', next_feeds )
        while feeds != '':
            for feed in feeds:
                if 'with_tags' in feed:

                    if 'data' in feed['with_tags']:
                        with_tags = feed['with_tags']['data']
                        print (" wiiiiith    ", with_tags)
                    if 'paging' in feed['with_tags'] and 'next' in feed['with_tags']['paging']:
                        with_tags_next = feed['with_tags']['paging']['next']
                    while with_tags != '':
                        for tag in with_tags:
                            fe_id = feed['id']
                            fe_id = fe_id.split('_', 1)
                            count += 1
                            ajout_tag('with', tag['id'], fe_id[1], 'f', cursor, tag['name'], 'need_search',
                                      fe_id[0], connection, count, id_group, nom)

                        with_tags = ''
                        if with_tags_next != '':
                            json_data = url_Suivant(with_tags_next, usr, pwd, file, lock)
                            with_tags_next = ''
                            if json_data == 100:
                                print json_data, "  au suivant "
                            else:
                                if 'data' in json_data:
                                    with_tags = json_data['data']
                                if 'paging' in json_data and 'next' in json_data['paging']:
                                    with_tags_next = json_data['paging']['next']
                                    #update_next(id_group, 'with_tags_next_feeds', with_tags_next, cursor, connection, count, nom)
            feeds = ''
            if next_feeds != '' and json_data <> 100 :
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if json_data == 100:
                    print json_data, "  au suivant "
                else:
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = json_data['paging']['next']
                        update_next(id_group, 'with_tags_next_feeds', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']
        if json_data <> 100:
            sql = " update nexts set with_tags_next_feeds = '1'   where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break
    connection.close()


def lancer_feedwith_tags():
    nbr = 0

    usr_p = [("inouflmsti@gmail.com", "987654321@cxwdsqeza", "../Token/inouflmsti"),
           ("crawling.user2@gmail.com", "123456789@1", "../Token/crawling.user2")]

    sql2 = " select idgroup,with_tags_next_feeds from groups , nexts where idNexts=idgroup  " \
           "and ( with_tags_next_feeds <> '1')  "

    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()
    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=feedwith_tags,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread0" + str(nbr), lock))
            thread_1.start()
            nbr += 1
            thread_1.join()
            os._exit(0)
        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            nbr = 0
    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=feedwith_tags,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=feedwith_tags,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    else:
        os._exit(0)


if __name__ == "__main__":
    lancer_feedwith_tags()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_feedwith_tags()
