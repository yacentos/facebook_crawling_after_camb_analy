# -*- coding: utf-8 -*-
import time
import urllib
import urllib2

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
chromedriver = "E:\chromedriver.exe"
import json
import requests



def Smart_extract_json(data, tweet_tweet_id_list, i):
    if ' smart new_latent_count' in data:
        print 'new_latent_count : ', data['new_latent_count']
    if 'items_html' in data:
        items_html = u''.join((data['items_html'])).encode('utf-8').strip()
        items_html = " ".join(items_html.split())
        items = items_html.split('<li class="js-stream-item stream-item stream-item "')

        tweet_tweet_id_list, i = gestion_elem(items, tweet_tweet_id_list, i)
    return tweet_tweet_id_list, i


def Smart_tweets():
    caps = DesiredCapabilities.CHROME
    caps['loggingPrefs'] = {'performance': 'ALL'}
    driver = webdriver.Chrome(chromedriver, desired_capabilities=caps)
    # direct link to user https://twitter.com/intent/user?user_id=34239289
    header = {
        "user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    # url = 'https://twitter.com/search?q=%D8%AC%D9%85%D8%A7%D9%84_%D8%A3%D8%AD%D9%85%D8%AF_%D8%AE%D8%A7%D8%B4%D9%82%D8%AC%D9%8A'
    # url = 'https://twitter.com/search?q=%D8%A7%D9%84%D8%B3%D8%A7%D8%B9%D8%A9_%D8%A7%D9%84%D9%85%D8%BA%D8%B1%D8%A8'
    urls = ['https://twitter.com/search?q=%D8%AE%D8%A7%D8%B4%D9%82%D8%AC%D9%8A',
            'https://twitter.com/search?q=PaulGAllen']
    tweet_tweet_id_list = []

    file_nbr = 0
    i = 0
    for url in urls:
        driver.get(url)
        time.sleep(2)
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        time.sleep(3)
        src = driver.page_source
        tweets_elem = src.split('<li class="js-stream-item')
        tweet_tweet_id_list, i = gestion_elem(tweets_elem, tweet_tweet_id_list, i)

        for entry in driver.get_log('performance'):
            timeline = str(entry)
            if 'timeline?vertical' in timeline:
                my_timelines = timeline.split('timeline?vertical')
                print  " leeeeeeeeeen ", len(my_timelines)
                # for t in timelines:
                # print " --- ", t
                # if len(timelines) == 3:
                # exit(0)
                if len(my_timelines) > 1:
                    print "  yyy  ", my_timelines[len(my_timelines) - 1]
                    timelines = my_timelines[len(my_timelines) - 1].split('"},"timestamp"')
                    timelines = timelines[0].split('&reset_error_state=false"')
                    url1 = timelines[0]

                    first_url = "https://twitter.com/i/search/timeline?vertical" + str(
                        url1) + '&reset_error_state=false'
                    print  " cc -----  ", first_url

                    response = requests.get(first_url, headers=header, allow_redirects=True)
                    json_data = json.loads(response.text)
                    tweet_tweet_id_list, i = Smart_extract_json(json_data, tweet_tweet_id_list, i)
                    # print " json data ",json_data
                    new_latent_count = json_data['new_latent_count']
                    print " avant new_latent_count  ", json_data['new_latent_count']
                    while new_latent_count <> 0:
                        new_latent_count = 0
                        bypass_url = first_url.split("&max_position=")
                        # url_timelines = "https://twitter.com/i/search/timeline?vertical=default&q=%D8%AE%D8%A7%D8%B4%D9%82%D8%AC%D9%8A&include_available_features=1&include_entities=1&max_position="
                        if 'min_position' in json_data:
                            print 'min_position : ', json_data['min_position']
                            second_url = bypass_url[0] + "&max_position=" + json_data[
                                'min_position'] + '&reset_error_state=false"'
                            print " url is ", second_url
                            response = requests.get(second_url, headers=header, allow_redirects=True)
                            json_data = json.loads(response.text)
                            tweet_tweet_id_list, i = Smart_extract_json(json_data, tweet_tweet_id_list, i)
                            new_latent_count = json_data['new_latent_count']
                            print " apres new_latent_count  ", json_data['new_latent_count']

                        time.sleep(3)
                        # exit(0)
                        # print " jj ------------------------------------------------------------------------------------------------------------------------------------------------------------"

                        ##print entry
                        # response = requests.get(url)
                        # json_data = json.loads(response.text)
                        ##print json_data
                        ##print "------------------------------------------------------------------------------------------------------------------------------------------------------------"

    # driver.execute_script("arguments[0].setAttribute('style', 'height:88845px')", body)
    time.sleep(100)


def gestion_elem(items, tweet_tweet_id_list, i):
    item_iterator = 0
    for t in items:
        if item_iterator > 0:
            # print t
            tweet_tweet_id = ''
            tweet_item_id = ''
            tweet_permalink_path = ''
            tweet_name = ''
            tweet_user_id = ''
            tweet_text = ''
            tweet_tweet_type = ''
            tweet_component_context = ''
            tweet_card_type = ''
            tweet_has_card = 'false'
            tweet_time_title = ''
            tweet_time_unix = ''
            tweet_has_parent = 'false'
            tweet_conversation_id = ''
            tweet_nonce_nonce = ''
            tweet_stat_initialized = ''
            tweet_conversation_section = ''
            tweet_disclosure_type = ''
            tweet_text_final = ''
            tweet_text_split = ''
            hashtags = []
            images = []
            video = []

            ##print "  1  " , t

            # #print ' 0 here ' , t


            tweet = t.split('data-tweet-id="', 1)
            if (len(tweet) > 1):
                tweet_tweet_id = tweet[1].split('"', 1)
                tweet_tweet_id = tweet_tweet_id[0]
            # print " tweet id " , tweet_tweet_id
            # exit(0)
            afficher = i + item_iterator
            print afficher, "  _   ", tweet_tweet_id, "  is in ", tweet_tweet_id in tweet_tweet_id_list
            if tweet_tweet_id not in tweet_tweet_id_list:

                tweet = t.split('data-disclosure-type="', 1)
                if (len(tweet) > 1):
                    tweet_disclosure_type = tweet[1].split('"', 1)
                    tweet_disclosure_type = tweet_disclosure_type[0]

                tweet = t.split('data-tweet-nonce="', 1)
                if (len(tweet) > 1):
                    tweet_nonce_nonce = tweet[1].split('"', 1)
                    tweet_nonce_nonce = tweet_nonce_nonce[0]

                tweet = t.split('data-tweet-stat-initialized="', 1)
                if (len(tweet) > 1):
                    tweet_stat_initialized = tweet[1].split('"', 1)
                    tweet_stat_initialized = tweet_stat_initialized[0]

                tweet = t.split('data-conversation-section-quality="', 1)
                if (len(tweet) > 1):
                    tweet_conversation_section = tweet[1].split('"', 1)
                    tweet_conversation_section = tweet_conversation_section[0]

                tweet = t.split('data-has-parent-tweet="', 1)
                if (len(tweet) > 1):
                    tweet_has_parent = tweet[1].split('"', 1)
                    tweet_has_parent = tweet_has_parent[0]

                tweet = t.split('data-conversation-id="', 1)
                if (len(tweet) > 1):
                    tweet_conversation_id = tweet[1].split('"', 1)
                    tweet_conversation_id = tweet_conversation_id[0]

                # #print ' 1 here '
                tweet = t.split('data-item-id="', 1)
                if (len(tweet) > 1):
                    tweet_item_id = tweet[1].split('"', 1)
                    tweet_item_id = tweet_item_id[0]
                # #print ' 2 here '
                tweet = t.split('data-permalink-path="', 1)
                if (len(tweet) > 1):
                    tweet_permalink_path = tweet[1].split('"', 1)
                    tweet_permalink_path = tweet_permalink_path[0]
                # #print ' 3 here '
                tweet = t.split('data-name="', 1)
                if (len(tweet) > 1):
                    tweet_name = tweet[1].split('"', 1)
                    tweet_name = tweet_name[0]
                # #print ' 4 here'
                tweet = t.split('data-user-id="', 1)
                if (len(tweet) > 1):
                    tweet_user_id = tweet[1].split('"', 1)
                    tweet_user_id = tweet_user_id[0]
                # #print ' 5 here'
                tweet = t.split('data-item-type="', 1)
                if (len(tweet) > 1):
                    tweet_tweet_type = tweet[1].split('"', 1)
                    tweet_tweet_type = tweet_tweet_type[0]
                # #print ' 6 here'
                tweet = t.split('data-mentions="', 1)
                # #print len(tweet) , ' 6 here' , tweet
                if (len(tweet) > 1):
                    tweet_tweet_mentions = tweet[1].split('"', 1)
                    tweet_tweet_mentions = tweet_tweet_mentions[0]
                # #print ' 7 here '
                tweet = t.split('data-card2-type="', 1)
                if (len(tweet) > 1):
                    tweet_card_type = tweet[1].split('"', 1)
                    tweet_card_type = tweet_card_type[0]

                tweet = t.split('data-has-cards="', 1)
                if (len(tweet) > 1):
                    tweet_has_card = tweet[1].split('"', 1)
                    tweet_has_card = tweet_has_card[0]

                tweet = t.split('data-component-context="', 1)
                if (len(tweet) > 1):
                    tweet_component_context = tweet[1].split('"', 1)
                    tweet_component_context = tweet_component_context[0]

                tweet = t.split('js-permalink js-nav js-tooltip" title="', 1)
                if (len(tweet) > 1):
                    tweet_time_title = tweet[1].split('"', 1)
                    tweet_time_title = tweet_time_title[0]

                tweet = t.split('data-time="', 1)
                if (len(tweet) > 1):
                    tweet_time_unix = tweet[1].split('"', 1)
                    tweet_time_unix = tweet_time_unix[0]
                # #print ' here '
                tweet = t.split('<p class="TweetTextSize', 1)
                if (len(tweet) > 1):
                    tweet_text = tweet[1].split('</p>', 1)
                    tweet_text = tweet_text[0]
                    tweet_text = tweet_text.split(">")
                    for text in tweet_text:
                        ##print " text is ",text
                        tweet_text_split = text.split("<")
                        ##print len(tweet_text_split) , " split ",tweet_text_split

                        if len(tweet_text_split) > 1:
                            tweet_text_final = tweet_text_final + tweet_text_split[0]
                            ##print " finaal  ",tweet_text_final

                tweet = t.split('href="/hashtag')
                # print len(tweet)," hash ",tweet
                if (len(tweet) > 1):
                    # hashtags_items = tweet[1]
                    j = 0
                    for item in tweet:
                        # print " hashtag item ", item
                        if j % 2:
                            ##print item
                            hashtag_complet = ''
                            hashtag_complet = item.split('"')
                            hashtag_complet = "/hashtag" + hashtag_complet[0]
                            # print " complet = ",hashtag_complet
                            hashtags.append(hashtag_complet)
                            # exit(0)
                        j += 1
                tweet = t.split('data-image-url="')
                # print len(tweet), " data-image-url= ", tweet
                if (len(tweet) > 1):
                    # image_items = tweet[1]
                    j = 0
                    for item in tweet:
                        # print " image item ", item
                        if j % 2:
                            # #print item
                            image_complet = ''
                            image_complet = item.split('"')
                            image_complet = image_complet[0]
                            # print " complet = ", image_complet
                            images.append(image_complet)
                            # exit(0)
                        j += 1
                tweet = t.split('data-video-url="')
                ##print len(tweet), " data-video-url= ", tweet
                if (len(tweet) > 1):
                    # image_items = tweet[1]
                    j = 0
                    for item in tweet:
                        ##print " video item ", item
                        if j % 2:
                            # #print item
                            video_complet = ''
                            video_complet = item.split('"')
                            video_complet = video_complet[0]
                            # print " video item = ", video_complet
                            video.append(video_complet)
                            # exit(0)
                        j += 1

                tweet_response = 0
                tweet_retweets = 0
                tweet_j_aime = 0

                statistique = t.split('<span class="ProfileTweet-action--retweet u-hiddenVisually">')
                # print  len(statistique), "   statistique  :  " ,statistique
                if len(statistique) > 1:
                    statistique_reponse = statistique[0]

                    recher1 = '<span class="ProfileTweet-actionCount" data-tweet-stat-count="'
                    recher2 = '<span class="ProfileTweet-actionCount" aria-hidden="true" data-tweet-stat-count="'
                    if recher1 in statistique_reponse:
                        tweet = statistique_reponse.split(recher1, 1)
                    else:
                        tweet = statistique_reponse.split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_response = tweet[1].split('"', 1)
                        tweet_response = tweet_response[0]

                    statistique_retweet_j_aime = statistique[1].split(
                        '<span class="ProfileTweet-action--favorite u-hiddenVisually">')

                    if recher1 in statistique_retweet_j_aime[0]:
                        tweet = statistique_retweet_j_aime[0].split(recher1, 1)
                    else:
                        tweet = statistique_retweet_j_aime[0].split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_retweets = tweet[1].split('"', 1)
                        tweet_retweets = tweet_retweets[0]

                    if recher1 in statistique_retweet_j_aime[1]:
                        tweet = statistique_retweet_j_aime[1].split(recher1, 1)
                    else:
                        tweet = statistique_retweet_j_aime[1].split(recher2, 1)

                    if (len(tweet) > 1):
                        tweet_j_aime = tweet[1].split('"', 1)
                        tweet_j_aime = tweet_j_aime[0]

                print " Tweet (", afficher, ")  __ : ", tweet_time_title, "tweet_tweet_id : ", tweet_tweet_id, \
                    " , tweet_item_id : ", tweet_item_id \
                    , " , tweet_permalink_path : ", tweet_permalink_path, " , tweet name : ", tweet_name, \
                    " , tweet user id : ", tweet_user_id, " , tweet_tweet_type : ", tweet_tweet_type, \
                    " , tweet card type  : ", tweet_card_type, " , tweet_has_card : ", tweet_has_card, \
                    " , tweet_component_context : ", tweet_component_context, " , tweet_time_title : ", tweet_time_title, \
                    " , tweet_text : ", tweet_text_final, " tweet_time_unix :", tweet_time_unix, \
                    " , tweet_response : ", tweet_response, " , tweet_retweets : ", tweet_retweets, \
                    " , tweet_j_aime : ", tweet_j_aime, " , tweet hashtags : ", hashtags, \
                    " , tweet images : ", images, " , tweet video : ", video

                # ajouter_tweet(cursor, connection, i, "tweets", tweet_tweet_id, tweet_item_id,
                #              tweet_user_id, tweet_tweet_type,
                #             tweet_name, tweet_permalink_path, tweet_has_card, tweet_card_type,
                #              tweet_component_context,
                #              tweet_time_title, tweet_text, tweet_response, tweet_retweets,
                #              tweet_j_aime, tweet_time_unix,
                #              tweet_disclosure_type, tweet_nonce_nonce, tweet_stat_initialized,
                #             tweet_conversation_section,
                #             tweet_conversation_id, tweet_has_parent, "Maroc", cher)
                #
                # connection.commit()
                tweet_tweet_id_list.append(tweet_tweet_id)

                # exit(0)

        item_iterator += 1

    return tweet_tweet_id_list, afficher


if __name__ == '__main__':

    Smart_tweets()