from _0_Functions import *


def reaction(first_sql, file, usr, pwd,  nom, lock, fin=0):
    connection = connect_db()
    cursor = connection.cursor()

    next_feeds = ''
    feeds = ''
    likes = ''
    likes_next = ''
    graph = ''
    count = 0
    json_data = 0
    post = ''

    cursor.execute(first_sql)
    groups = cursor.fetchall()

    for group in groups:

        id_group = group[0]
        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, file, lock)
            if json_data == 100:
                print json_data, "  Au suivant "

            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
        else:
            post_id = str(id_group) + '?fields=feed{created_time,reactions{id,name,type}}'
            graph = retourner_graph(file, usr, pwd,  lock)
            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " post id error ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100
            if 'feed' in post and 'data' in post['feed']:
                feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                next_feeds = post['feed']['paging']['next']
                # update_next(id_group, 'feed_reactions_next', next_feeds, cursor, connection,count,nom)

        while feeds <> '':
            for feed in feeds:
                created_time = uniix_time(feed['created_time'])
                # if (created_time > 1483228800):  # 01/01/2017
                if 'reactions' in feed:
                    if 'data' in feed['reactions']:
                        likes = feed['reactions']['data']
                    if 'paging' in feed['reactions'] and 'next' in feed['reactions']['paging']:
                        likes_next = feed['reactions']['paging']['next']

                    while likes <> '':
                        for like in likes:
                            fe_id = feed['id'].split('_', 1)
                            count += 1
                            name = ''
                            type_r = ''
                            if 'name' in like:
                                name = like['name']
                            if 'type' in like:
                                type_r = like['type']
                            ajout_like(like['id'], fe_id[1], 'f', type_r, cursor, name, connection, count,
                                       str(count) + "_" + nom)
                        connection.commit()

                        likes = ''
                        if likes_next <> '':
                            json_data = url_Suivant(likes_next, usr, pwd, file, lock)
                            likes_next = ''
                            if json_data == 100:
                                print json_data, "  au suivant "
                            else:
                                if 'data' in json_data:
                                    likes = json_data['data']
                                if 'paging' in json_data and 'next' in json_data['paging']:
                                    likes_next = json_data['paging']['next']
                                    # update_next(id_group, 'feed_reactions_next', likes_next, cursor, connection, count,nom)

                                    # else:
                                    # next_feeds = ''
                                    # json_data = 'Move To next element'

            feeds = ''
            if next_feeds <> '' and json_data <> 100:
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if json_data == 100:
                    print json_data, " au suivant "
                else:
                    # print 'json data '+json_data
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = json_data['paging']['next']
                        print "next feedssss", next_feeds
                        update_next(id_group, 'feed_reactions_next', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']

        if json_data <> 100:
            sql = " update nexts set feed_reactions_next = '1' where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break

    connection.close()


def lancer_reaction():
    nbr = 0

    usr_p = [usr_pwd[0], usr_pwd[1]]

    sql2 = " select idgroup,feed_reactions_next from groups , nexts where idNexts=idgroup  and " \
           " ( feed_reactions_next <> '1' )  "  # or feed_reactions_next <> '1'
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()
    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=reaction,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread0 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            thread_1.join()
            os._exit(0)
        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            nbr = 0
    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=reaction,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=reaction,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    else:
        os._exit(0)


if __name__ == "__main__":
    lancer_reaction()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_reaction()
