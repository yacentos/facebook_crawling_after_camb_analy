from requests import ConnectionError

from _0_Functions import *


def feedmessage_tags(first_sql, file, usr, pwd,  nom, lock, fin=0):
    connection = connect_db()
    cursor = connection.cursor()

    next_feeds = ''
    feeds = ''
    message_tags = ''
    message_tags_next = ''
    fee_next = ''
    count = 0
    post=''
    rupture = 0
    json_data = 0
    cursor.execute(first_sql)
    groups = cursor.fetchall()

    for group in groups:
        id_group = group[0]
        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, file, lock)
            if json_data == 100:
                print json_data, "  au suivant "
            else:
                if 'data' in json_data:
                    feeds = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']
        else:
            post_id = str(id_group) + '?fields=feed{message_tags}'
            graph = retourner_graph(file, usr, pwd, lock)
            try:
                post = graph.get_object(id=post_id)
            except facebook.GraphAPIError as ex:
                print " post id error ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100
            except ConnectionError as ex2 :
                print " post id error ", ex2
                sql = " update nexts set error ='" + str(ex2).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
                next_feeds = ''
                json_data = 100
            if 'feed' in post and 'data' in post['feed']:
                    feeds = post['feed']['data']
            if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                    next_feeds = post['feed']['paging']['next']

        while feeds != '':
            for feed in feeds:
                if 'message_tags' in feed:
                    message_tags = feed['message_tags']
                    if 'paging' in feed['message_tags'] and 'next' in feed['message_tags']['paging']:
                        message_tags_next = feed['message_tags']['paging']['next']
                while message_tags != '':
                    for message in message_tags:
                        fe_id = feed['id']
                        fe_id = fe_id.split('_', 1)
                        count += 1
                        ajout_tag('message', message['id'], fe_id[1], 'f', cursor, message['name'], 'need_search',
                                  fe_id[0],connection, count,id_group, nom)
                    message_tags = ''
                    if message_tags_next != '':
                        json_data = url_Suivant(message_tags_next, usr, pwd, file, lock)
                        message_tags_next = ''
                        if json_data == 100:
                            print json_data, "  au suivant "
                        else:
                            if 'data' in json_data:
                                message_tags = json_data['data']
                            if 'paging' in json_data and 'next' in json_data['paging']:
                                message_tags_next = json_data['paging']['next']
                                #update_next(id_group, 'message_tags_next_feeds', message_tags_next, cursor, connection, count, nom)
            feeds = ''
            if next_feeds != ''and json_data <> 100 :
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if json_data == 100:
                    print json_data, "  au suivant "
                else:
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_feeds = json_data['paging']['next']
                        update_next(id_group, 'message_tags_next_feeds', next_feeds, cursor, connection, count, nom)
                    if 'data' in json_data:
                        feeds = json_data['data']
        if json_data <> 100:
            sql = " update nexts set message_tags_next_feeds = '1'   where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) < 2 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break
    connection.close()


def lancer_feedmessage_tags():
    nbr = 0

    usr_p = [("ali.boulmi@gmail.com", "123456789@1", "../Token/ali.boulmi"),
           ("inouflmsti@gmail.com", "123456789@1 ", "../Token/inouflmsti")]

    sql2 = " select idgroup,message_tags_next_feeds from groups , nexts where idNexts=idgroup   " \
           "and (  message_tags_next_feeds <> '1')  "

    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()
    if len(nbr_groups) == 1 :
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=feedmessage_tags,
                                        args=(sql2+" order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1],  "thread0 " + str(nbr), lock))
            thread_1.start()
            thread_1.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    elif len(nbr_groups) > 1 :
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=feedmessage_tags,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=feedmessage_tags,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    else:
        os._exit(0)

if __name__ == "__main__":
    lancer_feedmessage_tags()

    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_feedmessage_tags()
