from _0_Functions import *


def Membres(first_sql, f, usr, pwd,  nom, lock, fin=0):
    connection = connect_db()
    cursor = connection.cursor()
    # ########################      information sur les Memebers      ###############################
    members = ''
    next_members = ''
    mem_next = ''
    get_members = ''
    count = 0
    graph = ''
    json_data = 0

    cursor.execute(first_sql)
    groups = cursor.fetchall()

    for group in groups:
        id_group = group[0]
        if len(group[1]) > 10:
            json_data = url_Suivant(group[1], usr, pwd, f, lock)
            if json_data == 100:
                print json_data, "  au suivant "
            else:
                if 'data' in json_data:
                    members = json_data['data']
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_members = remplacer_limit(json_data['paging']['next'])
        else:
            get_members = str(id_group) + '?fields=members{id,name,administrator,friends{id,name,timezone}}'
            graph = retourner_graph(f, usr, pwd, lock)
            try:
                post = graph.get_object(id=get_members)
            except facebook.GraphAPIError as ex:
                print " post id error ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(
                    id_group)
                cursor.execute(sql)
                connection.commit()
                members = ''
                mem_next = ''
                json_data = 100
            if 'members' in post and 'data' in post['members']:
                    members = post['members']['data']
            if 'members' in post and 'paging' in post['members'] and 'next' in post['members']['paging']:
                    next_members = remplacer_limit(post['members']['paging']['next'])

        print get_members
        print mem_next, '    next   ', next_members

        while members <> '':  # members <> '' :#count < 47611:
            for member in members:
                count += 1
                name = ''
                if 'name' in member:
                    name = member['name']

                ajout_acteur(str(member['id']), name.replace("'", ""), cursor, connection, count, nom)
                ajout_membre(str(id_group), str(member['id']), str(member['administrator']), cursor, connection, count,
                             nom)

            members = ''
            if next_members <> '':
                json_data = url_Suivant(next_members, usr, pwd, f, lock)
                next_members = ''
                if json_data == 0:
                    print json_data, "  au suivant "
                else:
                    if 'data' in json_data:
                        members = json_data['data']
                    if 'paging' in json_data and 'next' in json_data['paging']:
                        next_members = remplacer_limit(json_data['paging']['next'])
                        update_next(id_group, 'feed_member_next', next_members, cursor, connection, count, nom)
        if json_data <> 100:
            sql = " update nexts set feed_member_next = '1'   where idNexts=" + str(id_group)
            print len(groups), " ==> update fin memebre : ", sql
            # time.sleep(30)
            cursor.execute(sql)
            connection.commit()

            cursor.execute(first_sql)
            groups = cursor.fetchall()
            print len(groups), " ==> debut d 'un autre : ", fin

            if len(groups) == 1 and fin == 0:
                fin = 1
            elif fin == 1 or len(groups) == 0:
                print nom, " a fini son travail "
                # time.sleep(30)
                break
    connection.close()


def lancer_Membres():
    nbr = 0

    usr_p = [("ali.boulmi@gmail.com", "123456789@1", "../Token/ali.boulmi"),
           ("inouflmsti@gmail.com", "123456789@1 ", "../Token/inouflmsti")]

    sql2 = " select idgroup,feed_member_next from groups , nexts where idNexts=idgroup and" \
           " groups.type like 'Group' and ( feed_member_next is null or  feed_member_next <> '1') "  # like ' ')"#
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(sql2)
    nbr_groups = cursor.fetchall()
    lock = threading.Lock()
    print " noooombr nbr groups ", len(nbr_groups)
    connection.close()
    if len(nbr_groups) == 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=Membres,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1],
                                              "thread0 " + str(nbr), lock))
            thread_1.start()
            thread_1.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    elif len(nbr_groups) > 1:
        try:
            usr_c = usr_p[0]

            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_1 = threading.Thread(target=Membres,
                                        args=(sql2 + " order by idgroup ASC", usr_c[2], usr_c[0], usr_c[1],
                                              "thread1 " + str(nbr), lock))
            thread_1.start()
            nbr += 1
            usr_c = usr_p[1]
            print "   nombre   ", nbr, "   ", usr_c[2], "   ", usr_c[0], "   ", usr_c[1], "   ",
            thread_2 = threading.Thread(target=Membres,
                                        args=(sql2 + " order by idgroup DESC", usr_c[2], usr_c[0], usr_c[1], 
                                              "thread2 " + str(nbr), lock))
            thread_2.start()

            thread_1.join()
            thread_2.join()
            os._exit(0)

        except SystemExit:
            print " syyyyyyyyyyyyyyyyyyyyyyyyystem exit "
            # time.sleep(10)
            nbr = 0
    else:
        os._exit(0)

if __name__ == "__main__":
    lancer_Membres()
    while SystemExit:
        # print " uuuuuuuuuuuuuuuu "
        lancer_Membres()
