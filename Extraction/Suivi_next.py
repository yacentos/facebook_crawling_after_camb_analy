import xlsxwriter
from _0_Functions import *

dossier = "DBS_statistics/"

def Evolution(databases):


    for db in databases :
        ev_suivi = dossier +"db_statistic_" + db
        connection = connect_db_db(db)
        cursor = connection.cursor()
        cursor2 = connection.cursor()

        workbook = xlsxwriter.Workbook(ev_suivi+'.xlsx')
        worksheet = workbook.add_worksheet()

        # Add a bold format to use to highlight cells.
        bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})

        worksheet.merge_range('A1:A2', 'Id Groupe', bold)
        worksheet.merge_range('B1:B2', 'Nom', bold)
        worksheet.merge_range('C1:C2', 'Date Creation', bold)
        worksheet.merge_range('D1:L1', 'Suivi Operation', bold)
        worksheet.write('D2', 'Member_Next', bold)
        worksheet.write('E2', 'Publication_Next', bold)
        worksheet.write('F2', 'Commentaire_Next', bold)
        worksheet.write('G2', 'Likes_Next', bold)
        worksheet.write('H2', 'Reaction_Next', bold)
        worksheet.write('I2', 'With_tags', bold)
        worksheet.write('J2', 'Message_tags', bold)
        worksheet.write('K2', 'Commentaire_tags', bold)
        worksheet.write('L2', 'Partage_Next', bold)

        worksheet.merge_range('M1:M2', 'Total', bold)

        sql = " SELECT idgroup,name,feed_member_next,next_feeds,feed_comments_next,feed_likes_next,feed_reactions_next ," \
              " with_tags_next_feeds ,   message_tags_next_feeds,  comments_tags_next_feeds , story_tags_next_feeds " \
              "FROM nexts,groups where groups.idgroup=nexts.idnexts  "#  and next_feeds='1'  limit 1 " # where idGroup = "
        print sql
        cursor2.execute(sql)
        groups = cursor2.fetchall()
        row = 2
        col = 0

        print " nombre ",len(groups)
        for group in groups:

            memb_next= 0
            pub_next= 0
            comm_next= 0
            like_next= 0
            react_next= 0
            with_tags =1
            message_tages =1
            comm_tags= 0
            partage_next= 0

            id_group = group[0]
            d = '---'
            nom = '----'
            """
            sql = " SELECT min(createdtime) FROM feed where feed.idgroup="+str(id_group)#+" limit 1 " # where idGroup = "
            print sql
            print groups
            cursor.execute(sql)
            feeds = cursor.fetchone()

            if feeds[0] != None:
                #d = datetime.datetime.fromtimestamp(int(feeds[0])).strftime('%d-%m-%Y')
                format2 = workbook.add_format({'num_format': 'dd/mm/yy'})
                worksheet.write(row, col + 2, feeds[0], format2)  # 28/02/13
            else:
                worksheet.write(row, col + 2, "---")  # 28/02/13
            """
            worksheet.write(row, col + 2,db)  # 28/02/13
            if group[1] != None:
                nom = group[1]
            if group[2] <> '1':
                memb_next= 0
            if group[3] <> '1':
                pub_next= 0
            if group[4] <> '1':
                comm_next= 0
            if group[5] <> '1':
                like_next= 0
            if group[6] <> '1':
                react_next= 0
            if group[7] <> '1':
                with_tags= 0
            if group[8] <> '1':
                message_tages= 0
            if group[9] <> '1':
                comm_tags= 0
            if group[10] <> '1':
                partage_next= 0

            worksheet.write_number(row, col, id_group)
            worksheet.write_string(row, col + 1, nom)


            # worksheet.write_datetime(row, col + 2, d)
            worksheet.write_number(row, col + 3, memb_next)
            worksheet.write_number(row, col + 4, pub_next)
            worksheet.write_number(row, col + 5, comm_next)
            worksheet.write_number(row, col + 6, like_next)
            worksheet.write_number(row, col + 7, react_next)
            worksheet.write_number(row, col + 8, with_tags)
            worksheet.write_number(row, col + 9, message_tages)
            worksheet.write_number(row, col + 10, comm_tags)
            worksheet.write_number(row, col + 11, partage_next)

            worksheet.write_formula(row, col + 12, '=SUM($D$' + str(row+1) + ':$L$' + str(row+1) + ')')
            row += 1

        print " rooow ici ",row

        worksheet.merge_range('A' + str(row) + ':C' + str(row), 'Total', bold)
        row -= 1
        worksheet.write_formula(row, col + 3, '=SUM(D3:D' + str(row ) + ')')
        worksheet.write_formula(row, col + 4, '=SUM(E3:E' + str(row ) + ')')
        worksheet.write_formula(row, col + 5, '=SUM(F3:F' + str(row ) + ')')
        worksheet.write_formula(row, col + 6, '=SUM(G3:G' + str(row ) + ')')
        worksheet.write_formula(row, col + 7, '=SUM(H3:H' + str(row ) + ')')
        worksheet.write_formula(row, col + 8, '=SUM(I3:I' + str(row ) + ')')
        worksheet.write_formula(row, col + 9, '=SUM(J3:J' + str(row) + ')')
        worksheet.write_formula(row, col + 10, '=SUM(K3:K' + str(row) + ')')
        worksheet.write_formula(row, col + 11, '=SUM(L3:L' + str(row) + ')')

        workbook.close()
        connection.close()

if __name__ == "__main__":
    dbs = []
    dbs.append("facebook_share")
    dbs.append("celebreties")
    dbs.append("maroc36")

    Evolution(dbs)