#import _suivi_publication
from _0_Functions import *


def ajout_dans_groups():

    connection = connect_db()
    cursor = connection.cursor()
    count = 1
    sql = " select id_group_destination,story from shares where id_group_destination not in ( select idGroup from groups ) group by id_group_destination"
    sql = " select idGroup from groups "
    cursor.execute(sql)
    groups = cursor.fetchall()
    graph = ''
    graph = retourner_graph(file, usr, pwd, lock)
    for group in groups:

        id_group = group[0]
        print id_group,"        ",group[1]
        if 'group' in group[1]:
            post_id = str(id_group) + '?fields=updated_time,name,privacy,email,id,owner,icon,description,members{id,name}'
            post = graph.get_object(id=post_id)
            ajout_group(post, cursor, connection, 0, '', count,'Group')

        elif 'page' in group[1]:
            print " Paaaaaaaaage  "
            post_id = str(id_group) + '?fields=fan_count,name'
            post = graph.get_object(id=post_id)
            ajout_group(post, cursor, connection, 1, '', count,'Page')
        elif  'event' in group[1]:
            print " Event  "
            post_id = str(id_group) + '?fields=name'
            post = graph.get_object(id=post_id)
            ajout_group(post, cursor, connection, 1, '', count,'Event')
    cursor.close()
    connection.close()
    #_suivi_publication.main()
    os._exit(0)


def mise_a_jour_dans_groups(sql,f, usr, pwd, lock):
    connection = connect_db()
    cursor = connection.cursor()
    count = 1

    cursor.execute(sql)
    groups = cursor.fetchall()
    graph = ''
    graph = retourner_graph(f, usr, pwd, lock)
    for group in groups:

        id_group = group[0]
        print id_group, "        "
        try:
                post_id = str(id_group) + '?fields=updated_time,name,privacy,email,id,owner,icon,description,members{id,name}'

                post = graph.get_object(id=post_id)
                update_group(post, cursor, connection, count, 'Group','group','oki')

        except Exception as e:
            try:
                print " errooor ",e
                print " Paaaaaaaaage  "
                post_id = str(id_group) + '?fields=fan_count,name'
                post = graph.get_object(id=post_id)
                update_group(post, cursor, connection, count, 'Page',' page','oki')
            except Exception as e:
                    print " errooor ", e
                    print " Event  "
                    post["id"]=id_group
                    update_group(post, cursor, connection,count, 'Event',' update ',e)
    cursor.close()
    connection.close()
    # _suivi_publication.main()
    os._exit(0)

if __name__ == "__main__":

    lock = threading.Lock()
    usr_p = [("crawling.user2@gmail.com", "123456789@1", "../Token/crawling.user2","1-1-1988"),
           ("sanasara265@gmail.com", "123456789abcd", "../Token/sanasara265","1-1-1988")]

    #ajout_dans_groups()
    usr_c = usr_p[0]
    nbr = 1
    sql2 = " select idNexts from nexts "
    mise_a_jour_dans_groups(sql2, usr_c[2], usr_c[0], usr_c[1],lock)

