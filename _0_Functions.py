# -*- coding: utf-8 -*-
import calendar
import datetime
import datetime as dt
import json
import mysql.connector
import os
import pickle
import time
import urllib2
import threading
import facebook
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from pathlib import Path
from datetime import datetime
import sys

reload(sys)

sys.setdefaultencoding('utf-8')


chromedriver = ""
database_name = ""  # "maroc36"  # bi3_chri facebook_share   maroc36
database_password = ''
is_paramtrable = 0
renouvlement_token = 0
mot_cle = ''
parametre_group = ''

#mot_cle = 'and hs.hashtag_name like "%سنطرال%" or hs.hashtag_name like "%خليه%" or hs.hashtag_name like "%افريقيا%" or hs.hashtag_name like "%مازوط%") '
#mot_cle = 'and hs.hashtag_name like "%النكبة%" or hs.hashtag_name like "%مليونية_العودة‬%" or hs.hashtag_name like "%يوم_العبور%" or hs.hashtag_name like "%جمعة_النذير%") '

usr_pwd = []
# "/home/yacentos/parametres.txt"
# "D:\Token_Facebook\parametres.txt"

fp = open("E:\Token_Facebook\parametres.txt")
for i, line in enumerate(fp):
    if i == 0:
        chromedriver = str(line).replace("\n", '')
    elif i == 1:
        database_name = str(line).replace("\n", '')
    elif i == 2:
        database_password = str(line).replace("\n", '')
    elif i == 3:
        is_paramtrable = 1
        parametre_group = str(line).replace("\n", '')
        if not parametre_group.__contains__('#'):
            fp_p = open("D:\Token_Facebook\mot_cles.txt")
            linee = fp_p.readline()
            mot_cles = str(linee).split("#")
            mot_cle = mot_cles[0]
            #mot_cles = str(mot_cles).split("||")
            #mot_cherche = mot_cles[0]
            #mot_cles = str(mot_cles[1]).split(",")
            # print "  mot_cles  ", mot_cles
            #for mot in mot_cles:
                #mot_cle = mot_cle + ' and '+ mot_cherche +' like "%' + str(mot) + '%"'
            fp_p.close()
        parametre_group = parametre_group +  str(mot_cle)
    elif i == 4:
        SCROLL_PAUSE_TIME = int(str(line).replace("\n", ''))
    else:
        usr = str(line).split(",")
        #print usr
        usr_to_add = (usr[0], usr[1], usr[2], usr[3], usr[4], usr[5])
        usr_pwd.append(usr_to_add)
fp.close()

#print parametre_group, "|  :  ", database_name, "  :  ", database_password, "  :   ", usr_pwd

nombre_arret = 100000000000000000450
Date_D_arret = 1483228800  # 01/01/2017


def connect_db_db(db):
    print "  Connect to Mysql db  =>  ", db
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=db,
                                   charset='utf8mb4', use_unicode=True)

def connect_db():
    print "  Connect to Mysql db  =>  ", database_name
    return mysql.connector.connect(user='root', password=database_password, host='localhost', database=database_name,
                                   charset='utf8mb4', use_unicode=True)

def Configurer_driver():
    caps = DesiredCapabilities.CHROME
    caps['loggingPrefs'] = {'performance': 'ALL'}
    header = {
        "user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options, desired_capabilities=caps)
    return  driver

def after_url_detection():

        driver = Configurer_driver()
        for usr in usr_pwd:
            Authentification(driver, usr[0], usr[1])
            after_url = ''
            for entry in driver.get_log('performance'):
                timeline = str(entry)
                if '?modules=ChatSidebarSheet.react' in timeline:
                    print " timeline : ", timeline
                    after_url = timeline.split('?modules=ChatSidebarSheet.react')
                    after_url = after_url[1].split('"}')
                    after_url = after_url[0]
                    print usr, " after_url  ", after_url
                    break
            deconnexion_user_without_driver(driver)

def More_element(share, driver):
    there_is_more = 1
    while there_is_more == 1:
        more_comments = share.find_elements_by_css_selector("a[class='UFIPagerLink']")
        if len(more_comments) == 0:
            there_is_more = 0
        else:
            for elem in more_comments:
                print " - CLick on : ", elem
                driver.execute_script("arguments[0].click();", elem)
                time.sleep(5)


def deconnexion_user_without_driver(driver):
    try:
        url = 'https://www.facebook.com/'
        driver.get(url)
        time.sleep(3)

        more_comments = driver.find_element_by_css_selector("a[class='_5lxs _3qct _p']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(5)

        more_comments = driver.find_element_by_css_selector("form[class='_w0d _w0d']")
        print " - CLick on : ", more_comments
        more_comments.submit()
        # driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

    except Exception as e:
        print " ID introuvable :  ", e

"""
def Verify_token(file_t, loc):
    err = 0
    my_token = ""
    e3 = ''
    try:
        my_token = lecture_fichier(file_t, loc)
        graph = facebook.GraphAPI(access_token=my_token, version='2.7')
        graph.get_object("me")
        err = 1
        e3 = my_token
        #ecriture_fichier2(file_t, my_token)
    except facebook.GraphAPIError  as e3:
        print " facebook.GraphAPIError 3 :  ", e3

    return (graph, err,e3)
"""

def update_disapear_time(table, field_update, critere, con, cur, count, nom):
    try:
        sql = " update "+str(table)+" set "+str(field_update)+" where " + str(critere)
        print "(", count, "_", nom, ") Sql update "+str(table)+"  : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update "+str(table)+"  Exception  :  ", e

def retourner_graph(fichier, usr, pwd, lock):
        graph = ''
        error = 0
        error_message2 = ""
        my_token = ""
        heure_actuelle()
        try:
            my_token = lecture_fichier(fichier, lock)
            graph = facebook.GraphAPI(access_token=my_token, version='2.7')
            graph.get_object("me")
            error = 1
            etape = 1
        except facebook.GraphAPIError  as e3:
            print " facebook.GraphAPIError 3 :  ", e3
            error_message2 = e3
            type_error = gestion_error(error_message2)
            if type_error == 100:
                return 100

            if renouvlement_token == 1:
                while renouvlement_token == 1:
                    time.sleep(2)
            my_token = lecture_fichier(fichier, lock)
            print fichier, " lectuuuuuuuuuuuuuuuuuuuure ", my_token
            try:
                print "  url a chercher phasee 2 : ",my_token
                graph = facebook.GraphAPI(access_token=my_token, version='2.7')
                graph.get_object("me")
                error = 1
                etape = 2
            except facebook.GraphAPIError  as e3:
                error_message2 = e3
                print error_message2, " facebook.GraphAPIError 2 : ", error_message2

                type_error = gestion_error(error_message2)
                if type_error == 100:
                    return 100

                my_token = generation_token(usr, pwd, lock)
                ecriture_fichier(fichier, my_token, lock)
                #url = remplacer_token(url, my_token)
                try:
                    print "  url a chercher phase 3 : ",my_token
                    graph = facebook.GraphAPI(access_token=my_token, version='2.7')
                    graph.get_object("me")
                    error = 1
                    etape = 3
                except facebook.GraphAPIError  as e3:
                    error_message2 = e3
                    print error_message2, " facebook.GraphAPIError 4 : ", e3

                    type_error = gestion_error(error_message2)
                    if type_error == 100:
                        return 100
        return graph

def heure_actuelle():
    now = dt.datetime.utcnow()
    print ' Date recherche : ', now.day, "-", now.month, "-", now.year, "__", now.hour, ":", \
        now.minute, ":", now.second

def gestion_error(message_error):
    if "Une tentative de connexion a" in str(message_error) or "getaddrinfo failed" in str(message_error):
        print " will exit because of conection problems"
        return 100
    elif "User request limit reached" in str(message_error):
        print " User request limit reached and will exiiiiiiiiiiiiiiiiiit "
        return 100

    elif "The After Cursor specified exceeds the max limit supported by this endpoint" in str(message_error):
        print " the after error and will exiiiiiiiiiiiiiiiiiit "
        return 100
    elif ("An unknown error has occurred." in str(message_error)) or (
        "An unknown error occurred" in str(message_error)):
        print " An unknown error and will exiiiiiiiiiiiiiiiiiit "
        return 100  # exit()
    elif "Une tentative de connexion a" in str(message_error) or "getaddrinfo failed" in str(message_error):
        print " will exit because of conection problems"
        return 100


def url_Suivant(url, usr, pwd, fichier, lock):
    error = 0
    first = 0
    error_message2 = ""
    web_response = ""
    heure_actuelle()
    etape = 0
    #g , error,token_or_error_message2 = Verify_token(fichier,lock)
    try:
        print "  Url a chercher phasee 1 : ", url
        web_response = urllib2.urlopen(url)
        error = 1
        etape = 1

    except urllib2.HTTPError as e1:
            error_message2 = e1.read()
        #if( error == 1):
            #url = remplacer_token(url, token_or_error_message2)
            #print "  Url a chercher phase 125 : ", url
            #time.sleep(10)
            #web_response = urllib2.urlopen(url)
        #else:
            type_error = gestion_error(error_message2)
            if type_error == 100:
                return 100


            print renouvlement_token, "  " , "Error validating access token: Session has expired" in str(error_message2)," urllib2.HTTPError 1 : ", error_message2

            if "Error validating access token: Session has expired" in str(error_message2) \
                    or "An access token is required" in str(error_message2) \
                    or "Error validating access token: The session is invalid" in str(error_message2) \
                    or "Invalid OAuth access token." in str(error_message2) \
                    or "Error validating access token: This may be because the user" in str(error_message2) \
                    or "An active access token must be used" in str(error_message2):
                print " lectuuuuuuuuuuuuuuuuuuuure "
                if renouvlement_token == 1 :
                    while renouvlement_token == 1 :
                        time.sleep(2)

                my_token = lecture_fichier(fichier, lock)
                print fichier," lectuuuuuuuuuuuuuuuuuuuure ",my_token
                url = remplacer_token(url, my_token)

                try:
                    print "  url a chercher phasee 2 : ", url
                    web_response = urllib2.urlopen(url)
                    error = 1
                    etape = 2
                except urllib2.HTTPError as e2:
                    error_message2 = e2.read()
                    print error_message2, " urllib2.HTTPError 2 : ", e2

                    type_error = gestion_error(error_message2)
                    if type_error == 100:
                        return 100

                    my_token = generation_token(usr, pwd, lock)
                    ecriture_fichier(fichier, my_token, lock)
                    url = remplacer_token(url, my_token)
                    try:
                        print "  url a chercher phase 3 : ", url
                        web_response = urllib2.urlopen(url)
                        error = 1
                        etape = 3
                    except urllib2.HTTPError as e2:
                        error_message2 = e2.read()
                        print error_message2, " urllib2.HTTPError 3 : ", e2

                        type_error = gestion_error(error_message2)
                        if type_error == 100:
                            return 100

                    except urllib2.URLError  as e3:
                        print "  urllib2.URLError 3 :  ", e3
                        error_message2 = e3
                        type_error = gestion_error(error_message2)
                        if type_error == 100:
                            return 100

                except urllib2.URLError  as e3:
                    print "  urllib2.URLError 3 :  ", e3
                    error_message2 = e3
                    type_error = gestion_error(error_message2)
                    if type_error == 100:
                        return 100


    except urllib2.URLError  as e7:
            print "  urllib2.URLError 3 :  ", e7
            error_message2 = e7
            type_error = gestion_error(error_message2)
            if type_error == 100:
                return 100

    """
    while error == 0:

        if "Please reduce the amount of data you're asking for" in str(error_message2):
            url = remplacer_limit_amount(url)
        elif "Error validating access token: Session has expired" in str(error_message2) \
                or "An access token is required" in str(error_message2) \
                or "Error validating access token: This may be because the user" in str(error_message2) \
                or "Error validating access token: The session is invalid" in str(error_message2) \
                or "Invalid OAuth access token." in str(error_message2) \
                or "An active access token must be used" in str(error_message2):
            my_token = generation_token(usr, pwd, lock)
            ecriture_fichier(fichier, my_token, lock)
            url = remplacer_token(url, my_token)
        elif "User request limit reached" in str(error_message2):
            print " User request limit reached and will exiiiiiiiiiiiiiiiiiit "
            return 100
        elif "The After Cursor specified exceeds the max limit supported by this endpoint" in str(error_message2):
            print " the after error and will exiiiiiiiiiiiiiiiiiit "
            return 100
        elif ("An unknown error has occurred." in str(error_message2)) or (
            "An unknown error occurred" in str(error_message2)):
            print " An unknown error and will exiiiiiiiiiiiiiiiiiit "
            return 100  # exit()
        elif "Une tentative de connexion a" in str(error_message2) or "getaddrinfo failed" in str(error_message2):
            print " will exit because of conection problems"
            return 100

        print "  Url a chercher phase direct :  ", url
        try:
            web_response = urllib2.urlopen(url)
            error = 1
            etape = 3
        except urllib2.HTTPError as e4:
            error_message2 = e4.read()
            print " Erreur  phase direct :  ", error_message2
        except urllib2.URLError  as e7:
            error_message2 = e7
            print "  urllib2.URLErrorrrr 3 :  ", error_message2
        except Exception  as e8:
            error_message2 = e8
            print "  Erreur urllib 4 :  ", error_message2
    """
    print " ertape is : ",etape
    readable_page = web_response.read()
    return json.loads(readable_page)


def lecture_fichier2(nom_fic):
    with open(nom_fic, 'rb') as f:
        file = pickle.load(f)
    return file


def retourner_graph2(post_id, lock):
    graph = ''
    try:
        print "lecture fichier graph"
        with open("token2-3.txt", 'rb') as f:
            my_token = pickle.load(f)
        # print my_token
        graph = facebook.GraphAPI(access_token=my_token, version='2.7')
        post = graph.get_object(post_id)
    except Exception as e:
        print "ecriture fichier graph ", e
        my_token = generation_token("crawling.user2@gmail.com", "123456789@1", lock)
        graph = facebook.GraphAPI(access_token=my_token, version='2.3')
        with open("token2-3.txt", 'wb') as thefile:
            pickle.dump(my_token, thefile)
    return graph


def remove_sessions(driver):
    url = 'https://www.facebook.com/settings?tab=security'
    driver.get(url)
    time.sleep(3)
    try:

        more_comments = driver.find_element_by_css_selector("span[class='_2gee']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

        more_comments = driver.find_element_by_css_selector("span[class='_4h8f']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

        more_comments = driver.find_element_by_css_selector(
            "a[class=' layerCancel _4jy0 _4jy3 _4jy1 _51sy selected _42ft']")
        print " - CLick on : ", more_comments
        driver.execute_script("arguments[0].click();", more_comments)
        time.sleep(3)

    except Exception as e:
        print " ID introuvable :  ", e


def Authentification(driver, usr, pwd):
    driver.get("http://www.facebook.com")
    elem = driver.find_element_by_id("email")
    elem.send_keys(usr)

    elem = driver.find_element_by_id("pass")
    elem.send_keys(pwd)

    elem.send_keys(Keys.RETURN)
    time.sleep(5)


def Acces_Webdriver():

    os.environ["webdriver.chrome.driver"] = chromedriver
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications": 2}
    chrome_options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    return driver


def generation_token(usr, pwd, lock):
    lock.acquire()

    token = ''
    try:
        driver = Acces_Webdriver()

        Authentification(driver, usr, pwd)
        # remove_sessions(driver)

        try:
            url = 'https://developers.facebook.com/tools/explorer/?method=GET&path=me&version=v2.7#'
            driver.get(url)
            time.sleep(10)

            elem_id_user_2 = driver.find_element_by_css_selector("input[class='_58al']")

            token = elem_id_user_2.get_attribute("value")
            print " Token Genere est : ", token

        except Exception as e:
            print " ID introuvable :  ", e
            token = 100
        driver.close()
        driver.quit()

    except Exception as e:
        print " Error : ", e
        if "unexpectedly exited. Status code was" in str(e):
            exit()
    finally:
        lock.release()
    return token


def ajout_group(post, cur, con, hashtag, hashtag_concerne, count, type, nom):
    # print post
    name = remplacer_quotes(post['name'])
    id = post['id']
    if type == 'Group':

        owner_name = ''
        owner_id = '0'
        updated_time = uniix_time(post['updated_time'])
        privacy = remplacer_quotes(post['privacy'])
        email = remplacer_quotes(post['email'])

        if 'owner' in post:
            owner_name = remplacer_quotes(post['owner']['name'])
            owner_id = post['owner']['id']
        sql = "INSERT INTO `" + database_name + "`.`groups` (`idGroup`,`name`,type,`privacy`,`email`,`updated_time`,`adherents`" \
                                                ",owner_name,owner_id , hashtag, hashtag_concerne   ) VALUES (" + id + ",'" + name + "','" + type + "','" + privacy \
              + "','" + email + "','" + str(updated_time) + "','0','" + owner_name + "'," + owner_id + ",0,'');"
    elif type == 'Page':
        fan_count = post['fan_count']
        sql = "INSERT INTO `" + database_name + "`.`groups` (`idGroup`,`name`,type, adherents, hashtag, hashtag_concerne   ) " \
                                                "VALUES (" + id + ",'" + name + "','" + type + "','" + str(
            fan_count) + "',1,'" + remplacer_quotes(hashtag_concerne) + "');"
    else:
        sql = "INSERT INTO `" + database_name + "`.`groups` (`idGroup`,`name`,type   ) " \
                                                "VALUES (" + id + ",'" + name + "','" + type + "');"

    try:
        print "(", count, "_", nom, ") Sql Group : ", sql
        cur.execute(sql)
        # con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Group Exception  :  ", e

    ajout_next(id, cur, con, count, nom)


def update_group(post, cur, con, count, type, nom, e):
    # print post

    error = remplacer_quotes(str(e))

    if type == 'Group':
        id = post['id']
        name = remplacer_quotes(post['name'])
        owner_name = ''
        owner_id = '0'
        updated_time = uniix_time(post['updated_time'])
        privacy = remplacer_quotes(post['privacy'])
        email = remplacer_quotes(post['email'])

        if 'owner' in post:
            owner_name = remplacer_quotes(post['owner']['name'])
            owner_id = post['owner']['id']
        sql = "update `" + database_name + "`.`groups`  set `name`='" + name + "',type='" + type + \
              "',`privacy`='" + privacy + "',`email`='" + email + "',`updated_time`='" + str(
            updated_time) + "',`adherents`='0' ," \
                            "owner_name='" + owner_name + "',owner_id=" + owner_id + " , hashtag=0, hashtag_concerne=''  where `idGroup` =" + id
    elif type == 'Page':
        id = post['id']
        name = remplacer_quotes(post['name'])
        fan_count = post['fan_count']
        sql = "update `" + database_name + "`.`groups`  set `name`='" + name + "',type='" + type + \
              "',`adherents`='" + str(fan_count) + "'   where `idGroup` =" + id
    else:
        id = post['id']
        sql = "update `" + database_name + "`.`groups`  set hashtag=33 , adherents='" + error + "' where `idGroup` =" + str(
            id)
    try:
        print "(", count, "_", nom, ") Sql Group : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update Group Exception  :  ", e


def ajout_reactions(id_reacter, nom_reacter, type_com_pub, type_r, pub_id, con, cur, count, nom):
    ajout_acteur(id_reacter, nom_reacter, con, cur, count, nom)

    try:
        sql = "INSERT INTO `" + database_name + "`.`likes` VALUES (" + str(id_reacter) + "," + str(pub_id) + "," \
              + str(type_com_pub) + "','" + str(type_r) + "');"
        print "(", count, "_", nom, ") Sql Reaction : ", sql
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print "(", count, "_", nom, ") Insert Reaction Exception  :  ", e


def update_threat(table, threated, identifiant_table, identifiant_val, con, cur, nom):
    try:
        sql = " update " + str(table) + " set " + str(threated) + " = 1   " \
                                                                  "where " + str(identifiant_table) + "=" + str(
            identifiant_val)
        print "(", nom, ")  Sql update Threat : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", nom, ") Update Threat Exception  :  ", e


def ajout_next(id, cur, con, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`nexts`  VALUES (" + str(id) + ",'','','','','','','','');"
        print "(", count, "_", nom, ") Sql Next = ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Next Exception  :  ", e


def update_next(id_gr, next_coloumn, next_value, cur, con, count, nom):
    sql = "hi"
    try:
        # print "(", count,"_",nom, ") Sql Next Update : ", sql
        sql = "UPDATE `" + database_name + "`.`nexts` SET `" + str(next_coloumn) + "` = '" + str(
            next_value) + "' WHERE `idNexts` = " + str(id_gr) + " ; "
        print "(", count, "_", nom, ") Sql Next Update : ", sql
        cur.execute(sql)
        con.commit()
        #print "(", count,"_",nom, ") Sql Next Update : ", sql
    except Exception as e:
        print "(", count, "_", nom, ") Next Update exception : ", e


def ajout_acteur(id, name, cur, con, count, nom):
    """
    if count > nombre_arret :
        print " EXiiiiiiiiiiiiiiiiiiiiiiiiiiiit " , nom
        exit()
        """
    try:
        sql = "INSERT INTO `" + database_name + "`.`acteur` (`idActeurFace`,`name`) VALUES (" + str(
            id) + ",'" + remplacer_quotes(name) + "');"
        print "(", count, "_", nom, ") Sql Actor : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Actor Exception  :  ", e


def update_acteur_next(id, next_friends, cur, con, count, nom):
    try:
        sql = " update acteur set next_friends = '" + next_friends + "'   where idActeurFace=" + str(id)
        print "(", count, "_", nom, ") Sql update Actor : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Update Actor Exception  :  ", e


def ajout_ami(id1, id2, cur, con, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`friends` (`idfriends1`,`idfriends2`) VALUES (" + str(
            id1) + ",'" + str(id2) + "');"
        print "(", count, "_", nom, ") Sql Friend : ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Friend Exception  :  ", e


def update_acteur(set, where, cur, con, count, nom):
    try:
        sql = "update `" + database_name + "`.`acteur` Set " + set + " where " + where
        print "(", count, "_", nom, ") Sql Actor : ", sql
        cur.execute(sql)
        # con.commit()

    except Exception as e:
        print "(", count, "_", nom, ") Insert Actor Exception  :  ", e


def ajout_comment(comment, idFeed, type, cur, con, count, nom):
    print "  AJOUT COMMENT : ", comment
    idcomm = comment['id'].split('_', 1)
    if len(idcomm) > 1:
        comment_id = idcomm[1]
    else:
        comment_id = idcomm[0]

    created_time = uniix_time(comment['created_time'])

    from_id = 0
    from_name = ''

    if 'from' in comment:
        if 'id' in comment['from']:
            from_id = comment['from']['id']
        if 'name' in comment['from']:
            from_name = remplacer_quotes(comment['from']['name'])
            print " from_name  = ", from_name

    message = ''
    if 'message' in comment:
        message = remplacer_quotes(comment['message'])

    ajout_acteur(from_id, from_name, cur, con, count, nom)
    try:
        sql = "INSERT INTO `" + database_name + "`.`comments` (`idComments`,`message`,like_count,`created_time`,`type`,`idfeed`,`idActeur`)" \
                                                "  VALUES (" + comment_id + ",'" + message + "','" + str(
            comment['like_count']) + "','" + \
              str(created_time) + "','" + type + "'," + str(idFeed) + "," + str(from_id) + ");"
        print "(", count, "_", nom, ") Sql Comment = ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Comment Exception  :  ", e


def parcourir_likes_comment(comm, id_com, type2, cur, con, count, usr, pwd, file, lock, nom, id_gro):
    idcomm = comm['id'].split('_', 1)
    if len(idcomm) > 1:
        idcom = idcomm[1]
    else:
        idcom = idcomm[0]

    likes2_next = ''
    likes2 = ''
    if 'data' in comm['reactions']:
        likes2 = comm['reactions']['data']
    if 'paging' in comm['reactions'] and 'next' in comm['reactions']['paging']:
        likes2_next = comm['reactions']['paging']['next']

    while likes2 <> '':
        for like2 in likes2:
            count += 1
            name = ''
            type = 'LIKE'
            if 'name' in like2:
                name = like2['name']
            if 'type' in like2:
                type = like2['type']
                # name = name.decode('utf8')
            ajout_like(like2['id'], idcom, type2, type, cur, name, con, count, nom)
        # con.commit()
        likes2 = ''
        if likes2_next <> '':
            json_data = url_Suivant(likes2_next, usr, pwd, file, lock)
            likes2_next = ''
            if 'data' in json_data:
                likes2 = json_data['data']
            if 'paging' in json_data and 'next' in json_data['paging']:
                likes2_next = json_data['paging']['next']
                # update_next(id_gro, 'feed_likes_next', likes2_next, cur, con, count, nom)

    return count


def ajout_like(id1, id2, type, type_like, cur, from_name, con, count, nom):
    ajout_acteur(id1, from_name, cur, con, count, nom)
    try:
        sql = "INSERT INTO `" + database_name + "`.`likes` VALUES (" + str(id1) + "," + str(
            id2) + ",'" + type + "','" + type_like + "','');"
        print "(", count, "_", nom, ") Sql Like = ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Like Exception  :  ", e


def ajout_tag(type_tag, id_tagged, id_where, type_place, cur, tagged_name, tagger_name, id_tagger, con, count, id_group,
              nom):
    # id

    ajout_acteur(id_tagged, tagged_name, cur, con, count, nom)
    ajout_acteur(id_tagger, tagger_name, cur, con, count, nom)
    try:
        sql = "INSERT INTO `" + database_name + "`.`tags_acteur`(idActeurFace,idtag,type,type_tag,disapear_time,idGroup,id_tag_where)" \
                                                " VALUES (" + str(id_tagger) + "," + str(
            id_tagged) + ",'" + type_place + "','" + type_tag + "',''," \
              + str(id_group) + "," + str(id_where) + ");"
        print "(", count, "_", nom, ") Sql Tag = ", sql
        cur.execute(sql)
        con.commit()

    except Exception as e:
        print "(", count, "_", nom, ") Insert Tag Exception  :  ", e


def ajout_membre(id_group, member_id, administrator, cur, con, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`members` (`idGroup`,`idActeurs`,`administrateur`,disapear_time) " \
                                                "VALUES (" + str(id_group) + "," + str(
            member_id) + ",'" + administrator + "','');"
        print "(", count, "_", nom, ") Sql Member = ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Member Exception  :  ", e


def ajout_parcour(id_acteur, id_etablissement, type, cur, con, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`parcours` VALUES (" + str(id_acteur) + ",'" + str(
            id_etablissement) + "','" + type + "');"
        print "(", count, "_", nom, ") Sql parcours = ", sql
        cur.execute(sql)
        # con.commit()
    except Exception as e:
        print "(", count, "_", nom, ") Insert Parcours Exception  :  ", e


def ajout_feed(idFeed1, name, created_time, updated_time, story, message, from_id, shares, id_group, cur, con, count,
               nom):
    inserted = 0
    try:
        sql = "INSERT INTO `" + database_name + "`.`Feed`(`idFeed`,`name`,`createdtime`," \
                                                "`updatedtime`,message,story,`IdActeurs`,`shares`,`idGroup`) VALUES (" + str(
            idFeed1) + ",'" + name + \
              "','" + str(created_time) + "','" + str(updated_time) + "','" + story + "','" + message + "','" + \
              str(from_id) + "','" + str(shares) + "'," + str(id_group) + ");"
        print "(", count, "_", nom, ") Sql Feed = ", sql
        cur.execute(sql)
        con.commit()
        inserted = 1
    except Exception as e:
        print " Insert Feed Exception  :  ", e
    return inserted


def ajout_post(idpost, message, story, created_time, updated_time, shares, iduser_post, cur, con, count, nom):
    try:
        sql = "INSERT INTO `" + database_name + "`.`posts`(`idposts`,`messsage`,`story`,`createdtime`,`updatedtime`,`shares`,`idActeurFace`) " \
                                                " VALUES (" + str(
            idpost) + ",'" + message + "','" + story + "','" + str(created_time) + "','" + str(updated_time) \
              + "','" + str(shares) + "'," + str(iduser_post) + ");"
        print "(", count, "_", nom, ") Sql Post = ", sql
        cur.execute(sql)
        con.commit()
    except Exception as e:
        print " Insert Post Exception  :  ", e


def ajout_share_prof(id_group_original, idfeed_original, created_time_original, created_time_destination, disapear_time,
                     id_group_destination, idfeed_destination, parent_id, story, cur, con, count, id_feed_etudie, nom):
    try:
        # print "sstory  :", story
        story = story.encode('utf-8')
        # print "sstory  :", story
        sql = "INSERT INTO `" + database_name + "`.`shares_destination` VALUES (" + str(id_group_original) + "," + str(
            idfeed_original) + \
              ",'" + str(created_time_original) + "','" + str(created_time_destination) + "',''," + str(
            id_group_destination) + "," + str(idfeed_destination) + \
              ",'" + str(parent_id) + "','" + str(story) + "','" + str(id_feed_etudie) + "',0);"
        print "(", count, "_", nom, ") Sql Share = ", sql
        cur.execute(sql)
        # con.commit()
    except Exception as e:
        print " Insert Share Exception  :  ", e


def ajout_share(id_group_original, idfeed_original, created_time_original, created_time_destination, disapear_time,
                id_group_destination, idfeed_destination, parent_id, story, sh_count, cur, con, count, id_feed_etudie,
                nom):
    try:
        story = story.encode('utf-8')

        sql = " INSERT INTO `" + database_name + "`.`shares` VALUES (" + str(id_group_original) + "," + str(
            idfeed_original) + ",'" + str(created_time_original) + "','" + str(created_time_destination) + "',''," + \
              str(id_group_destination) + "," + str(idfeed_destination) + \
              ",'" + str(parent_id) + "','" + str(story) + "','" + str(id_feed_etudie) + "',0," + str(sh_count) + ");"
        print "(", count, "_", nom, ") Sql Share = ", sql
        cur.execute(sql)
        # con.commit()
    except Exception as e:
        print " Insert Share Exception  :  ", e


def retour_next(idgroup, id_retourne, cur):
    """ id est la position de l'item a retourne
    0   id Groupe    1   feed_member_next     2   feed_comments_next   3   next_feeds    4   feed_likes_next    """
    sql = 'select * from nexts where idNexts=' + str(idgroup)
    print sql
    cur.execute(sql)
    retour = ''
    for item in cur:
        retour = item[id_retourne]
    return retour


def remplacer_token(token, access):
    t1 = token.split('access_token=', 1)
    t2 = t1[1].split('&', 1)
    t = str(t1[0]) + 'access_token=' + str(access) + '&' + str(t2[1])
    remplacer_limit(t)
    return t


def remplacer_limit_amount(token):
    return token.replace("&limit=25", "&limit=8")


def remplacer_limit(token):
    return token.replace("&limit=25", "&limit=70")


def remplacer_quotes(chaine):
    chaine = chaine.replace("'", "")
    chaine = chaine.replace('"', '')
    return chaine


def uniix_time(t):
    date_tuple = t.split('T')
    date_num = date_tuple[0].split('-')
    hours1 = date_tuple[1].split('+')
    hours = hours1[0].split(':')
    epoch = datetime(1970, 1, 1)
    t = datetime(int(date_num[0]), int(date_num[1]), int(date_num[2]), int(hours[0]), int(hours[1]), int(hours[2]))
    diff = t - epoch
    un_time = diff.days * 24 * 3600 + diff.seconds
    #print "  t   ", t, "     ", un_time
    return int(un_time)

"""
def comment_subcomment(first_sql, file, usr, pwd, nom, comment_like, lock):
    connection = connect_db()
    cursor = connection.cursor()
    G = ''
    next_feeds = ''
    comments = ''
    comments_next = ''
    likes = ''
    likes_next = ''
    comments_next2 = ''
    comments2 = ''
    likes2 = ''
    likes2_next = ''
    my_token = ''
    count = 0
    feeds = ''
    connection = connect_db()
    cursor = connection.cursor()
    cursor.execute(first_sql)
    groups = cursor.fetchall()

    my_file = Path(file)
    print " file is ", file
    if my_file.exists():
        try:
            with open(file, 'rb') as f:
                my_token = pickle.load(f)
        except Exception as e:
            count += 1
            print "error ", e

    for group in groups:
        id_group = group[0]
        url = group[1]
        if len(url) > 10:
            print ("here 1 ", next)
            url = remplacer_token(url, my_token)
            json_data = url_Suivant(url, usr, pwd, file, lock)
            # print " jsooooooooooon data = " , json_data
            if 'data' in json_data:
                feeds = json_data['data']
            if 'paging' in json_data and 'next' in json_data['paging']:
                next_feeds = json_data['paging']['next']
        else:
            post_id = str(
                id_group) + '?fields=feed{comments{from,created_time,message,like_count,message_tags,id,reactions{id,name,type}}}'
            G = retourner_graph(file, usr, pwd, lock)
            print ("here 2 ", post_id)
            try:
                post = G.get_object(id=post_id)
                if 'feed' in post and 'data' in post['feed']:
                    feeds = post['feed']['data']
                if 'feed' in post and 'paging' in post['feed'] and 'next' in post['feed']['paging']:
                    next_feeds = post['feed']['paging']['next']
                    # print (" feeds ", feeds, ' post ', post)
            except facebook.GraphAPIError as ex:
                print " errooor  : ", ex
                sql = " update nexts set error ='" + str(ex).replace("'", " ") + "'   where idNexts=" + str(id_group)
                cursor.execute(sql)
                connection.commit()
                feeds = ''
        # print " feeeeeeeeeeeeeeeeeeeeeeeds data = ", feeds

        while feeds <> '':  # jour < 2:#feeds <> '':  # Un mois de Feed
            for feed in feeds:
                if 'comments' in feed:
                    if 'data' in feed['comments']:
                        comments = feed['comments']['data']
                    if 'paging' in feed['comments'] and 'next' in feed['comments']['paging']:
                        comments_next = feed['comments']['paging']['next']
                        print  "   comments_next  =  ", comments_next
                    while comments <> '':
                        for comment in comments:
                            idFeed1 = feed['id'].split('_', 1)
                            if comment_like == 1:
                                count += 1
                                ajout_comment(comment, idFeed1[1], 'f', cursor, connection, count,
                                              str(count) + "_" + nom)
                            elif 'reactions' in comment:
                                # print " reactions data = ", feeds
                                count = parcourir_likes_comment(comment, 1, 'c', cursor, connection, count, usr, pwd,
                                                                file, lock, nom, id_group)
                            # connection.commit()
                            if 'comments' in comment:
                                id_comm = comment['id'].split('_', 1)
                                post_id = str(id_comm[
                                                  1]) + '?fields=comments{from,created_time,message,like_count,message_tags,id,reactions{id,name,type}}'
                                ##print "contieeent subcomments", post_id
                                try:
                                    post2 = G.get_object(id=post_id)
                                except Exception as e:
                                    my_token = generation_token(usr, pwd, lock)
                                    G = facebook.GraphAPI(access_token=my_token, version='2.7')
                                    post2 = G.get_object(id=post_id)
                                if 'data' in post2['comments']:
                                    comments2 = post2['comments']['data']
                                if 'paging' in post2['comments'] and 'next' in post2['comments']['paging']:
                                    comments_next2 = post2['comments']['paging']['next']
                                    # print  "   comments_next2  =  ", comments_next2

                                while comments2 <> '':
                                    for comment2 in comments2:
                                        count += 1
                                        if comment_like == 1:
                                            ajout_comment(comment2, id_comm[1], 'c', cursor, connection, count,str(count) + "_" + nom)
                                        elif 'reactions' in comment2:
                                            count = parcourir_likes_comment(comment2, 1, 'sc', cursor, connection,
                                                                            count, file, lock,
                                                                            nom, id_group)
                                    # connection.commit()
                                    comments2 = ''
                                    if comments_next2 <> '':
                                        # print ' Comments next 2 urllib '
                                        json_data2 = url_Suivant(comments_next2, usr, pwd, file, lock)
                                        comments_next2 = ''
                                        if 'data' in json_data2:
                                            comments2 = json_data2['data']
                                        if 'paging' in json_data2 and 'next' in json_data2['paging']:
                                            comments_next2 = json_data2['paging']['next']
                                            # print  "  here comments_next2  =  ", comments_next2
                                            # if comment_like == 0:
                                            # update_next(id_group, 'feed_likes_next', next_feeds, cursor, connection, count,nom)
                                            # else:
                                            # update_next(id_group, 'feed_comments_next', next_feeds, cursor,
                                            # connection, count, nom)
                        comments = ''
                        if comments_next <> '':
                            # print ' Comments next urllib'
                            json_data = url_Suivant(comments_next, usr, pwd, file, lock)
                            comments_next = ''
                            if 'data' in json_data:
                                comments = json_data['data']
                            if 'paging' in json_data and 'next' in json_data['paging']:
                                comments_next = json_data['paging']['next']
                                # print  "  here comments_nextcomments_next  =  ", comments_next
                                # if comment_like == 0:
                                #   update_next(id_group, 'feed_likes_next', next_feeds, cursor, connection, count, nom)
                                # else:
                                #    update_next(id_group, 'feed_comments_next', next_feeds, cursor, connection, count,nom)
            feeds = ''
            if next_feeds <> '':
                # print ' feed next  urllib'
                json_data = url_Suivant(next_feeds, usr, pwd, file, lock)
                next_feeds = ''
                if 'paging' in json_data and 'next' in json_data['paging']:
                    next_feeds = json_data['paging']['next']

                    if comment_like == 0:
                        update_next(id_group, 'feed_likes_next', next_feeds, cursor, connection, count, nom)
                    else:
                        update_next(id_group, 'feed_comments_next', next_feeds, cursor, connection, count, nom)
                        # print  "  here next_feeds  =  ", next_feeds
                if 'data' in json_data:
                    feeds = json_data['data']

        if comment_like == 0:
            sql = " update nexts set feed_likes_next = '1'   where idNexts=" + str(id_group)
        else:
            sql = " update nexts set feed_comments_next = '1'   where idNexts=" + str(id_group)
        print "saql    = ", sql
        cursor.execute(sql)
        connection.commit()

        cursor.execute(first_sql)
        groups = cursor.fetchall()
    connection.close()
    os._exit(0)
"""

def ecriture_fichier(nom_fic, val, lock):
    lock.acquire()
    try:
        with open(nom_fic, 'wb') as f:
            pickle.dump(val, f)
    finally:
        lock.release()


def ecriture_fichier2(nom_fic, val):
    # print " fichier ",nom_fic
    with open(nom_fic, 'wb') as f:
        pickle.dump(val, f)
        f.close()


def lecture_fichier(nom_fic, lock):
    lock.acquire()
    try:
        with open(nom_fic, 'rb') as f:
            file = pickle.load(f)
            f.close()
    finally:
        lock.release()
    return file
