from datetime import datetime
import xlsxwriter
import datetime
from facebook_share_18_04_2017.sharing._0_Functions import *


def main():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    workbook = xlsxwriter.Workbook('Sommeil_Etude.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': 1})

    # Write some data headers.
    worksheet.write('A1', 'IdActeur', bold)
    worksheet.write('B1', 'Nom', bold)
    worksheet.write('C1', 'Hour', bold)
    worksheet.write('D1', 'Heure', bold)
    worksheet.write('E1', 'Minute', bold)
    worksheet.write('F1', 'Seconde', bold)
    # worksheet.write('C1', 'Reaction', bold)

    sql = "SELECT feed.createdtime , feed.idActeurs , comments.created_time , comments.idActeur FROM feed , comments " \
          " where comments.idfeed=feed.idfeed order by feed.idActeurs "
    print sql
    cursor2.execute(sql)
    feeds_datetime = cursor2.fetchall()
    row = 1
    col = 0
    for feed_datetime in feeds_datetime:
        feed_date = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%d-%m-%Y')
        feed_hours = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%H:%M:%S')
        feed_Hour = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%H')
        feed_Minu = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%M')
        feed_Sec = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%S')
        worksheet.write_number(row, col, feed_datetime[1])
        worksheet.write_string(row, col + 1, feed_datetime[0])
        worksheet.write_string(row, col + 2, feed_date)
        worksheet.write_string(row, col + 3, feed_hours)
        worksheet.write_string(row, col + 4, feed_Hour)
        worksheet.write_string(row, col + 5, feed_Minu)
        worksheet.write_string(row, col + 6, feed_Sec)
        row += 1

        feed_date = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%d-%m-%Y')
        feed_hours = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%H:%M:%S')
        feed_Hour = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%H')
        feed_Minu = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%M')
        feed_Sec = datetime.fromtimestamp(int(feed_datetime[2])).strftime('%S')
        worksheet.write_number(row, col, feed_datetime[3])
        worksheet.write_string(row, col + 1, feed_datetime[2])
        worksheet.write_string(row, col + 2, feed_date)
        worksheet.write_string(row, col + 3, feed_hours)
        worksheet.write_string(row, col + 4, feed_Hour)
        worksheet.write_string(row, col + 5, feed_Minu)
        worksheet.write_string(row, col + 6, feed_Sec)
        row += 1

    workbook.close()
    connection.close()

if __name__ == "__main__":
    main()