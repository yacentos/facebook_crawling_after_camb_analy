import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import xlsxwriter

from _0_Functions import *


def main():
    """ Avec ce code , je peux afficher :
        1 : les publications destinations qui deviennent original et ont a leur tour des publication destinations
        2 : les groupes destinations qui deviennent original et ont a leur tour des groupes destinations"""
    connection = connect_db()
    cursor = connection.cursor()

    ################## difference et difference  ######################
    workbook = xlsxwriter.Workbook('Publications_Vitesse.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})
    worksheet.write_string('A1', 'Id Group', bold)
    worksheet.write_string('B1', 'Tranche', bold)
    worksheet.write_string('C1', 'Nombre', bold)
    worksheet.write_string('D1', 'vitesse', bold)
    worksheet.write_string('E1', 'Acceleration', bold)

    sql = 'Select Case When min(created_time_destination) < min(created_time_original) Then min(created_time_destination)' \
          ' else min(created_time_original) End As TheMin,' \
          ' Case When max(created_time_destination) > max(created_time_original) Then max(created_time_destination)' \
          ' else max(created_time_original) End As TheMax From   shares'
    print sql
    cursor.execute(sql)
    feed = cursor.fetchall()
    print feed

    count_feed  = 1
    V = []
    row = 1
    col = 0
    i = 0
    vitesse_ancien = 0
    interval = []
    evolution = []
    evolution_vitesse = []
    moyenne_date2 = []
    acceleration = []
    acceleration_zero = []

    sql = ' SELECT id_group_original  from shares group by id_group_original'
    cursor.execute(sql)
    groups = cursor.fetchall()

    for group in groups:

        feedtimemin = feed[0][0]
        print feedtimemin
        feedtimemax = feed[0][1]
        pas = 2678400  # 1 Mois
        # for feed in feeds:
        suivant = float(feedtimemin) + float(pas)
        print feedtimemin, '   <== min , max ==>', feedtimemax
        #delta_t = float((datetime.datetime.fromtimestamp(int(pas)).strftime('%m')))
        cumul = 0
        acc = 0

        while float(suivant) <= float(feedtimemax):
            sql = ' SELECT count(*) ,id_group_original  from shares where created_time_destination >='+str(feedtimemin)+\
                  ' and created_time_destination <'+str(suivant)+' and id_group_original='+str(group[0])+' group by id_group_original'
            #created_time_destination >='+str(feedtimemin)+' and ' \
            print sql
            cursor.execute(sql)
            elements = cursor.fetchall()
            if len(elements) < 1 :
                vitesse = 0
                acc = 0
            else :
                for element in elements:
                    print " avaaaaaaaatn  ",cumul
                    cumul = cumul + element[0]
                    print " apres  ", cumul
                    nbr   = 1
                    if element[1] == 377364058953191:
                        print " Danssss  ", cumul
                        moyenne_date2.append(str((datetime.datetime.fromtimestamp(int(feedtimemin)).strftime('%d-%m-%Y'))+"//"+
                                                  (datetime.datetime.fromtimestamp(int(suivant)).strftime('%d-%m-%Y'))))
                        moyenne_date = (float(feedtimemin) + float(suivant)) / 2
                        interval.append(moyenne_date)

                        v = str(element[1]), "[" + str(feedtimemin) + "," + str(suivant) + "[", str(element[0]), str(vitesse)
                        V.append(v)

                        evolution.append(cumul)
                        evolution_vitesse.append(element[0])

                        acc = ( element[0] - vitesse ) #/ float((datetime.datetime.fromtimestamp(int(pas)).strftime('%m')))
                        acceleration.append(acc)

                        acc2 = element[0]  / nbr  # / float((datetime.datetime.fromtimestamp(int(pas)).strftime('%m')))
                        acceleration_zero.append(acc2)

                        vitesse = element[0]
                        nbr += 1
                        print element[0], '    ==>   ', element[1], " vitesse", element[0]," vitesse ancien ", vitesse_ancien, " acc ", acc
            worksheet.write_string(row, col, str(group[0]))
            worksheet.write_string(row, col + 1, "[" + str(feedtimemin) + "," + str(suivant) + "[")
            worksheet.write_string(row, col + 2, str(cumul))
            worksheet.write_string(row, col + 3, str(vitesse))
            worksheet.write_string(row, col + 4, str(acc))
            row += 1

            feedtimemin  = float(suivant)
            suivant = float(feedtimemin) + float(pas)
            #print feedtimemin, "  ", suivant , "  ",feedtimemax
    plt.clf()
    # print interval
    # print comments
    # print feeds
    # print moyenne_date2[0],"    ", moyenne_date2[moyenne_date2.__len__()-1],"    ", min_date, "    ",max_date
    print len(interval),"  ",interval
    print len(evolution), "  ", evolution
    print len(moyenne_date2), "  ", moyenne_date2
    print len(evolution_vitesse), "  ", evolution_vitesse
    print len(acceleration), "  ", acceleration

    plt.title(" Evolution Partage Au Sein d'un Groupe ")
    plt.xticks(interval, moyenne_date2, rotation=90)
    red_patch = mpatches.Patch(color='red', label='Evolution en nombre de partage')
    green_patch = mpatches.Patch(color='green', label='Vitesse de partage')
    blue_patch = mpatches.Patch(color='blue', label='Acceleration de partage')
    plt.legend(handles=[red_patch, green_patch ,blue_patch])

    plt.plot(interval, evolution, 'r', interval, evolution_vitesse, 'g', interval, acceleration, 'b')
    plt.ylabel('Nombre')
    plt.xlabel('Date')
    plt.savefig("Publications_Vitesse_Acceleration/Evolutions.png")
    #plt.show()

    plt.title(" Evolution en nombre de partage ")
    plt.xticks(interval, moyenne_date2, rotation=90)
    red_patch = mpatches.Patch(color='red', label='Evolution en nombre de partage')
    plt.legend(handles=[red_patch])
    plt.plot(interval, evolution, 'r')
    plt.ylabel('Nombre')
    plt.xlabel('Date')
    plt.savefig("Publications_Vitesse_Acceleration/Ev_Nombre_partage.png")
    #plt.show()


    plt.title("Vitesse de partage")
    plt.xticks(interval, moyenne_date2, rotation=90)
    green_patch = mpatches.Patch(color='green', label='Vitesse de partage')
    plt.legend(handles=[green_patch])
    plt.plot(interval, evolution_vitesse, 'g')
    plt.ylabel('Nombre')
    plt.xlabel('Date')
    plt.savefig("Publications_Vitesse_Acceleration/Ev_vitesse_partage.png")
    #plt.show()


    plt.title(" Acceleration de partage Par interval ")
    plt.xticks(interval, moyenne_date2, rotation=90)
    blue_patch = mpatches.Patch(color='blue', label='Acceleration de partage')
    plt.legend(handles=[blue_patch])
    plt.plot( interval, acceleration, 'b')
    plt.ylabel('Nombre')
    plt.xlabel('Date')
    plt.savefig("Publications_Vitesse_Acceleration/Ev_acceleration_partage.png")
    #plt.show()


    """
    plt.title(" Acceleration de partage Par rapport a lroigine des axes")
    plt.xticks(interval, moyenne_date2, rotation=90)
    yellow_patch = mpatches.Patch(color='yellow', label='Acceleration de partage')
    plt.legend(handles=[yellow_patch])
    plt.plot(interval, acceleration_zero, 'y')
    plt.ylabel('Nombre')
    plt.xlabel('Date')
    plt.show()
    """
    workbook.close()
    connection.close()



if __name__ == '__main__':
    main()