import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

from facebook_share_18_04_2017.sharing._0_Functions import *


def main():
    """ Evolution Comments Publication
    Exemple : Etude de Saad Lamjarre  where hashtag = 1 """

    connection = connect_db()
    cursor = connection.cursor()

    #sql = 'SELECT feed.`created_time` , `feed`.`feed_iterator` , `feed`.`idfeed`  FROM `comments` , feed WHERE `comments`.`idfeed`=feed.idfeed group by `feed`.`feed_iterator` , `feed`.`idfeed`  '
    #cursor.execute(sql)
    date1 = 1476316800
    date2 = 1478995200
    sql = 'SELECT count(`comments`.`idComments`) , max(`comments`.`created_time`) , min(`comments`.`created_time`)  ' \
          ' FROM `comments` , feed , groups WHERE `comments`.`idfeed`=feed.idfeed ' \
          ' and `comments`.`created_time`>'+str(date1)+' and `comments`.`created_time`<'+str(date2)+\
          ' and feed.idgroup=groups.idgroup and hashtag =1'
    # ' and `comments`.`created_time`> 1473522444 '\  pas = pas = (( float(cur[1]) + float(cur[2]) ) / int(cur[0]) ) * 7
    # ' and `comments`.`created_time`>1474934400 and `comments`.`created_time`<1478563200'\
    #  1476144000     1477094400
    #  1476316800     1478995200  13/10/2016 ==> 13/11/2016

    cursor.execute(sql)
    cur = cursor.fetchone()
    pas = (( float(cur[1]) + float(cur[2]) ) / int(cur[0]) )

    comments = []
    feeds = []
    interval =[]
    moyenne_date2 = []

    min_date = float(cur[2])
    max_date = float(cur[1])

    iteration = min_date
    iteration_suivante = min_date + pas
    total_com = 0
    total_fee = 0

    print " Pas de calcul est " ,pas ,"    max  ",max_date,"  min  ",min_date
    while iteration_suivante <= max_date:
        sql = 'SELECT count(`comments`.`idComments`) FROM `comments` , feed , groups WHERE `comments`.`created_time`>='+str(iteration)+\
              ' and `comments`.`created_time`<'+str(iteration_suivante)+' and comments.idfeed=feed.idfeed and feed.idgroup=groups.idgroup and hashtag =1'
        print sql
        cursor.execute(sql)
        nbr_com = cursor.fetchone()
        sql = 'SELECT count(`feed`.`idfeed`) FROM `feed` , groups WHERE `feed`.`createdtime`>=' + str(iteration) + \
              ' and `feed`.`createdtime`<' + str(iteration_suivante)+' and feed.idgroup=groups.idgroup and hashtag =1'
        print sql
        cursor.execute(sql)
        nbr_fee = cursor.fetchone()

        total_com = total_com + nbr_com[0]
        total_fee = total_fee + nbr_fee[0]

        comments.append(nbr_com[0])
        feeds.append(nbr_fee[0])

        moyenne_date = (float(iteration)+float(iteration_suivante))/2
        interval.append(moyenne_date)

        moyenne_date2.append((datetime.datetime.fromtimestamp(int(moyenne_date)).strftime('%d-%m-%Y')))  # ('%Y-%m-%d %H:%M:%S')
        iteration = iteration_suivante
        iteration_suivante = iteration + pas
        #count_comments = cursor.fetchone()
        # print count_comments[0],"     ",count_comments[1],"   select   : ", sql
        #print "[",iteration,",",iteration_suivante,"] Comments   : ", nbr_com[0]," Feeds   : ", nbr_fee[0]," moyenne   : ", moyenne_date

    connection.close()
    #plt.rcParams['figure.figsize'] = (20.0, 20.0)
    plt.clf()
    #print interval
    #print comments
    #print feeds
    #print moyenne_date2[0],"    ", moyenne_date2[moyenne_date2.__len__()-1],"    ", min_date, "    ",max_date
    print " ploooooooooooooot ",iteration_suivante ," maax ", max_date
    plt.ylabel('Nombre')
    plt.xlabel('Date')

    plt.title(" Evolution Commentaires Sur Saad_Lamjarred "+datetime.datetime.fromtimestamp(date1).strftime('%d-%m-%Y')+
              "**"+datetime.datetime.fromtimestamp(date2).strftime('%d-%m-%Y'))
    plt.xticks(interval, moyenne_date2,rotation=90)
    red_patch = mpatches.Patch(color='red', label='Commentaires')
    plt.legend(handles=[red_patch])
    plt.plot(interval, comments,'r')
    plt.savefig("Evolution_comments_feeds/Evolution_comments_lamjarred.png")  # save as png
    plt.show()


    plt.title(" Evolution Publication Sur Saad_Lamjarred "+datetime.datetime.fromtimestamp(date1).strftime('%d-%m-%Y')+
              "**"+datetime.datetime.fromtimestamp(date2).strftime('%d-%m-%Y'))
    plt.xticks(interval, moyenne_date2, rotation=90)
    blue_patch = mpatches.Patch(color='green', label='Publications')
    plt.legend(handles=[ blue_patch])
    plt.plot( interval, feeds, 'g')
    plt.savefig("Evolution_comments_feeds/Evolution_feeds_lamjarred.png")  # save as png
    plt.show()

    plt.title(" Evolution Publication Commentaire Sur Saad_Lamjarred " + datetime.datetime.fromtimestamp(date1).strftime('%d-%m-%Y') +
        "**" + datetime.datetime.fromtimestamp(date2).strftime('%d-%m-%Y'))
    plt.xticks(interval, moyenne_date2, rotation=90)
    plt.legend(handles=[red_patch, blue_patch])
    plt.plot(interval, comments, 'r', interval, feeds, 'g')
    plt.savefig("Evolution_comments_feeds/Evolution_comments_feeds_lamjarred.png")  # save as png
    plt.show()

    print "total comments ",total_com
    print "total feed ", total_fee
if __name__ == '__main__':
    main()