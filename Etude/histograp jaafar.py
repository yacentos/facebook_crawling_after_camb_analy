import matplotlib.pyplot as plt
import numpy as np
import mysql.connector
import calendar

def connect_db():
    connection = mysql.connector.connect(user='root', password='', host='localhost', database='est_group', charset='utf8mb4',
                                         use_unicode=True)
    return connection

def fun_fetch_data_hour(name_table):
    v_x=[]
    v_y=[]
    cursor.execute("SELECT hour,count(*) as x FROM " + name_table + " where year!=2017 group by hour ")
    data_y = cursor.fetchall()
    for a in data_y:
        v_x.append(a[0])
        v_y.append(a[1])
    return v_x,v_y

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom',fontsize='smaller')

def autolabel2(rects):
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width() / 2.5, 1.05 * height,
                 '%d' % int(height),
                 ha='center', va='bottom', fontsize='smaller')

connection = connect_db()
cursor = connection.cursor()
table_name=["comments_hist_table","tags_hist_table","likes_hist_table","reactions_hist_table"]


x = []
y = []
z = []
t = []
s = []

x,y = fun_fetch_data_hour(table_name[0])
x,z = fun_fetch_data_hour(table_name[1])
x,t = fun_fetch_data_hour(table_name[2])
x,s = fun_fetch_data_hour(table_name[3])


plt.xticks(x,x)
p1=plt.bar(np.asarray(x)-0.3, y,width=0.2, color="blue",align='center')#parameter ,bottom=a2
p2=plt.bar(np.asarray(x)-0.1, z,width=0.2,  color="red",align='center')#parameter ,bottom=a2
p3=plt.bar(np.asarray(x)+0.1, t,width=0.2,  color="green",align='center')#parameter ,bottom=a2
p4=plt.bar(np.asarray(x)+0.3, s,width=0.2,  color="yellow",align='center')#parameter ,bottom=a2
plt.legend([p1,p2,p3,p4],["Comments","tags","likes","reactions"],loc=9) #positions 1-11
autolabel(p1)
autolabel2(p2)
autolabel(p3)
autolabel2(p4)
plt.title("Interactions by hour ")
plt.xlabel("Hour")
plt.ylabel("Number of interactions")
plt.savefig("hour_interactions.png")  # save as png
plt.show()

connection.commit()
connection.close()