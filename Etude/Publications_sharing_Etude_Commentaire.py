import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import pylab

from _0_Functions import *

G=nx.Graph()
def plot(T,nom):
    try:
        import pygraphviz
        from networkx.drawing.nx_agraph import graphviz_layout
    except ImportError:
        try:
            import pydotplus
            from networkx.drawing.nx_pydot import graphviz_layout
        except ImportError:
            raise ImportError("This example needs Graphviz and either "
                              "PyGraphviz or PyDotPlus")
    node_color = []
    node_lise = []
    node_size = []
    i = j = 0
    update = 0
    for vertic in T:
        update = 0
        if vertic[4] == 'n' :
            print vertic
            if not (G.has_node(vertic[2])):
                print i, '  haaaaaaaaas not npde', vertic[2]
                node_color.append(vertic[3])
                node_lise.append(int(vertic[2]))
                G.add_node(int(vertic[2]))  # , vertic[0]
                node_size.append(30)
                update = 1
        else:
            i += 1
            print i, '  -  edge', vertic

            if not (G.has_edge(int(vertic[3]), int(vertic[2]))):
                G.add_edge(int(vertic[3]), int(vertic[2]))
                update = 1
        print update,"    nodesss =", G.nodes()
        j += 1
        if update != 0 :
            # pos=nx.random_layout(G)
            # pos = nx.spring_layout(G)
            # pos = nx.circular_layout(G)
            # pos = nx.nx_pydot.graphviz_layout(G)
            pos = graphviz_layout(G, prog="neato")  # atlas algorithme
            # pos = nx.fruchterman_reingold_layout(G)
            # plt.clf()

            nx.draw_networkx_nodes(G, pos, nodelist=node_lise, node_color=node_color,node_size=node_size)
            nx.draw_networkx_edges(G, pos, width=1.0)
            plt.axis('off')  # supprimer les axes
            plt.pause(0.00000000001)
            plt.show()  # display
            #if j>4:plt.pause(600.00000000001)
            if (G.number_of_nodes() % 100) == 0 :
                plt.title(" Utilisateurs ayant Commente les Publications Partagees")
                blue_patch = mpatches.Patch(color='blue', label='Actor Comments')
                red_patch  = mpatches.Patch(color='red' , label='Publication partage')
                plt.legend(handles=[blue_patch,red_patch])

                plt.savefig("Publications_sharing_Etude_Commentaire/"+nom+str(G.number_of_nodes())+".png")  # save as png
                with open("Publications_sharing_Etude_Commentaire/position.txt", 'wb') as thefile:
                    print pos
                    pickle.dump(pos, thefile)
                with open("Publications_sharing_Etude_Commentaire/node_color.txt", 'wb') as thefile:
                    print node_color
                    pickle.dump(node_color, thefile)
                with open("Publications_sharing_Etude_Commentaire/node_size.txt", 'wb') as thefile:
                    print node_size
                    pickle.dump(node_size, thefile)
                with open("Publications_sharing_Etude_Commentaire/node_list.txt", 'wb') as thefile:
                    print node_lise
                    pickle.dump(node_lise, thefile)
                with open("Publications_sharing_Etude_Commentaire/edges.txt", 'wb') as thefile:
                    print G.edges()
                    pickle.dump(G.edges(), thefile)

    print "Nodes= ", G.number_of_nodes()
    print "Edges= ", G.number_of_edges()
    plt.savefig("Etude_Commentaire/"+nom+"_Final.png")

def main():

    connection = connect_db()
    cursor = connection.cursor()
    pylab.ion()  # Turn on interactive mode.
    pylab.hold(False)  # Clear the plot before adding new data.

    T = []
    sql = 'SELECT `comments`.`created_time` , `comments`.`idActeur` ,`shares`.`created_time_original`, ' \
          '`shares`.`idfeed_original` , id_group_original FROM `comments` , shares WHERE `comments`.`idfeed`=`shares`.`idfeed_original`' \
          ' order by `comments`.`created_time`,`shares`.`created_time_original` '
    print sql
    cursor.execute(sql)
    liste = cursor.fetchall()
    type ='Publications_sharing_Etude_Commentaire'
    for l in liste:
        TT = (float(l[0]), 1, l[1],'blue','n')
        T.append(TT)
        TT = (float(l[2]), 1, l[3], 'red', 'n')
        T.append(TT)
        min = float(l[0])
        if min < float(l[2]):
            min = float(l[2])
        TT = (min, 9, l[1], l[3], 'e')
        T.append(TT)


    sql = 'SELECT `comments`.`created_time` , `comments`.`idActeur` ,`shares`.`created_time_destination`, ' \
          '`shares`.`idfeed_destination` , id_group_destination FROM `comments` , shares WHERE `comments`.`idfeed`=`shares`.`idfeed_destination` ' \
          ' order by `comments`.`created_time` , `shares`.`created_time_destination` '
    print sql
    cursor.execute(sql)
    liste = cursor.fetchall()
    for l in liste:
        TT = (float(l[0]), 1, l[1], 'blue', 'n')
        T.append(TT)
        TT = (float(l[2]), 1, l[3], 'red', 'n')
        T.append(TT)
        min = float(l[0])
        if min < float(l[2]):
            min = float(l[2])
        TT = (min, 9, l[1], l[3], 'e')
        T.append(TT)

    connection.close()
    T.sort()
    plot(T,type)

if __name__ == '__main__':
    main()