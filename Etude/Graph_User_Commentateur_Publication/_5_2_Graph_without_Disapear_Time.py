import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import pylab

from _0_Functions import *

G=nx.Graph()
def main():

    try:
        import pygraphviz
        from networkx.drawing.nx_agraph import graphviz_layout
    except ImportError:
        try:
            import pydotplus
            from networkx.drawing.nx_pydot import graphviz_layout
        except ImportError:
            raise ImportError("This example needs Graphviz and either "
                              "PyGraphviz or PyDotPlus")

    connection = connect_db()
    cursor = connection.cursor()
    pylab.ion()  # Turn on interactive mode.
    pylab.hold(False)  # Clear the plot before adding new data.

    i = 0
    T = []
    node_color = []
    node_lise = []
    node_size = []

    sql = 'SELECT `comments`.`created_time` , `acteur`.`iterator` ,  `comments`.`idActeur` , `feed`.`feed_iterator` ' \
          ' FROM `comments` , feed , acteur WHERE feed.idgroup=9 and  `comments`.`idfeed`=feed.idfeed and ' \
          '`comments`.`idActeur`=acteur.idActeurFace '
    cursor.execute(sql)
    liste = cursor.fetchall()

    for l in liste:
        #if i>2:break
        i += 1
        TT = (float(l[0]),l[1],l[2],l[3],'c','red')
        T.append(TT)
        node_size.append(21)

    sql = 'SELECT feed.`createdtime` , `feed`.`feed_iterator` , `feed`.`idfeed` , feed.`Disapear_time`  ' \
          'FROM `comments` , feed WHERE feed.idgroup=9 and  `comments`.`idfeed`=feed.idfeed ' \
          'group by `feed`.`feed_iterator` , `feed`.`idfeed`  '
    cursor.execute(sql)
    liste = cursor.fetchall()
    for l in liste:
        # if i>2:break
        TT = (float(l[0]), l[1], l[2], 'f', 'blue','a')#add
        T.append(TT)
        node_size.append(7)
        i += 1
    connection.close()
    T.sort()
    print T.__len__(),"     avant i = " , i

    i= 0
    j = 0
    for vertic in T:
        if (vertic[4] == 'c') and (not G.has_edge(int(vertic[3]), int(vertic[1]))):
                i += 1
                print i,'  -  comm',vertic
                G.add_node(int(vertic[1]))   #,vertic[0]
                node_color.append(vertic[5])
                node_lise.append(vertic[1])
                G.add_edge(int(vertic[3]), int(vertic[1]), weight=0.2)
        elif (vertic[3] == 'f') and (not G.has_node(int(vertic[1]))):
                    print i,'  -  add feed', vertic

                    node_color.append(vertic[4])
                    node_lise.append(int(vertic[1]))
                    G.add_node(int(vertic[1]))   #, vertic[0]

                    #print  ' apres ',node_color
                    #
        j+=1
        #pos=nx.random_layout(G)
        #pos = nx.spring_layout(G)
        #pos = nx.circular_layout(G)
        pos = nx.nx_pydot.graphviz_layout(G)
        #pos = graphviz_layout(G, prog="neato")  # atlas algorithme
        #pos = nx.fruchterman_reingold_layout(G)
        #plt.clf()
        if (G.number_of_nodes() % 100) == 0:
            plt.title("Publication Comments Relation ( "+ str(G.number_of_nodes())+"_Nodes -- "+ str(G.number_of_edges())+"_Arrets )")
            blue_patch = mpatches.Patch(color='blue', label='Publications')
            red_patch = mpatches.Patch(color='red', label='Commentaires')
            plt.legend(handles=[blue_patch, red_patch])
            plt.savefig("_5_1_Graph_with_Disapear/Graph_with_Disapear_" + str(G.number_of_nodes()) + ".png")  # save as png

            with open("_5_1_Graph_with_Disapear/position.txt", 'wb') as thefile:
                print pos
                pickle.dump(pos, thefile)

            with open("_5_1_Graph_with_Disapear/node_color.txt", 'wb') as thefile:
                # thefile.write(str(node_color))
                print node_color
                pickle.dump(node_color, thefile)
            with open("_5_1_Graph_with_Disapear/node_size.txt", 'wb') as thefile:
                # thefile.write(str(node_size))
                print node_size
                pickle.dump(node_size, thefile)
            with open("_5_1_Graph_with_Disapear/node_list.txt", 'wb') as thefile:
                # thefile.write(str(node_lise))
                print node_lise
                pickle.dump(node_lise, thefile)
            with open("_5_1_Graph_with_Disapear/edges.txt", 'wb') as thefile:
                # thefile.write(str(node_lise))
                print G.edges()
                pickle.dump(G.edges(), thefile)
        nx.draw_networkx_nodes(G, pos, node_size=node_size, nodelist=node_lise, node_color=node_color,with_labels=True)
        nx.draw_networkx_edges(G, pos, width=1.0)
        plt.axis('off')  # supprimer les axes
        plt.pause(0.000000000000000000000000000000000000000000000000001)
        plt.show()  # display
    print "Nodes= ", G.number_of_nodes()
    print "Edges= ", G.number_of_edges()

    plt.savefig("weighted_graph.png")  # save as png
if __name__ == '__main__':
    main()