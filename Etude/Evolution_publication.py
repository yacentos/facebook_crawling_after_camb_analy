from datetime import datetime
import xlsxwriter
import datetime
from _0_Functions import *

dossier = "Evolution/"
ev_publication_xls = dossier+'Evoltuion_publication_PC1'
ev_commentaire_xls = dossier+'Evoltuion_commentaire_PC1'
ev_like_feed_xls = dossier+'Evoltuion_like_feed_PC1'
ev_like_comm_xls = dossier+'Evoltuion_like_comm_PC1'
ev_mot_feed_xls = dossier+'Evoltuion_Mot_feed_PC1'
ev_mot_comm_xls = dossier+'Evoltuion_Mot_comm_PC1'
ev_partage_xls = dossier+'Evoltuion_partage_PC1'

def Evolution(sql_script,ev,titre,critere):

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    statistics1 = {}
    stat_detai = []

    workbook = xlsxwriter.Workbook(ev+'.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.merge_range('A1:A2', 'Id Groupe', bold)
    worksheet.merge_range('B1:B2', 'Nom', bold)
    worksheet.merge_range('C1:C2', 'Date Creation', bold)
    #worksheet.merge_range('C1:P1', titre, bold)

    worksheet.write('C2', '2004', bold)
    worksheet.write('D2', '2005', bold)
    worksheet.write('E2', '2006', bold)
    worksheet.write('F2', '2007', bold)
    worksheet.write('G2', '2008', bold)
    worksheet.write('H2', '2009', bold)
    worksheet.write('I2', '2010', bold)
    worksheet.write('J2', '2011', bold)
    worksheet.write('K2', '2012', bold)
    worksheet.write('L2', '2013', bold)
    worksheet.write('M2', '2014', bold)
    worksheet.write('N2', '2015', bold)
    worksheet.write('O2', '2016', bold)
    worksheet.write('P2', '2017', bold)


    sql = " SELECT idnexts FROM nexts where  "+critere+"='1'  "# limit 1 " # where idGroup = "
    print sql
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    count = 0

    an_timestamp = 31622400
    print " nombre ",len(groups)
    statistics1.clear()
    for group in groups:
        del stat_detai[:]
        id_group = group[0]
        sql = " SELECT min(createdtime) , groups.name FROM groups,feed where groups.idgroup=feed.idgroup and feed.idgroup="+str(id_group)  # limit 1 " # where idGroup = "
        print sql
        cursor.execute(sql)
        feeds = cursor.fetchone()
        #stat_detai.append(id_group)

        d = '---'
        nom = '----'
        if feeds[0] != None :
            format2 = workbook.add_format({'num_format': 'dd/mm/yy'})
            worksheet.write(row, col + 2, feeds[0], format2)  # 28/02/13
        if feeds[1] != None:
                nom = feeds[1]
        #stat_detai.append(d)
        worksheet.write_number(row, col     , id_group)
        worksheet.write_string(row, col + 1 , nom)
        worksheet.write_string(row, col + 2, d)
        i = 3
        annee = 1072915200  # 01/01/2004

        while annee < 1514764800 : #01/01/2018 :

            sql_script2 = sql_script +  str(id_group) + " and createdtime >= " + str(annee) \
                      + " and createdtime < " + str(annee + an_timestamp)  # where idGroup = "
            print(sql_script2)
            cursor.execute(sql_script2)
            count = cursor.fetchone()
            if count[0] <> None :
                print str(id_group) ," == ( ",str(annee)," ,",str(annee+an_timestamp),"  )  ==>  " ,count[0]
                stat_detai.append(count[0])
                worksheet.write_number(row, col + i, count[0])
            else:
                print str(id_group), " == ( ", str(annee), " ,", str(annee + an_timestamp), "  )  ==>  ", 0
                stat_detai.append(0)
                worksheet.write_number(row, col + i, 0)
            annee = annee+an_timestamp  # 01/01/2004
            i += 1

        row += 1
        print id_group, " statttt " , statistics1
        statistics1[id_group] = stat_detai

        #afficher_figure(ev, titre, stat_detai, id_group)
        print " apres " , stat_detai

    workbook.close()
    ecriture_fichier2(ev, statistics1)
    connection.close()
    print statistics1

def afficher_figure(fic,titre,data,id):
        print " salam "
        lock = threading.Lock()
        #liste_group = lecture_fichier(fic,lock)
        interval = [1,5,10,15,20,25,30,35,40,45,50,55,60,65]
        date2    = [2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017]

    #for list in liste_group :
        #data = list
        print interval
        print data
        plt.clf()
        plt.ylabel('Nombre')
        plt.xlabel('Annee')
        plt.title(titre)
        plt.xticks(interval, date2)
        red_patch = mpatches.Patch(color='red', label='Commentaires')
        plt.legend(handles=[red_patch])
        plt.plot(interval, data, 'r')
        plt.savefig(fic + "_"+str(id)+".png")#,dpi=1024) # save as png
        #plt.show()

if __name__ == "__main__":
    sql = "SELECT count(idfeed) FROM feed where idgroup = "
    Evolution(sql,ev_publication_xls,'Nombre Publication Par Annee','next_feeds')

    sql = "SELECT count(idcomments) FROM comments,feed where comments.idfeed=feed.idfeed and feed.idgroup="
    Evolution(sql , ev_commentaire_xls,'Nombre Commentaire Par Annee','feed_comments_next')

    sql = "SELECT count(idActeur) FROM likes,feed where likes.idliked=feed.idfeed and likes.type='f' and  feed.idgroup="
    Evolution(sql, ev_like_feed_xls,'Nombre Reaction au publication Par Annee','')

    sql = "SELECT count(likes.idActeur) FROM likes,comments,feed where likes.idliked=comments.idcomments and " \
          " comments.idfeed=feed.idfeed and  likes.type='c' and feed.idgroup="
    Evolution(sql, ev_like_comm_xls, 'Nombre Reaction au Commentaire Par Annee','feed_comments_next')

    sql = "SELECT COUNT(LENGTH(story) - LENGTH(REPLACE(story, ' ', '')) + 1) FROM feed where feed.idgroup="
    Evolution(sql, ev_mot_feed_xls, 'Nombre Mot des publications Par Annee','next_feeds')

    sql = "SELECT COUNT(LENGTH(comments.message) - LENGTH(REPLACE(comments.message, ' ', '')) + 1) FROM comments,feed where " \
          " comments.idfeed=feed.idfeed and feed.idgroup="
    Evolution(sql, ev_mot_comm_xls, 'Nombre Mot des commentaires Par Annee','feed_comments_next')

    sql = "SELECT sum(shares)  FROM feed where feed.idgroup="
    Evolution(sql, ev_partage_xls, 'Nombre de Partage Par Annee','next_feeds')

    #afficher_figure(ev_partage_xls,'Nombre de Partage Par Annee')