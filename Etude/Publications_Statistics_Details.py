from datetime import datetime

import xlsxwriter

from _0_Functions import *


def main():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    workbook = xlsxwriter.Workbook('Publications_Statistics_Details.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})
    worksheet.merge_range('A1:A3', 'Id Communaute', bold)
    worksheet.merge_range('B1:B3', 'Nom', bold)
    worksheet.merge_range('C1:D3', 'Tranche', bold)
    worksheet.merge_range('Y1:AB3', 'Total Publications', bold)

    worksheet.merge_range('E1:X1', 'Type Publications', bold)
    worksheet.merge_range('E2:H2', 'Photos', bold)
    worksheet.write('E3', 'Nombre', bold)
    worksheet.write('F3', 'Reactions', bold)
    worksheet.write('G3', 'Share', bold)
    worksheet.write('H3', 'Nbr Commentaire', bold)

    worksheet.merge_range('I2:L2', 'Videos', bold)
    worksheet.write('I3', 'Nombre', bold)
    worksheet.write('J3', 'Reactions', bold)
    worksheet.write('K3', 'Share', bold)
    worksheet.write('L3', 'Nbr Commentaire', bold)

    worksheet.merge_range('M2:P2', 'Link', bold)
    worksheet.write('M3', 'Nombre', bold)
    worksheet.write('N3', 'Reactions', bold)
    worksheet.write('O3', 'Share', bold)
    worksheet.write('P3', 'Nbr Commentaire', bold)

    worksheet.merge_range('Q2:T2', 'Event', bold)
    worksheet.write('Q3', 'Nombre', bold)
    worksheet.write('R3', 'Reactions', bold)
    worksheet.write('S3', 'Share', bold)
    worksheet.write('T3', 'Nbr Commentaire', bold)

    worksheet.merge_range('U2:X2', 'Normal', bold)
    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    # 1262303999 # 31/12/2009
    #  1 an de tranche
    feedtimemin = 0

    sql = 'SELECT count(`feed`.`idfeed`) , max(`feed`.`createdtime`) , min(`feed`.`createdtime`)  ' \
          ' FROM  feed , groups WHERE feed.idgroup=groups.idgroup'
    # ' and `comments`.`created_time`> 1473522444 '\  pas = pas = (( float(cur[1]) + float(cur[2]) ) / int(cur[0]) ) * 7
    # ' and `comments`.`created_time`>1474934400 and `comments`.`created_time`<1478563200'\
    #  1476144000     1477094400
    #  1476316800     1478995200  13/10/2016 ==> 13/11/2016

    cursor.execute(sql)
    cur = cursor.fetchone()
    pas = (float(cur[2]) + float(cur[1])) / float(cur[0])
    feedtimemin = cur[2]
    pas_suivant = feedtimemin
    feedtimemax = cur[1]

    row = 3
    col = 0
    i = 1

    while pas_suivant < feedtimemax:
        # if i > 1: break
        sql = "SELECT groups.* FROM groups,feed where groups.idgroup=feed.idgroup and groups.idgroup>157713321077194 " \
              " group by groups.idgroup"
        print sql
        feedtimemin = cur[2]
        pas_suivant = feedtimemin
        feedtimemax = cur[1]
        cursor.execute(sql)
        groups = cursor.fetchall()

        for group in groups:
            # if i> 1:break
            pas_suivant = int(pas_suivant) + int(pas)
            worksheet.write_number(row, col, group[0])
            worksheet.write_string(row, col + 1, group[2])

            worksheet.write_string(row, col + 2,
                                   datetime.datetime.fromtimestamp(int(feedtimemin)).strftime('%d-%m-%Y') +
                                   "_" + datetime.datetime.fromtimestamp(int(feedtimemax)).strftime('%d-%m-%Y'))

            photos_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_photos"
            where = "  feed.idGroup=" + str(
                group[0]) + " and  (message like '%photo%' or message like '%picture%' ) and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(photos_view, cursor2, connection, worksheet, row, col + 4, where)

            videos_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_videos"
            where = " feed.idGroup=" + str(group[0]) + " and  (message like '%video%') and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(videos_view, cursor2, connection, worksheet, row, col + 8, where)

            link_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_link"
            where = " feed.idGroup=" + str(group[0]) + " and  (message like '%link%') and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(link_view, cursor2, connection, worksheet, row, col + 12, where)

            event_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_event"
            where = " feed.idGroup=" + str(group[0]) + " and  (message like '%event%') and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(event_view, cursor2, connection, worksheet, row, col + 16, where)

            normal_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_normal"
            where = " feed.idGroup=" + str(group[
                                               0]) + " and  (message not like '%link%' and message not like '%event%' and message not like '%video%'" \
                                                     " and message not like '%photo%' and message not like  '%picture%' ) and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(normal_view, cursor2, connection, worksheet, row, col + 20, where)

            total_view = "feed_" + str(feedtimemin) + "_" + str(pas_suivant) + "_total"
            where = " feed.idGroup=" + str(group[0]) + " and createdtime < " + str(
                pas_suivant) + " and createdtime > " + str(feedtimemin)
            statistique_publication(total_view, cursor2, connection, worksheet, row, col + 24, where)

            row += 1
            i += 1
        feedtimemin = pas_suivant

        # feedtimemin = "0"

    cursor.close()
    workbook.close()


def main_2():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    workbook = xlsxwriter.Workbook('Publications_Statistics_Details.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})

    worksheet.merge_range('A1:A3', 'Id Communaute', bold)
    worksheet.merge_range('B1:B3', 'Nom', bold)
    worksheet.merge_range('C1:D3', 'Tranche', bold)
    worksheet.merge_range('Y1:AB3', 'Total Publications', bold)

    worksheet.merge_range('E1:X1', 'Type Publications', bold)
    worksheet.merge_range('E2:H2', 'Photos', bold)
    worksheet.write('E3', 'Nombre', bold)
    worksheet.write('F3', 'Reactions', bold)
    worksheet.write('G3', 'Share', bold)
    worksheet.write('H3', 'Nbr Commentaire', bold)

    worksheet.merge_range('I2:L2', 'Videos', bold)
    worksheet.write('I3', 'Nombre', bold)
    worksheet.write('J3', 'Reactions', bold)
    worksheet.write('K3', 'Share', bold)
    worksheet.write('L3', 'Nbr Commentaire', bold)

    worksheet.merge_range('M2:P2', 'Link', bold)
    worksheet.write('M3', 'Nombre', bold)
    worksheet.write('N3', 'Reactions', bold)
    worksheet.write('O3', 'Share', bold)
    worksheet.write('P3', 'Nbr Commentaire', bold)

    worksheet.merge_range('Q2:T2', 'Event', bold)
    worksheet.write('Q3', 'Nombre', bold)
    worksheet.write('R3', 'Reactions', bold)
    worksheet.write('S3', 'Share', bold)
    worksheet.write('T3', 'Nbr Commentaire', bold)

    worksheet.merge_range('U2:X2', 'Normal', bold)
    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    sql = "SELECT * FROM groups "  # ,feed where groups.idgroup=feed.idgroup  group by groups.idgroup"
    print sql
    cursor.execute(sql)
    groups = cursor.fetchall()

    row = 3
    col = 0
    i = 1

    for group in groups:
        worksheet.write_number(row, col, group[0])
        worksheet.write_string(row, col + 1, group[2])

        photos_view = "feed_photos"
        where = "  feed.idGroup=" + str(group[0]) + " and  (message like '%photo%' or message like '%picture%' ) "
        statistique_publication(photos_view, cursor2, connection, worksheet, row, col + 4, where)

        videos_view = "feed_videos"
        where = " feed.idGroup=" + str(group[0]) + " and  (message like '%video%') "
        statistique_publication(videos_view, cursor2, connection, worksheet, row, col + 8, where)

        link_view = "feed_link"
        where = " feed.idGroup=" + str(group[0]) + " and  (message like '%link%') "
        statistique_publication(link_view, cursor2, connection, worksheet, row, col + 12, where)

        event_view = "feed_event"
        where = " feed.idGroup=" + str(group[0]) + " and  (message like '%event%') "
        statistique_publication(event_view, cursor2, connection, worksheet, row, col + 16, where)

        normal_view = "feed_normal"
        where = " feed.idGroup=" + str(
            group[0]) + " and  (message not like '%link%' and message not like '%event%' and message not like '%video%'" \
                        " and message not like '%photo%' and message not like  '%picture%' ) "
        statistique_publication(normal_view, cursor2, connection, worksheet, row, col + 20, where)

        total_view = "feed_total"
        where = " feed.idGroup=" + str(group[0])
        statistique_publication(total_view, cursor2, connection, worksheet, row, col + 24, where)

        row += 1
        i += 1

    cursor.close()
    workbook.close()

def create_tables(nom):
    sql  = " CREATE TABLE `"+nom+"` (`feed_iterator` bigint(255) NOT NULL AUTO_INCREMENT,`idFeed` bigint(255) NOT NULL," \
           "  `name` tinytext CHARACTER SET utf8mb4 NOT NULL, `createdtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0'," \
           "  `updatedtime` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0'," \
           "  `Disapear_Time` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0'," \
           "  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL," \
           "  `story` mediumtext CHARACTER SET utf8mb4 NOT NULL," \
           "  `shares` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0'," \
           "  `IdActeurs` bigint(255) NOT NULL," \
           "  `idGroup` bigint(255) NOT NULL," \
           "  `feed_share` int(11) DEFAULT '0'," \
           "  PRIMARY KEY (`idFeed`)," \
           "  KEY `IdActeurs_idx` (`IdActeurs`)," \
           "KEY `idGroup_idx` (`idGroup`)," \
           "KEY `iterator_index2` (`feed_iterator`)" \
           ") ENGINE=InnoDB AUTO_INCREMENT=5945981 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"
    connection = connect_db()
    cursor = connection.cursor()
    try :
        cursor.execute(sql)
        connection.commit()
    except Exception as e :
        print " error while creatin table ",nom, " : ",e
    cursor.close()
    connection.close()

def main_total():
    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()

    workbook = xlsxwriter.Workbook('Statistics_Details_Total.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter'})

    worksheet.merge_range('A1:A3', 'Id Communaute', bold)
    worksheet.merge_range('B1:B3', 'Nom', bold)
    worksheet.merge_range('C1:D3', 'Tranche', bold)
    worksheet.merge_range('Y1:AB3', 'Total Publications', bold)

    worksheet.merge_range('E1:X1', 'Type Publications', bold)
    worksheet.merge_range('E2:H2', 'Photos', bold)
    worksheet.write('E3', 'Nombre', bold)
    worksheet.write('F3', 'Reactions', bold)
    worksheet.write('G3', 'Share', bold)
    worksheet.write('H3', 'Nbr Commentaire', bold)

    worksheet.merge_range('I2:L2', 'Videos', bold)
    worksheet.write('I3', 'Nombre', bold)
    worksheet.write('J3', 'Reactions', bold)
    worksheet.write('K3', 'Share', bold)
    worksheet.write('L3', 'Nbr Commentaire', bold)

    worksheet.merge_range('M2:P2', 'Link', bold)
    worksheet.write('M3', 'Nombre', bold)
    worksheet.write('N3', 'Reactions', bold)
    worksheet.write('O3', 'Share', bold)
    worksheet.write('P3', 'Nbr Commentaire', bold)

    worksheet.merge_range('Q2:T2', 'Event', bold)
    worksheet.write('Q3', 'Nombre', bold)
    worksheet.write('R3', 'Reactions', bold)
    worksheet.write('S3', 'Share', bold)
    worksheet.write('T3', 'Nbr Commentaire', bold)

    worksheet.merge_range('U2:X2', 'Normal', bold)
    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    worksheet.write('U3', 'Nombre', bold)
    worksheet.write('V3', 'Reactions', bold)
    worksheet.write('W3', 'Share', bold)
    worksheet.write('X3', 'Nbr Commentaire', bold)

    row = 3
    col = 0
    i = 1

    worksheet.write_string(row, col, " *** ")
    worksheet.write_string(row, col + 1, " --- ")

    photos_view = "feed_photos"
    where = " and  (message like '%photo%' or message like '%picture%' ) "
    statistique_publication(photos_view, cursor2, connection, worksheet, row, col + 4, where)

    videos_view = "feed_videos"
    where = " and  (message like '%video%') "
    statistique_publication(videos_view, cursor2, connection, worksheet, row, col + 8, where)

    link_view = "feed_link"
    where =  " and  (message like '%link%') "
    statistique_publication(link_view, cursor2, connection, worksheet, row, col + 12, where)

    event_view = "feed_event"
    where = " and  (message like '%event%') "
    statistique_publication(event_view, cursor2, connection, worksheet, row, col + 16, where)

    normal_view = "feed_note"
    where = "  (message not like '%link%' and message not like '%event%' and message not like '%video%'" \
                    " and message not like '%photo%' and message not like  '%picture%' ) "
    statistique_publication(normal_view, cursor2, connection, worksheet, row, col + 20, where)

    total_view = "feed_total"
    where = " "
    statistique_publication(total_view, cursor2, connection, worksheet, row, col + 24, where)

    row += 1
    i += 1
    cursor.close()
    workbook.close()


def statistique_publication(view_name, cur, con, sheet, row, col, where):
    sql = " CREATE OR REPLACE VIEW  " + view_name + " as select feed.* from feed " \
                                                    " where True " + where
    print (sql)
    cur.execute(sql)
    con.commit()

    sql = " select count(*) from " + view_name
    print (sql)
    cur.execute(sql)
    nbr = cur.fetchone()
    if len(nbr) < 1:
        nbr[0] = 0
    print "  nombre  ", nbr[0]
    sheet.write_string(row, col, str(nbr[0]))

    sql = " select count(*) from " + view_name + " , likes  where  " + view_name + ".idfeed=likes.idliked"
    print (sql)
    cur.execute(sql)
    nbr = cur.fetchone()
    if len(nbr) < 1:
        nbr[0] = 0
    print "  reaction  ", nbr[0]
    sheet.write_string(row, col + 1, str(nbr[0]))

    sql = " select sum(shares) from " + view_name
    print (sql)
    cur.execute(sql)
    nbr = cur.fetchone()
    if len(nbr) < 1:
        nbr[0] = 0
    print "  share  ", nbr[0]
    sheet.write_string(row, col + 2, str(nbr[0]))

    sql = " select count(*) from " + view_name + " , comments  where  " + view_name + ".idfeed=comments.idfeed"
    print (sql)
    cur.execute(sql)
    nbr = cur.fetchone()
    if len(nbr) < 1:
        nbr[0] = 0
    print "  comments  ", nbr[0]
    sheet.write_string(row, col + 3, str(nbr[0]))


if __name__ == "__main__":
    create_tables("feed_photos")
    create_tables("feed_link")
    create_tables("feed_videos")
    create_tables("feed_event")
    create_tables("feed_note")
    main_total()
    #main()
