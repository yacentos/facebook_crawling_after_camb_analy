import networkx as nx
import pylab
import matplotlib.pyplot as plt
import pickle

def main():
    pylab.ion()  # Turn on interactive mode.
    pylab.hold(False)  # Clear the plot before adding new data.
    pos2 =[]

    with open("SaveGraph/position.txt", 'rb') as f:
        pos1 = pickle.load(f)
        print pos1
    with open("SaveGraph/node_list.txt", 'rb') as f:
        node_lise2 = pickle.load(f)
        print node_lise2
    with open("SaveGraph/node_color.txt", 'rb') as f:
        node_color = pickle.load(f)
        print node_color
    with open("SaveGraph/node_size.txt", 'rb') as f:
        node_size = pickle.load(f)
        print node_size
    with open("SaveGraph/edges.txt", 'rb') as f:
        edges = pickle.load(f)
        print edges

    G = nx.Graph()
    G.add_edges_from(edges)

    #print pos1
    nx.draw_networkx_nodes(G, pos1,node_color=node_color,node_size=node_size,nodelist=node_lise2)
    nx.draw_networkx_edges(G, pos1, width=1.0)
    plt.axis('off')  # supprimer les axes
    plt.show()  # display
    plt.pause(600.00000000001)

    "10.9.0.15"
    "root su esta/*2017"

if __name__ == '__main__':
    main()