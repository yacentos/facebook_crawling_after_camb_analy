import unicodedata

from facebook_share_18_04_2017.sharing._0_Functions import *


def contient_saad(id , g):
    post = g.get_object(id=str(id))
    name = unicodedata.normalize('NFKD', post['name']).encode('ascii','ignore')
    #print 'naaaaaaaame = ',post['name']
    if ( 'saad' in str.lower(name) or 'lamjarred' in str.lower(name)) or 'ghaltana' in str.lower(name) \
            or 'Ana machi sahel' in str.lower(name) :
        return post
    else:
        return 0

connection = connect_db()
cursor = connection.cursor()
graph = facebook.GraphAPI(access_token=my_token, version='2.7')

urls  = []
urls.append('lm3allem')
i = 1
count = 0
for url in urls:
    # **********   Utilisation de facebook-hashtag pour recuperer les apparition du hashtag **************************'
    req = 'http://www.facebook-hashtag.com/search/hashtag/'+url
    web_response = urllib2.urlopen(req)
    readable_page = web_response.read()
    hashtags = json.loads(readable_page)

    #Parcours des apparition du hashtag donne
    for hashtag in hashtags['result']:
        ajout_acteur(hashtag['userID'], hashtag['user'], cursor, connection,count)
        ajout_feed(hashtag['postID'],'name',hashtag['created'],'0','story',remplacer_quotes(hashtag['content']),
                   hashtag['userID'],hashtag['shares'],'1',cursor,connection,count)
        print hashtag['postID']
        print hashtag['userID']
        print hashtag['user']
        print hashtag['created']
        print hashtag['type']

        # Si la publication qui contient le hashtag a recu des likes , on les enregistre dans la BD
        if hashtag['likes']>0:
            #print " lik =",hashtag['likes']
            post_id = str(hashtag['postID']) + '?fields=likes'
            #post = graph.get_object(id=post_id)
            #parcourir_likes_comment(post,0,'f', cursor, connection)

        # Si la publication qui contient le hashtag a recu des commentaires , on les enregistre dans la BD
        if hashtag['comments'] > 0:
            #print "com =",hashtag['comments']
            post_id = str(hashtag['postID']) + '?fields=comments{from,created_time,message,like_count,message_tags,id,likes{id,name},comments{from,created_time,message,like_count,message_tags,id,likes{id,name}}}'
            comment_subcomment(post_id, 0, hashtag['postID'], my_token, graph,'facebook_gephi')
        if hashtag['shares'] > 0:
            print "sha =",hashtag['shares']

        # Si la publication qui contient le hashtag appartien a une page , on essai d'aller recuperer ses publications
        if hashtag['type']=='page' and hashtag['userID'] != '377364058953191' :
            # Si le nom de la page contient saad ou lamjarred on va la parcourir
                #id_group = hashtag['userID']
                #rupture = 0
                #Parcour de toutes les publications de la page
                #publication(hashtag['userID'], rupture)
                #Parcours des commentaires de toutes les publication dejas recupere
                post_id = str(hashtag['userID']) + '?fields=name,id'
                post = contient_saad(post_id, graph)
                if post <> 0 :
                    #post = graph.get_object(id=post_id)
                    count += 1
                    ajout_group(post,cursor,connection,1,url,count)
                    print "contieeent paaage ",post_id
                #comment_subcomment(post_id, rupture, id_group, my_token, graph, 'facebook_data')
        count += 1

cursor.close()
connection.close()



