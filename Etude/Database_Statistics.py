import mysql.connector
from datetime import datetime
import xlsxwriter
import datetime
from _0_Functions import *

def main():

    connection = connect_db()
    cursor = connection.cursor()
    cursor2 = connection.cursor()
    statistics = {}
    stat_detai = []
    workbook = xlsxwriter.Workbook('Database_Statistics_facebook_data.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'center',
    'valign': 'vcenter'})

    worksheet.merge_range('A1:A2', 'Id Communaute', bold)
    #worksheet.merge()
    worksheet.merge_range('B1:B2', 'Nom', bold)
    worksheet.merge_range('C1:C2', 'Type', bold)
    worksheet.merge_range('D1:D2', 'Nombre Adherents', bold)
    worksheet.merge_range('E1:E2', 'Publications', bold)
    worksheet.merge_range('F1:F2', 'Commentaires', bold)
    worksheet.merge_range('G1:G2', 'Commentaire LIKE', bold)
    worksheet.merge_range('H1:M1', 'Reactions', bold)
    worksheet.write('H2', 'LIKE', bold)
    worksheet.write('I2', 'LOVE', bold)
    worksheet.write('J2', 'HAHA', bold)
    worksheet.write('K2', 'WOW', bold)
    worksheet.write('L2', 'TRISTE', bold)
    worksheet.write('M2', 'Grr', bold)
    worksheet.merge_range('N1:P1', 'Tags', bold)
    worksheet.write('N2', 'With', bold)
    worksheet.write('O2', 'Publication', bold)
    worksheet.write('P2', 'Comment', bold)

    sql = "SELECT * FROM groups "#where idGroup = "
    print( sql)
    cursor2.execute(sql)
    groups = cursor2.fetchall()
    row = 2
    col = 0
    i = 1
    for group in groups:
        nbr_feeds = 0
        nbr_comm = 0
        nbr_com_like = 0
        nbr_rea_like = 0
        nbr_rea_love=0
        nbr_rea_haha=0
        nbr_rea_gr = 0
        nbr_rea_wow =0
        nbr_rea_triste =0
        if i==0 :
            i = 1
        else:
            stat_detai.append(group[0])
            stat_detai.append(group[2])
            worksheet.write_number(row, col   , group[0])
            worksheet.write_string(row, col+1 , group[2])
            print( " group 3 ",len(group[4]))
            if len(group[4]) == 1:
                stat_detai.append('Page')
                stat_detai.append('0')
                worksheet.write_string(row, col + 2, 'Page')
                worksheet.write_string(row, col+3 , '--')
                stat_detai.append('--')
            else:
                stat_detai.append('Groupe')
                worksheet.write_string(row, col + 2, 'Groupe')
                sql2 = "SELECT count(*) FROM members where idGroup="+ str(group[0])
                cursor.execute(sql2)
                members = cursor.fetchone()
                print( sql2,"  ==>  ",members[0])
                worksheet.write_number(row, col + 3, members[0])
                stat_detai.append(members[0])

            sql2 = "SELECT idfeed FROM feed where idGroup="+ str(group[0])
            print(sql2, "  ==>  ", group[0])
            cursor.execute(sql2)
            feeds = cursor.fetchall()
            for fe in feeds :
                nbr_feeds += 1

                sql2 = "SELECT idComments FROM comments where comments.idfeed=" + str(fe[0])
                print(sql2, "  ==>  ", fe[0])
                cursor.execute(sql2)
                comments = cursor.fetchall()
                for comm in comments :
                    nbr_comm += 1
                    sql2 = "SELECT count(idLiked) FROM likes where likes.idLiked=" + str(comm[0])
                    print(sql2, "  ==>  ", comm[0])
                    cursor.execute(sql2)
                    comments_like = cursor.fetchone()
                    print(sql2, "  ==>  ", comments_like[0])
                    nbr_com_like +=  comments_like[0]

                sql2 = "SELECT count(idLiked) , type_like FROM likes where likes.idLiked=" + str(fe[0]) + " group by type_like "
                cursor.execute(sql2)
                feeds_like = cursor.fetchall()
                print(sql2, "  ==>  ", feeds_like)

                for feed_like in feeds_like:
                    print(feed_like[0], "       ", feed_like[1])
                    if feed_like[1] == 'LIKE':
                        nbr_rea_like += feed_like[0]

                    elif feed_like[1] == 'LOVE':
                        nbr_rea_love += feed_like[0]

                    elif feed_like[1] == 'HAHA':
                        nbr_rea_haha += feed_like[0]

                    elif feed_like[1] == 'WOW':
                        nbr_rea_wow += feed_like[0]

                    elif feed_like[1] == 'TRISTE':
                        nbr_rea_triste += feed_like[0]

                    else:
                        nbr_rea_gr += feed_like[0]


            print(sql2, "  ==>  ", nbr_comm)
            worksheet.write_number(row, col + 5, nbr_comm)
            stat_detai.append(nbr_comm)

            print( sql2, "  ==>  ",nbr_feeds)
            worksheet.write_number(row, col + 4, nbr_feeds)
            stat_detai.append(nbr_feeds)


            worksheet.write_number(row, col + 6, nbr_com_like)
            stat_detai.append(nbr_com_like)

            worksheet.write_number(row, col + 7, nbr_rea_like)
            stat_detai[7] = 0

            worksheet.write_number(row, col + 8, nbr_rea_love)
            stat_detai[8] = 0

            worksheet.write_number(row, col + 9, nbr_rea_haha)
            stat_detai[9] = 0

            worksheet.write_number(row, col + 10, nbr_rea_wow)
            stat_detai[10] = 0

            worksheet.write_number(row, col + 11,nbr_rea_triste)
            stat_detai[11] = 0

            worksheet.write_number(row, col + 12,nbr_rea_gr)
            stat_detai[12] = 0

            sql2 = "SELECT count(idtag) FROM tags where tags.type_tag='with' and tags.idGroup=" + str(group[0])
            cursor.execute(sql2)
            tags_with = cursor.fetchone()
            print(sql2, "  ==>  ", tags_with[0])
            worksheet.write_number(row, col + 13, tags_with[0])
            stat_detai.append(tags_with[0])

            sql2 = "SELECT count(idtag) FROM tags where tags.type_tag='message' and tags.type='f' and tags.idGroup=" + str(group[0])
            cursor.execute(sql2)
            tags_mf = cursor.fetchone()
            print(sql2, "  ==>  ", tags_mf[0])
            worksheet.write_number(row, col + 14, tags_mf[0])
            stat_detai.append(tags_mf[0])

            sql2 = "SELECT count(idtag) FROM tags where tags.type_tag='message' and tags.type='c' and tags.idGroup=" + str(group[0])
            cursor.execute(sql2)
            tags_mc = cursor.fetchone()
            print(sql2, "  ==>  ", tags_mc[0])
            worksheet.write_number(row, col + 15, tags_mc[0])
            stat_detai.append(tags_mc[0])

            row += 1
            statistics[group[0]]=stat_detai
    #print( workbook
    workbook.close()
    ecriture_fichier("statistics_Database", statistics)
    connection.close()


if __name__ == "__main__":
    main()