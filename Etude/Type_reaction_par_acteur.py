import xlsxwriter
from facebook_share_18_04_2017.sharing._0_Functions import *


def main():
    connection = connect_db()
    cursor2 = connection.cursor()

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook('Type_reaction_par_acteur.xlsx')
    worksheet = workbook.add_worksheet()

    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': 1})

    # Write some data headers.
    worksheet.write('A1', 'IdActeur', bold)
    worksheet.write('B1', 'Nom', bold)
    worksheet.write('C1', 'Nombre', bold)
    worksheet.write('C1', 'Reaction', bold)
    """
    """
    sql = 'SELECT comments.`idActeur` , acteur.name  , count(`comments`.`com_iterator`) ' \
          'FROM `comments` , acteur where comments.`idActeur`=acteur.IdActeurFace group by comments.`idActeur`'
    cursor2.execute(sql)
    acteurs_comments = cursor2.fetchall()
    for acteur_comment in acteurs_comments:
        print " ** ", acteur_comment[0], " ** ", acteur_comment[1], " ** ", acteur_comment[2], " ** "

    sql = "SELECT likes.idActeur , acteur.name  , count(likes.idliked) as nb, likes.type_like " \
          " FROM likes , acteur where likes.idActeur=acteur.IdActeurFace and likes.type='f' " \
          " group by likes.idActeur , likes.type_like order by nb" \

    print sql
    cursor2.execute(sql)
    acteurs_comments = cursor2.fetchall()
    row = 1
    col = 0
    for acteur_comment in acteurs_comments:
        print " ** ", acteur_comment[0], " ** ", acteur_comment[1], " ** ", acteur_comment[2], " ** ", acteur_comment[
            3], " ** "
        worksheet.write_number(row, col, acteur_comment[0])
        worksheet.write_string(row, col + 1, acteur_comment[1])
        worksheet.write_number(row, col + 2, acteur_comment[2])
        worksheet.write_string(row, col + 3, acteur_comment[3])
        row += 1
    workbook.close()
    connection.close()


if __name__ == "__main__":
    main()